import 'dart:collection';
import 'package:ifada/model/product.dart';

class CategoryProduct {
  List<Product> products = [];
  int total_rows = 0;
  double max_price_for_slider = 0.0;
  double min_price_for_slider = 0.0;
  String image_path = "";

  static CategoryProduct fromMap(LinkedHashMap<String, dynamic>map){
    CategoryProduct product = CategoryProduct();
    if(map.containsKey("image_path")){
      product.image_path = map["image_path"];
    }
    if(map.containsKey("products")){
      (map["products"] as List).forEach((element) {
        var item = Product.fromMap(element);
        item.image = product.image_path+item.image;
        product.products.add(item);
      });
    }
    if(map.containsKey("total_rows"))
      product.total_rows = int.parse(map["total_rows"]);
    if(map.containsKey("max_price_for_slider"))
      product.max_price_for_slider = double.parse(map["max_price_for_slider"]);
    if(map.containsKey("min_price_for_slider"))
      product.min_price_for_slider = double.parse(map["min_price_for_slider"]);
    return product;
  }
}