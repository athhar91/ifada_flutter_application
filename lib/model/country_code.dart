class CountryCode{
  String cc = "";
  String country = "";
  String flag = "";

  CountryCode(this.cc,this.country,this.flag){}

  static List<CountryCode> getCountries (){
    var countries = List<CountryCode>();
    countries.add(CountryCode("+966","Saudi Arabia","assets/flag_saudi_arabia.png"));
    countries.add(CountryCode("+971","United Arab Emirates","assets/flag_uae.png"));
    countries.add(CountryCode("+973","Bahrain","assets/flag_bahrain.png"));
    countries.add(CountryCode("+974","Qatar","assets/flag_qatar.png"));
    countries.add(CountryCode("+965","Kuwait","assets/flag_kuwait.png"));
    countries.add(CountryCode("+91","India","assets/flag_india.png"));
    return countries;
  }
}