import 'dart:collection';

import 'package:ifada/model/product.dart';

class WishlistProduct {
  Product product = null;
  static WishlistProduct fromMap(LinkedHashMap<String, dynamic>map){
    var wishListProduct = WishlistProduct();
    wishListProduct.product = Product.fromMap(map);
    if(map.containsKey("MENU_NAME"))
      wishListProduct.product.menu_name = map["MENU_NAME"];
    if(map.containsKey("currency"))
      wishListProduct.product.currency = map["currency"];
    if(map.containsKey("product_image"))
      wishListProduct.product.image = map["product_image"];
    return wishListProduct;
  }
}