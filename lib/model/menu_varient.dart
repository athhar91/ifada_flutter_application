import 'dart:collection';

class MenuVarient{
  String id = "";
  String menu_id = "";
  String menu_varient_name = "";
  String unit_value = "";
  String unit_id = "";
  String price = "";
  String price_for_retailer = "";
  String status = "";
  String is_in_stock = "";
  String quantity = "";
  String position = "";
  String created_date = "";
  String updated_date = "";
  String lang = "";
  String user_id = "";
  String menu_category_id = "";
  String menu_sub_category_id = "";
  String menu_retailer_id = "";
  String menu_type = "";
  String menu_name = "";
  String brand_origin = "";
  String content = "";
  String description = "";
  String image = "";
  String is_approve = "";
  String country_id = "";
  String tax_amount = "";
  String keyword = "";
  String MANUFACTURER_ID = "";
  String ALLOW_TO_ADD_IN_CART = "";
  bool is_in_wishlist = false;
  bool is_in_cart = false;
  String cart_row_id = "";
  int cart_quantity = 0;


  static MenuVarient fromMap(LinkedHashMap<String, dynamic>map){
    MenuVarient item=MenuVarient();
    if(map.containsKey("id"))
      item.id = map["id"];
    if(map.containsKey("menu_id"))
      item.menu_name = map["menu_id"];
    if(map.containsKey("menu_varient_name"))
      item.menu_varient_name = map["menu_varient_name"];
    if(map.containsKey("unit_value"))
      item.unit_value = map["unit_value"];
    if(map.containsKey("unit_id"))
      item.unit_id = map["unit_id"];
    if(map.containsKey("price"))
      item.price = map["price"];
    if(map.containsKey("price_for_retailer"))
      item.price_for_retailer = map["price_for_retailer"];
    if(map.containsKey("status"))
      item.status = map["status"];
    if(map.containsKey("is_in_stock"))
      item.is_in_stock = map["is_in_stock"];
    if(map.containsKey("quantity"))
      item.quantity = map["quantity"];
    if(map.containsKey("position"))
      item.position = map["position"];
    if(map.containsKey("created_date"))
      item.created_date = map["created_date"];
    if(map.containsKey("updated_date"))
      item.updated_date = map["updated_date"];
    if(map.containsKey("lang"))
      item.lang = map["lang"];
    if(map.containsKey("user_id"))
      item.user_id = map["user_id"];
    if(map.containsKey("menu_category_id"))
      item.menu_category_id = map["menu_category_id"];
    if(map.containsKey("menu_sub_category_id"))
      item.menu_sub_category_id = map["menu_sub_category_id"];
    if(map.containsKey("menu_retailer_id"))
      item.menu_retailer_id = map["menu_retailer_id"];
    if(map.containsKey("menu_type"))
      item.menu_type = map["menu_type"];
    if(map.containsKey("menu_name"))
      item.menu_name = map["menu_name"];
    if(map.containsKey("brand_origin"))
      item.brand_origin = map["brand_origin"];
    if(map.containsKey("content"))
      item.content = map["content"];
    if(map.containsKey("description"))
      item.description = map["description"];
    if(map.containsKey("image"))
      item.image = map["image"];
    if(map.containsKey("is_approve"))
      item.is_approve = map["is_approve"];
    if(map.containsKey("country_id"))
      item.country_id = map["country_id"];
    if(map.containsKey("tax_amount"))
      item.tax_amount = map["tax_amount"];
    if(map.containsKey("keyword"))
      item.keyword = map["keyword"];
    if(map.containsKey("MANUFACTURER_ID"))
      item.MANUFACTURER_ID = map["MANUFACTURER_ID"];
    if(map.containsKey("ALLOW_TO_ADD_IN_CART"))
      item.ALLOW_TO_ADD_IN_CART = map["ALLOW_TO_ADD_IN_CART"];
    if(map.containsKey("is_in_wishlist"))
      item.is_in_wishlist = map["is_in_wishlist"].toString() == "1";
    if(map.containsKey("is_in_cart"))
      item.is_in_cart = map["is_in_cart"].toString() == "1";
    if(map.containsKey("cart_row_id"))
      item.cart_row_id = map["cart_row_id"];
    // string if empty and int if value should be 0 if empty
    if(map.containsKey("cart_quantity") && map.containsKey("cart_quantity") is int)
      item.cart_quantity = map["cart_quantity"];
    return item;
  }
}