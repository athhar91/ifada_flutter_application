import 'dart:collection';

import 'package:ifada/model/Response.dart';

class MyAddressesItem  {

  String id = "";
  String mobile_number = "";
  String address = "";
  String COUNTRY_NAME = "";

  static MyAddressesItem fromMap(LinkedHashMap<String, dynamic> map) {

    MyAddressesItem address = new MyAddressesItem();
    if (map.containsKey("id")) address.id = map["id"];
    if (map.containsKey("mobile_number"))
      address.mobile_number = map["mobile_number"];
    if (map.containsKey("address")) address.address = map["address"];
    if (map.containsKey("COUNTRY_NAME"))
      address.COUNTRY_NAME = map["COUNTRY_NAME"];
    return address;
  }
}
