import 'dart:collection';

class Order {
  String id = "";
  String total_items = "";
  String final_amount = "";
  String country_id = "";
  String created_date = "";
  String user_name = "";
  String profile_image = "";
  String coupon_code_amount = "";
  String item_cancelled_amount = "";
  String transaction_id = "";
  String pt_invoice_id = "";
  String payment_type = "";
  String payment_status = "";


  static Order fromMap(LinkedHashMap<String, dynamic>map) {
    Order item = Order();
    if(map.containsKey("id"))
      item.id = map["id"];
    if(map.containsKey("total_items"))
      item.total_items = map["total_items"];
    if(map.containsKey("final_amount"))
      item.final_amount = map["final_amount"];
    if(map.containsKey("country_id"))
      item.country_id = map["country_id"];
    if(map.containsKey("created_date"))
      item.created_date = map["created_date"];
    if(map.containsKey("user_name"))
      item.user_name = map["user_name"];
    if(map.containsKey("profile_image"))
      item.profile_image = map["profile_image"];
    if(map.containsKey("coupon_code_amount"))
      item.coupon_code_amount = map["coupon_code_amount"];
    if(map.containsKey("item_cancelled_amount"))
      item.item_cancelled_amount = map["item_cancelled_amount"];
    if(map.containsKey("transaction_id"))
      item.transaction_id = map["transaction_id"];
    if(map.containsKey("created_date"))
      item.created_date = map["created_date"];
    if(map.containsKey("pt_invoice_id"))
      item.pt_invoice_id = map["pt_invoice_id"];
    if(map.containsKey("payment_type"))
      item.payment_type = map["payment_type"];
    if(map.containsKey("payment_status"))
      item.payment_status = map["payment_status"];
    return item;
  }
}