import 'dart:collection';

class CartItem {
  String id = "";
  int qty = 0;
  double price = 0.0;
  String name = "";
  String lang_name = "";
  String manufacture_id = "";
  double unit_value = 0.0;
  String unit_name = "";
  double tax_percentage = 0.0;
  String image = "";
  String menu_type = "";
  String brand_origin = "";
  String country_id = "";
  String rowid = "";
  int subtotal = 0;
  String prod_id ="";
  String main_prod_id = "";
  String main_row_id = "";
  bool is_free = false;
  bool free = false;
  double retailer_price_without_discount = 0.0;
  double item_discount_percent = 0.0;
  String menu_id = "";
  double vat_on_this_item = 0;
  double item_final_total_with_vat = 0;

  static CartItem fromMap(LinkedHashMap<String, dynamic> map){
    CartItem item = new CartItem();
    if(map.containsKey("id"))
      item.id = map["id"];
    if(map.containsKey("qty"))
      item.qty = map["qty"];
    if(map.containsKey("price"))
      item.price = double.parse(map["price"]);
    if(map.containsKey("name"))
      item.name = map["name"];
    if(map.containsKey("lang_name"))
      item.lang_name = map["lang_name"];
    if(map.containsKey("manufacture_id"))
      item.manufacture_id = map["manufacture_id"];
    if(map.containsKey("unit_value"))
      item.unit_value = double.parse(map["unit_value"]);
    if(map.containsKey("tax_percentage"))
      item.tax_percentage = double.parse(map["tax_percentage"]);
    if(map.containsKey("image"))
      item.image = map["image"];
    if(map.containsKey("menu_type"))
      item.menu_type = map["menu_type"];
    if(map.containsKey("brand_origin"))
      item.brand_origin = map["brand_origin"];
    if(map.containsKey("country_id"))
      item.country_id = map["country_id"];
    if(map.containsKey("rowid"))
      item.rowid = map["rowid"];
    if(map.containsKey("subtotal"))
      item.subtotal = map["subtotal"];
    if(map.containsKey("prod_id"))
      item.prod_id = map["prod_id"];
    if(map.containsKey("main_prod_id"))
      item.main_prod_id = map["main_prod_id"];
    if(map.containsKey("main_row_id"))
      item.main_row_id = map["main_row_id"];
    if(map.containsKey("is_free"))
      item.is_free = map["is_free"].toString() == "1";
    if(map.containsKey("free"))
      item.free = map["free"].toString() == "1";
    if(map.containsKey("retailer_price_without_discount"))
      item.retailer_price_without_discount = double.parse(map["retailer_price_without_discount"]);
    if(map.containsKey("item_discount_percent"))
      item.item_discount_percent = double.parse(map["item_discount_percent"]);
    if(map.containsKey("menu_id"))
      item.menu_id = map["menu_id"];

    // sometimes double and sometimes int can't able to predict the response

    // if(map.containsKey("vat_on_this_item"))
    //   item.vat_on_this_item = double.parse(map["vat_on_this_item"]);
    // if(map.containsKey("item_final_total_with_vat"))
    //   item.item_final_total_with_vat = double.parse(map["item_final_total_with_vat"]);
    return item;
  }
}