import 'dart:collection';

class Offer {
  String id = "";
  String offer_name = "";
  String image = "";
  String mobile_image = "";
  String retailer_id = "";
  String country_id = "";
  String from_to = "";
  String to_date = "";
  String category_id = "";
  String status = "";
  String created_date = "";
  String updated_date = "";
  String URL = "";
  String ad_image = "";
  String ad_big_image = "";
  String ad_mobile_image = "";
  String ad_big_mobile_image = "";

  static Offer fromMap(LinkedHashMap<String, dynamic>map){
    var offer = Offer();
    if(map.containsKey("id"))
      offer.id = map["id"];
    if(map.containsKey("offer_name"))
      offer.offer_name = map["offer_name"];
    if(map.containsKey("image"))
      offer.image = map["image"];
    if(map.containsKey("mobile_image"))
      offer.mobile_image=map["mobile_image"];
    if(map.containsKey("retailer_id"))
      offer.retailer_id=map["retailer_id"];
    if(map.containsKey("country_id"))
      offer.country_id=map["country_id"];
    if(map.containsKey("from_to"))
      offer.from_to=map["from_to"];
    if(map.containsKey("to_date"))
      offer.to_date=map["to_date"];
    if(map.containsKey("category_id"))
      offer.category_id=map["category_id"];
    if(map.containsKey("status"))
      offer.status=map["status"];
    if(map.containsKey("created_date"))
      offer.created_date=map["created_date"];
    if(map.containsKey("updated_date"))
      offer.updated_date=map["updated_date"];
    if(map.containsKey("URL"))
      offer.URL=map["URL"];
    if(map.containsKey("ad_image"))
      offer.ad_image=map["ad_image"];
    if(map.containsKey("ad_big_image"))
      offer.ad_big_image=map["ad_big_image"];
    if(map.containsKey("ad_mobile_image"))
      offer.ad_mobile_image=map["ad_mobile_image"];
    if(map.containsKey("ad_big_mobile_image"))
      offer.ad_big_mobile_image=map["ad_big_mobile_image"];
    return offer;
  }
}