import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:ifada/model/Response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class User extends Response {
  String id = "";
  String first_name = "";
  String last_name = "";
  String email = "";
  String country_code = "";
  String mobile_number = "";
  String status = "";
  String is_verify = "";
  String otp = "";
  String is_profile_complete = "";
  String is_approved = "";
  String user_type = "";
  String role_id = "";
  String profile_image = "";
  String brand = "";
  String manufacturer_category = "";
  String type = "";
  String address = "";
  String street = "";
  String address_country = "";
  String city = "";
  String latitude = "";
  String longitude = "";
  String website = "";
  String contact_person = "";
  String phone = "";
  String referal_code = "";
  String total_loyalty_point = "";
  String retailer_credit_limit = "";
  String retailer_used_credit_limit = "";
  String commission_rate = "";
  String country_id = "";
  String created_date = "";
  String updated_date = "";
  String last_login = "";
  String added_by = "";
  String wallet_total_amount = "";
  String wallet_used_amount = "";
  String lang = "";
  String is_retailer_approved = "";
  String is_retailer_credit_approved = "";
  String retailer_online_payment = "";

  User fromJson(LinkedHashMap<String, dynamic> json){
    if(json.containsKey("id"))
      id = json["id"];
    if(json.containsKey("first_name"))
      first_name = json["first_name"];
    if(json.containsKey("last_name"))
      last_name = json["last_name"];
    if(json.containsKey("email"))
      email = json["email"];
    if(json.containsKey("country_code"))
      country_code = json["country_code"];
    if(json.containsKey("mobile_number"))
      mobile_number = json["mobile_number"];
    if(json.containsKey("status"))
      status = json["status"];
    if(json.containsKey("is_verify"))
      is_verify = json["is_verify"];
    if(json.containsKey("otp"))
      otp = json["otp"].toString();
    if(json.containsKey("is_profile_complete"))
      is_profile_complete = json["is_profile_complete"];
    if(json.containsKey("is_approved"))
      is_approved = json["is_approved"];
    if(json.containsKey("user_type"))
      user_type = json["user_type"];
    if(json.containsKey("role_id"))
      role_id = json["role_id"];
    if(json.containsKey("profile_image"))
      profile_image = json["profile_image"];
    if(json.containsKey("brand"))
      brand = json["brand"];
    if(json.containsKey("manufacturer_category"))
      manufacturer_category = json["manufacturer_category"];
    if(json.containsKey("type"))
      type = json["type"];
    if(json.containsKey("address"))
      address = json["address"];
    if(json.containsKey("street"))
      street = json["street"];
    if(json.containsKey("address_country"))
      address_country = json["address_country"];
    if(json.containsKey("city"))
      city = json["city"];
    if(json.containsKey("latitude"))
      latitude = json["latitude"];
    if(json.containsKey("longitude"))
      longitude = json["longitude"];
    if(json.containsKey("website"))
      website = json["website"];
    if(json.containsKey("contact_person"))
      contact_person = json["contact_person"];
    if(json.containsKey("phone"))
      phone = json["phone"];
    if(json.containsKey("referal_code"))
      referal_code = json["referal_code"];
    if(json.containsKey("total_loyalty_point"))
      total_loyalty_point = json["total_loyalty_point"];
    if(json.containsKey("retailer_credit_limit"))
      retailer_credit_limit = json["retailer_credit_limit"];
    if(json.containsKey("retailer_used_credit_limit"))
      retailer_used_credit_limit = json["retailer_used_credit_limit"];
    if(json.containsKey("commission_rate"))
      commission_rate = json["commission_rate"];
    if(json.containsKey("country_id"))
      country_id = json["country_id"];
    if(json.containsKey("created_date"))
      created_date = json["created_date"];
    if(json.containsKey("updated_date"))
      updated_date = json["updated_date"];
    if(json.containsKey("last_login"))
      last_login = json["last_login"];
    if(json.containsKey("added_by"))
      added_by = json["added_by"];
    if(json.containsKey("wallet_total_amount"))
      wallet_total_amount = json["wallet_total_amount"];
    if(json.containsKey("wallet_used_amount"))
      wallet_used_amount = json["wallet_used_amount"];
    if(json.containsKey("lang"))
      lang = json["lang"];
    if(json.containsKey("is_retailer_approved"))
      is_retailer_approved = json["is_retailer_approved"];
    if(json.containsKey("is_retailer_credit_approved"))
      is_retailer_credit_approved = json["is_retailer_credit_approved"];
    if(json.containsKey("retailer_online_payment"))
      retailer_online_payment = json["retailer_online_payment"];
    return this;
  }

  void save(SharedPreferences pref){
    pref.setString("id", id);
    pref.setString("first_name", first_name);
    pref.setString("last_name", last_name);
    pref.setString("email", email);
    pref.setString("country_code", country_code);
    pref.setString("mobile_number", mobile_number);
    pref.setString("status", status);
    pref.setString("is_verify", is_verify);
    pref.setString("is_profile_complete", is_profile_complete);
    pref.setString("is_approved", is_approved);
    pref.setString("user_type", user_type);
    pref.setString("role_id", role_id);
    pref.setString("profile_image", profile_image);
    pref.setString("brand", brand);
    pref.setString("manufacturer_category", manufacturer_category);
    pref.setString("type", type);
    pref.setString("address", address);
    pref.setString("street", street);
    pref.setString("address_country", address_country);
    pref.setString("city", city);
    pref.setString("latitude", latitude);
    pref.setString("longitude", longitude);
    pref.setString("website", website);
    pref.setString("contact_person", contact_person);
    pref.setString("phone", phone);
    pref.setString("referal_code", referal_code);
    pref.setString("total_loyalty_point", total_loyalty_point);
    pref.setString("retailer_credit_limit", retailer_credit_limit);
    pref.setString("retailer_used_credit_limit", retailer_used_credit_limit);
    pref.setString("commission_rate", commission_rate);
    pref.setString("country_id", country_id);
    pref.setString("created_date", created_date);
    pref.setString("updated_date", updated_date);
    pref.setString("last_login", last_login);
    pref.setString("added_by", added_by);
    pref.setString("wallet_total_amount", wallet_total_amount);
    pref.setString("wallet_used_amount", wallet_used_amount);
    pref.setString("lang", lang);
    pref.setString("is_retailer_approved", is_retailer_approved);
    pref.setString("is_retailer_credit_approved", is_retailer_credit_approved);
    pref.setString("retailer_online_payment", retailer_online_payment);
  }

  static User getUserFromPref(SharedPreferences pref){
    var user = new User();
    user.id = pref.getString("id");
    user.first_name = pref.getString("first_name");
    user.last_name = pref.getString("last_name");
    user.email = pref.getString("email");
    user.country_code = pref.getString("country_code");
    user.mobile_number = pref.getString("mobile_number");
    user.is_verify = pref.getString("is_verify");
    user.is_profile_complete = pref.getString("is_profile_complete");
    user.is_approved = pref.getString("is_approved");
    user.user_type = pref.getString("user_type");
    user.role_id = pref.getString("role_id");
    user.profile_image = pref.getString("profile_image");
    user.brand = pref.getString("brand");
    user.manufacturer_category = pref.getString("manufacturer_category");
    user.type = pref.getString("type");
    user.address = pref.getString("address");
    user.street = pref.getString("street");
    user.address_country = pref.getString("address_country");
    user.city = pref.getString("city");
    user.latitude = pref.getString("latitude");
    user.longitude = pref.getString("longitude");
    user.website = pref.getString("website");
    user.contact_person = pref.getString("contact_person");
    user.phone = pref.getString("phone");
    user.referal_code = pref.getString("referal_code");
    user.total_loyalty_point = pref.getString("total_loyalty_point");
    user.retailer_credit_limit = pref.getString("retailer_credit_limit");
    user.retailer_used_credit_limit = pref.getString("retailer_used_credit_limit");
    user.commission_rate = pref.getString("commission_rate");
    user.country_id = pref.getString("country_id");
    user.created_date = pref.getString("created_date");
    user.updated_date = pref.getString("updated_date");
    user.last_login = pref.getString("last_login");
    user.added_by = pref.getString("added_by");
    user.wallet_total_amount = pref.getString("wallet_total_amount");
    user.wallet_used_amount = pref.getString("wallet_used_amount");
    user.lang = pref.getString("lang");
    user.is_retailer_approved = pref.getString("is_retailer_approved");
    user.is_retailer_credit_approved = pref.getString("is_retailer_credit_approved");
    user.retailer_online_payment = pref.getString("retailer_online_payment");
    return user;
  }

  static clearUser(SharedPreferences pref){
    pref.remove("id");
    pref.remove("first_name");
    pref.remove("last_name");
    pref.remove("email");
    pref.remove("country_code");
    pref.remove("mobile_number");
    pref.remove("is_verify");
    pref.remove("is_profile_complete");
    pref.remove("is_approved");
    pref.remove("user_type");
    pref.remove("role_id");
    pref.remove("profile_image");
    pref.remove("brand");
    pref.remove("manufacturer_category");
    pref.remove("type");
    pref.remove("address");
    pref.remove("street");
    pref.remove("address_country");
    pref.remove("city");
    pref.remove("latitude");
    pref.remove("longitude");
    pref.remove("website");
    pref.remove("contact_person");
    pref.remove("phone");
    pref.remove("referal_code");
    pref.remove("total_loyalty_point");
    pref.remove("retailer_credit_limit");
    pref.remove("retailer_used_credit_limit");
    pref.remove("commission_rate");
    pref.remove("country_id");
    pref.remove("created_date");
    pref.remove("updated_date");
    pref.remove("last_login");
    pref.remove("added_by");
    pref.remove("wallet_total_amount");
    pref.remove("wallet_used_amount");
    pref.remove("lang");
    pref.remove("is_retailer_approved");
    pref.remove("is_retailer_credit_approved");
    pref.remove("retailer_online_payment");
  }
}