import 'dart:collection';

class BannerModel {
  String product_image = "";
  String category_id = "";
  static BannerModel fromJson(LinkedHashMap<String, dynamic>json) {
    var banner = BannerModel();
    if(json.containsKey("product_image"))
      banner.product_image = json["product_image"];
    if(json.containsKey("category_id"))
      banner.category_id = json["category_id"];
    return banner;
  }
}