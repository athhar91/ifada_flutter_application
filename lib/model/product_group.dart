import 'dart:collection';

import 'package:ifada/model/menu_item.dart';

class ProductGroup {
  String category_id = "";
  String product_id = "";
  String name = "";
  String url = "";
  String category_name = "";
  String category_url = "";
  String product_ids_array = "";
  List<MenuItem> product_data = [];

  static ProductGroup fromMap(LinkedHashMap<String, dynamic>map){
    var group = ProductGroup();
    if(map.containsKey("category_id"))
      group.category_id = map["category_id"];
    if(map.containsKey("product_id"))
      group.product_id = map["product_id"];
    if(map.containsKey("name"))
      group.name = map["name"];
    if(map.containsKey("url"))
      group.url = map["url"];
    if(map.containsKey("category_name"))
      group.category_name = map["category_name"];
    if(map.containsKey("category_url"))
      group.category_url = map["category_url"];
    if(map.containsKey("product_ids_array"))
      group.product_ids_array = map["product_ids_array"];
    if(map.containsKey("product_data")){
      (map["product_data"] as List).forEach((element) {
        group.product_data.add(MenuItem.fromMap(element));
      });
    }
    return group;
  }
}