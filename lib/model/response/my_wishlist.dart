import 'dart:collection';

import 'package:ifada/model/Response.dart';
import 'package:ifada/model/product.dart';
import 'package:ifada/model/wishlist_product.dart';

class MyWishList extends Response{
  String status = "";
  List<WishlistProduct> wishlist = [];
  @override
  fromJson(LinkedHashMap<String, dynamic> json) {
    if(json.containsKey("status"))
      status = json["status"];
    if(json.containsKey("wishlist")){
      (json["wishlist"] as List).forEach((element) {
        wishlist.add(WishlistProduct.fromMap(element));
      });
    }
    return this;
  }

}