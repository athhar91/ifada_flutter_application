import 'dart:collection';

import 'package:ifada/model/Response.dart';
import 'package:ifada/model/banner_model.dart';
import 'package:ifada/model/category.dart';
import 'package:ifada/model/country.dart';
import 'package:ifada/model/menu_item.dart';
import 'package:ifada/model/offer.dart';
import 'package:ifada/model/product_group.dart';

class HomeResponse extends Response {
  String status = "";
  List<BannerModel> banner = [];
  String default_banner = "";
  List<Offer> offer = [];
  List<MenuItem> best_picks_of_the_season = [];
  List<MenuItem> product_of_the_day = [];
  List<MenuItem> best_picks_of_the_featured_products = [];
  List<MenuItem> dairy_product = [];
  bool testinomial_data = false;
  bool banner_category = false;
  List<Category> popular_category = [];
  List<Country> country_data = [];
  String banner_category_image_path = "";
  Country country_name = null;
  List<ProductGroup> mobile_app_product = [];
  @override
  fromJson(LinkedHashMap<String, dynamic> json) {
    if(json.containsKey("status"))
      status = json["status"];
    if(json.containsKey("banner")&& json["banner"] is List){
      (json["banner"] as List).forEach((element) {
        banner.add(BannerModel.fromJson(element));
      });
    }
    if(json.containsKey("default_banner"))
      default_banner = json["default_banner"];
    if(json.containsKey("offer"))
      (json["offer"] as List).forEach((element) { 
        offer.add(Offer.fromMap(element));
      });
    if(json.containsKey("best_picks_of_the_season")){
      (json["best_picks_of_the_season"] as List).forEach((element) {
        best_picks_of_the_season.add(MenuItem.fromMap(element));
      });
    }
    if(json.containsKey("product_of_the_day")){
      (json["product_of_the_day"] as List).forEach((element) {
        product_of_the_day.add(MenuItem.fromMap(element));
      });
    }
    if(json.containsKey("best_picks_of_the_featured_products")){
      (json["best_picks_of_the_featured_products"] as List).forEach((element) {
        best_picks_of_the_featured_products.add(MenuItem.fromMap(element));
      });
    }
    if(json.containsKey("dairy_product"))
      (json["dairy_product"] as List).forEach((element) {
        dairy_product.add(MenuItem.fromMap(element));
      });
    //if(json.containsKey("testinomial_data"))
    //  testinomial_data = json["testinomial_data"];
    if(json.containsKey("banner_category"))
      banner_category = json["banner_category"];
    if(json.containsKey("popular_category"))
      (json["popular_category"] as List).forEach((element) {
        if(element is LinkedHashMap)
          popular_category.add(Category.fromJson(element));
        else if(element is List){
          (element as List).forEach((child) {
            popular_category.add(Category.fromJson(child));
          });
        }
      });
    if(json.containsKey("country_data"))
      (json["country_data"] as List).forEach((element) {
        country_data.add(Country.fromMap(element));
      });
    if(json.containsKey("banner_category_image_path"))
      banner_category_image_path = json["banner_category_image_path"];
    if(json.containsKey("country_name"))
      country_name = Country.fromMap(json["country_name"]);
    if(json.containsKey("mobile_app_product"))
      (json["mobile_app_product"] as List).forEach((element) {
        mobile_app_product.add(ProductGroup.fromMap(element));
      });
    return this;
  }
}