import 'dart:collection';
import 'package:ifada/model/cart_item.dart';
import 'package:ifada/model/Response.dart';
import 'package:ifada/model/product.dart';

class cart extends Response{

  List<CartItem> products = [];
  bool couponApplied = false;
  LinkedHashMap<String,CartItem> cart_data = new LinkedHashMap();
  int cartItemCount = 0;
  int subtotal = 0;
  int subtotalafterdiscount = 0;
  int manufacturer_count = 0;
  String image_path = "";
  int total_cart_discount_manufacturer_wise = 0;
  double tax_amount = 0.0;
  int deliveryFee = 0;
  double finalTotal = 0.0;
  double credit_amount = 0.0;
  String credit_invalid_product = "";
  bool credit_invalid = false;
  @override
  fromJson(LinkedHashMap<String, dynamic> json) {
    if(json.containsKey("is_coupon_applied"))
      couponApplied = json["is_coupon_applied"] == "1";
    if(json.containsKey("cart_item_count"))
      cartItemCount = json["cart_item_count"];
    if(json.containsKey("image_path"))
      image_path = json["image_path"];
    if(json.containsKey("cart_data")){
      (json["cart_data"] as LinkedHashMap<String, dynamic>).keys.forEach((element) {
        var product = CartItem.fromMap(json["cart_data"][element] as LinkedHashMap<String, dynamic>);
        product.image = image_path + product.image;
        products.add(product);
        cart_data[element] = product;
      });
    }
    if(json.containsKey("manufacturer_count"))
      manufacturer_count = json["manufacturer_count"];
    if(json.containsKey("subtotal"))
      subtotal = json["subtotal"];
    if(json.containsKey("subtotalafterdiscount"))
      subtotalafterdiscount = json["subtotalafterdiscount"];
    if(json.containsKey("total_cart_discount_manufacturer_wise"))
      total_cart_discount_manufacturer_wise = json["total_cart_discount_manufacturer_wise"];
    if(json.containsKey("tax_amount"))
      tax_amount = json["tax_amount"];
    if(json.containsKey("deliveryFee"))
      deliveryFee = json["deliveryFee"];
    if(json.containsKey("finalTotal"))
      finalTotal = json["finalTotal"];
    if(json.containsKey("credit_amount"))
      credit_amount = json["credit_amount"];
    if(json.containsKey("credit_invalid_product"))
      credit_invalid_product = json["credit_invalid_product"];
    if(json.containsKey("credit_invalid"))
      credit_invalid = json["credit_invalid"] == "1";
    return this;
  }

}