import 'dart:collection';

import 'package:ifada/model/order.dart';

import '../Response.dart';

class MyOrderResponse extends Response{
  String status = "";
  List<Order> order_list = [];
  String total_row = "";

  @override
  fromJson(LinkedHashMap<String, dynamic> json) {
    if(json.containsKey("status")){
      status = json["status"];
    }
    if(json.containsKey("order_list")){
      var orderList = (json["order_list"] as LinkedHashMap<String,dynamic>);
      if(orderList.containsKey("total_row"))
        total_row = orderList["total_row"];
      if(orderList.containsKey("result")){
        (orderList["result"] as List).forEach((element) {
          order_list.add(Order.fromMap(element));
        });
      }
    }
    return this;
  }

}