import 'dart:collection';

import 'package:ifada/model/Response.dart';
import 'package:ifada/model/category.dart';

class AllCategories extends Response{

  String status = "";
  List<Category> category = [];

  @override
  fromJson(LinkedHashMap<String, dynamic> json) {
    if(json.containsKey("status"))
      status = json["status"];
    if(json.containsKey("category")){
      (json["category"] as List).forEach((element) {
        category.add(Category.fromJson(element as LinkedHashMap<String, dynamic>));
      });
    }
    return this;
  }

}