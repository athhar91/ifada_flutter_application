import 'dart:collection';

import 'package:ifada/model/Response.dart';
import 'package:ifada/model/my_address_item.dart';

class MyAddresses extends Response {
  LinkedHashMap<String,MyAddressesItem> my_address = new LinkedHashMap();

  @override
  fromJson(LinkedHashMap<String, dynamic> json) {
    if(json.containsKey("my_address")){
      (json["my_address"] as LinkedHashMap<String, dynamic>).keys.forEach((element) {
        var product = MyAddressesItem.fromMap(json["my_address"][element] as LinkedHashMap<String, dynamic>);
        my_address[element] = product;
      });
    }
    return this;
  }
}
