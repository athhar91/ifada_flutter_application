import 'dart:collection';

import 'package:ifada/model/Response.dart';

class BasicResponse extends Response {
  String responseCode = "";
  String responseStatus = "";
  dynamic responseMessage = null;
  dynamic data = null;
  @override
  fromJson(LinkedHashMap<String, dynamic> json) {
    if(json.containsKey("responseCode"))
      responseCode = json["responseCode"];
    if(json.containsKey("responseStatus"))
      responseStatus = json["responseStatus"];
    if(json.containsKey("responseMessage"))
      responseMessage = json["responseMessage"];
    if(json.containsKey("data"))
      data = json["data"];
    return this;
  }

}