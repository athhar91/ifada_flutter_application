import 'dart:collection';
import 'package:ifada/model/category_product.dart';

import '../Response.dart';

class ProductListResponse extends Response {

  CategoryProduct category_product;

  @override
  fromJson(LinkedHashMap<String, dynamic> json) {
    if(json.containsKey("category_product")){
      category_product = CategoryProduct.fromMap(json["category_product"]);
    }
    return this;
  }

}