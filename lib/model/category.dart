import 'dart:collection';

class Category {
  String id = "";
  String parent = "";
  String name = "";
  String image = "";
  String banner_image = "";
  String url = "";
  String status = "";
  String created_date = "";
  String updated_date = "";
  String lang = "";
  String is_selected_for_banner = "";
  String is_selected_for_popular = "";
  String cat_image = "";
  List<Category> sub = [];

  static Category fromJson(LinkedHashMap<String, dynamic>json) {
    var category = Category();
    if(json.containsKey("id"))
      category.id = json["id"];
    if(json.containsKey("parent"))
      category.parent = json["parent"];
    if(json.containsKey("name"))
      category.name = json["name"];
    if(json.containsKey("image"))
      category.image = json["image"];
    if(json.containsKey("banner_image"))
      category.banner_image = json["banner_image"];
    if(json.containsKey("url"))
      category.url = json["url"];
    if(json.containsKey("status"))
      category.status = json["status"];
    if(json.containsKey("created_date"))
      category.created_date = json["created_date"];
    if(json.containsKey("updated_date"))
      category.updated_date = json["updated_date"];
    if(json.containsKey("lang"))
      category.lang = json["lang"];
    if(json.containsKey("is_selected_for_banner"))
      category.is_selected_for_banner = json["is_selected_for_banner"];
    if(json.containsKey("is_selected_for_popular"))
      category.is_selected_for_popular = json["is_selected_for_popular"];
    if(json.containsKey("cat_image"))
      category.cat_image = json["cat_image"];
    if(json.containsKey("sub")){
      (json["sub"] as List).forEach((element) {
        category.sub.add(Category.fromJson(element as LinkedHashMap<String,dynamic>));
      });
    }
    return category;
  }
}
