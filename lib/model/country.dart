import 'dart:collection';

class Country {
  String name = "";
  String id = "";
  String currency = "";
  String symbol = "";
  String country_code = "";
  String status = "";
  String address = "";
  String latitude = "";
  String longitude = "";
  String created_date = "";
  String updated_date = "";
  String lang = "";
  String image = "";


  static Country fromMap(LinkedHashMap<String, dynamic>map){
    var country = Country();
    if(map.containsKey("name"))
      country.name = map["name"];
    if(map.containsKey("id"))
      country.id = map["id"];
    if(map.containsKey("currency"))
      country.currency = map["currency"];
    if(map.containsKey("symbol"))
      country.symbol = map["symbol"];
    if(map.containsKey("country_code"))
      country.country_code = map["country_code"];
    if(map.containsKey("status"))
      country.status = map["status"];
    if(map.containsKey("address"))
      country.address = map["address"];
    if(map.containsKey("latitude"))
      country.latitude = map["latitude"];
    if(map.containsKey("longitude"))
      country.longitude = map["longitude"];
    if(map.containsKey("created_date"))
      country.created_date = map["created_date"];
    if(map.containsKey("updated_date"))
      country.updated_date = map["updated_date"];
    if(map.containsKey("lang"))
      country.lang = map["lang"];
    if(map.containsKey("image"))
      country.image = map["image"];
    return country;
  }
}