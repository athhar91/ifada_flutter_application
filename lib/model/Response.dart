import 'dart:collection';

abstract class Response<T> {
  T fromJson(LinkedHashMap<String, dynamic> json);
}