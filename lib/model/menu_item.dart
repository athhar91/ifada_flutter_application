import 'dart:collection';

import 'package:ifada/model/menu_varient.dart';
import 'package:ifada/model/product.dart';

class MenuItem {
  String id = "";
  String menu_name = "";
  String image = "";
  String MANUFACTURER_ID = "";
  String lang = "";
  String product_image = "";
  String product = "";
  MenuVarient menu_varient_data=null;
  static MenuItem fromMap(LinkedHashMap<String, dynamic>map){
    MenuItem item = MenuItem();
    if(map.containsKey("id"))
      item.id = map["id"];
    if(map.containsKey("menu_name"))
      item.menu_name = map["menu_name"];
    if(map.containsKey("image"))
      item.image = map["image"];
    if(map.containsKey("product_image"))
      item.product_image = map["product_image"];
    if(map.containsKey("product"))
      item.product = map["product"];
    if(map.containsKey("lang"))
      item.lang = map["lang"];
    if(map.containsKey("MANUFACTURER_ID"))
      item.MANUFACTURER_ID = map["MANUFACTURER_ID"];
    if(map.containsKey("menu_varient_data"))
      item.menu_varient_data = MenuVarient.fromMap(map["menu_varient_data"] as LinkedHashMap<String, dynamic>);
    return item;
  }

  Product toProduct(){
    var product = Product();
    product.id = id;
    product.menu_name = menu_name;
    product.image = product_image;
    product.lang = lang;
    product.product_varient.add(menu_varient_data);
    return product;
  }
}