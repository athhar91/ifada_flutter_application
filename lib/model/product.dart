import 'dart:collection';

import 'package:ifada/model/menu_varient.dart';

class Product {
  String id = "";
  String user_id = "";
  String menu_category_id = "";
  String menu_sub_category_id = "";
  String menu_retailer_id = "";
  String menu_type = "";
  String menu_name = "";
  String brand_origin = "";
  String content = "";
  String description = "";
  String image = "";
  String status = "";
  String is_approve = "";
  String country_id = "";
  String is_in_stock = "";
  String quantity = "";
  String tax_amount = "";
  String keyword = "";
  String created_date = "";
  String updated_date = "";
  String lang = "";
  String currency = "";

  List<MenuVarient> product_varient = [];

  static Product fromMap(LinkedHashMap<String, dynamic>map){
    Product product = new Product();
    if(map.containsKey("id"))
      product.id = map["id"];
    if(map.containsKey("user_id"))
      product.user_id = map["user_id"];
    if(map.containsKey("menu_category_id"))
      product.menu_category_id = map["menu_category_id"];
    if(map.containsKey("menu_sub_category_id"))
      product.menu_sub_category_id = map["menu_sub_category_id"];
    if(map.containsKey("menu_retailer_id"))
      product.menu_retailer_id = map["menu_retailer_id"];
    if(map.containsKey("menu_type"))
      product.menu_type = map["menu_type"];
    if(map.containsKey("menu_name"))
      product.menu_name = map["menu_name"];
    if(map.containsKey("brand_origin"))
      product.brand_origin = map["brand_origin"];
    if(map.containsKey("content"))
      product.content = map["content"];
    if(map.containsKey("description"))
      product.description = map["description"];
    if(map.containsKey("image"))
      product.image = map["image"];
    if(map.containsKey("status"))
      product.status = map["status"];
    if(map.containsKey("is_approve"))
      product.is_approve = map["is_approve"];
    if(map.containsKey("country_id"))
      product.country_id = map["country_id"];
    if(map.containsKey("is_in_stock"))
      product.is_in_stock = map["is_in_stock"];
    if(map.containsKey("quantity"))
      product.quantity = map["quantity"];
    if(map.containsKey("tax_amount"))
      product.tax_amount = map["tax_amount"];
    if(map.containsKey("keyword"))
      product.keyword = map["keyword"];
    if(map.containsKey("created_date"))
      product.created_date = map["created_date"];
    if(map.containsKey("updated_date"))
      product.updated_date = map["updated_date"];
    if(map.containsKey("lang"))
      product.lang = map["lang"];

    if(map.containsKey("product_varient")){
      if(map["product_varient"] is List) {
        (map["product_varient"] as List).forEach((element) {
          product.product_varient.add(MenuVarient.fromMap(element));
        });
      } else if(map["product_varient"] is LinkedHashMap){
        product.product_varient.add(MenuVarient.fromMap(map["product_varient"]));
      }
    }
    return product;
  }
}