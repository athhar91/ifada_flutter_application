import 'package:flutter/material.dart';
import 'package:ifada/helpers/dialog_helper.dart';

bool isNullOrEmpty(String string) {
  return string == null || string == "";
}

bool isNullOrWhiteSpace(String string) {
  return string == null || string.trim().isEmpty;
}

bool isEqualIgnored(String s1, String s2) {
  return s1.toLowerCase() == s2.toLowerCase();
}

final intRegex = RegExp(r'\s+(\d+)\s+', multiLine: false);

bool isNumber(String text) {

  var myInt = double.tryParse(text);
  print("$text->$myInt");
  return myInt != null && myInt >= 0;
}

bool isPhoneNumber(String text){
  if(text == "+")
    return true;
  var value = text;
  if(text.startsWith("+"))
    value = value.replaceFirst("+", "");
  return isNumber(value);
}

double getNumber(String text) {
  var myInt = double.tryParse(text);
  return myInt != null ? myInt : 0;
}

bool isValidEmail(String email) {
  String p = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
      + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})\$";
  RegExp regExp = new RegExp(p);
  return regExp.hasMatch(email);
}

void showAlertMessage(String message, BuildContext context,
    {String title = "ALERT:"}) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        shape:
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Container(
              margin: EdgeInsets.only(top: 20, left: 15),
              child: Text(
                title,
                style: new TextStyle(
                  color: DialogHelper.themeColor,
                  fontFamily: 'Product Sans',
                  fontSize: 17.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 15,right: 15, left: 15),
              child: new Text(
                message,
                style: new TextStyle(
                  color: Colors.black54,
                  fontFamily: 'Product Sans',
                  fontSize: 15.0,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              alignment: Alignment.bottomRight,
              child: InkWell(
                splashColor: Colors.transparent,
                focusColor: Colors.transparent,
                highlightColor: Colors.transparent,
                child: new Container(
                  margin: EdgeInsets.only(right: 10,bottom: 10, left: 10),
                  child: Text(
                    "OK",
                    style: new TextStyle(
                      color: DialogHelper.themeColor,
                      fontFamily: 'Product Sans',
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),),
                onTap: () {
                  Navigator.of(context, rootNavigator: true).pop();
                },
              ),
            )
          ],
        ),
      );
    },
  );
}
