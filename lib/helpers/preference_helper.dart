import 'package:ifada/helpers/app_data_helper.dart';
import 'package:ifada/model/User.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenceHelper {
  static SharedPreferences sharedPref;

  static Future<SharedPreferences> getPref() async {
    if(sharedPref!=null)
      return sharedPref;
    return await SharedPreferences.getInstance();
  }

  static void saveUserData(User user) async {
    AppDataHelper.user = user;
    var pref = await getPref();
    user.save(pref);
  }

  static Future<User> getUserData() async {
    var pref = await getPref();
    AppDataHelper.user = User.getUserFromPref(pref);
    return AppDataHelper.user;
  }

  static clearUser() async {
    var pref = await getPref();
    User.clearUser(pref);
    AppDataHelper.user = null;
  }
}