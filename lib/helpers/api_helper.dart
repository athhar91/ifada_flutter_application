import 'dart:collection';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:ifada/helpers/preference_helper.dart';
import 'package:ifada/model/Response.dart';
import 'package:ifada/model/menu_item.dart';
import 'package:ifada/model/menu_varient.dart';
import 'package:ifada/model/response/BasicResponse.dart';
typedef S ItemCreator<S>();

class ApiHelper<T extends Response> {

  ItemCreator<T> creator;
  Function(T) onSuccess;
  Function(String) onFailure;
  ApiHelper(this.onSuccess,this.onFailure,this.creator){

  }

  static String hostURL = "http://mirnahshop.com/ats/webservices/";
  void login(String phone, String password){
    var body = {
      "login_mobile_number":phone,
      "login_password":password,
      "language_code":"en"
    };
    makePostReq(hostURL+"auth/login", body, onSuccess,onFailure);
  }

  void signup(String f_name,String l_name,String email,String password,String cc, String ph,{String referal = "",bool retailer = false}){
    var body = {
      "first_name":f_name,
      "last_name":l_name,
      "email":email,
      "password":password,
      "mobile_number":ph,
      "language_code":"en",
      "country_code":"cc",
      "referal_code":referal,
      "i_am_retailer":retailer.toString()
    };
    makePostReq(hostURL+"auth/registration", body, onSuccess,onFailure);
  }

  void forgetPassword(String phone){
    var body = {
      "mobile_number_varify":phone,
      "language_code":"en"
    };
    makePostReq(hostURL+"auth/forget", body, onSuccess,onFailure);
  }

  void getMyAddress(String userId){
    var body = {
      "country_id":"1",
      "user_id": userId,
    };
    makePostReq(hostURL + "/my_address", body, onSuccess, onFailure);
  }

  void getCart(String userId){
    var body = {
      "country_id":"1",
      "user_id": userId,
      "language_code":"en"
    };
    makePostReq(hostURL + "cart/checkout", body, onSuccess, onFailure);
  }

  void addToCart(MenuItem menuItem,String userId){
    var body = {
      "user_type":"4",
      "country_id":"1",
      "user_id": userId,
      "manufacture_id":menuItem.MANUFACTURER_ID,
      "menu_id":menuItem.id,
      "menu_varient_id":menuItem.menu_varient_data.id
    };
    makePostReq(hostURL+"cart/add_cart", body, onSuccess,onFailure);
  }

  void addToWishList(MenuVarient menuVarient,String userId){
    var body = {
      "country_id":"1",
      "user_id": userId,
      "menu_varient_id":menuVarient.id,
      "Is_for":"add"
    };
    makePostReq(hostURL+"add_wishlist", body, onSuccess, onFailure);
  }

  void deleteFromWishList(MenuVarient menuVarient,String userId){
    var body = {
      "user_id": userId
    };
    makePostReq(hostURL+"add_wishlist", body, onSuccess, onFailure);
  }

  void getWishList(String userId){
    var body = {
      "country_id":"1",
      "user_id": userId,
      "language_code":"en"
    };
    makePostReq(hostURL+"wishlist", body, onSuccess, onFailure);
  }

  void changePassword(String oldPasssword, String password, String confirm_new,String user_id){
  }

  void getCategories(){
    var body = {
      "country_id":"1",
      "language_code":"en"
    };
    makePostReq(hostURL+"category_api", body, onSuccess,onFailure);
  }

  void getHomePage({String userId = ""}){
    var body = {
      "country_id":"1",
      "language_code":"en",
      "user_id":userId
    };
    makePostReq(hostURL+"home_page", body, onSuccess,onFailure);
  }

  void getProducts(String catId){
    var body = {
      "country_id":"1",
      "language_code":"en",
      "category_id": catId
    };
    makePostReq(hostURL+"product_list", body, onSuccess,onFailure);
  }

  void makePostReq(String url,dynamic body, Function(T) onSuccess,Function(String) onFailure) async{
    var response = null;
    try {
      response = await http.post(url, headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }, body: body, encoding: Encoding.getByName("utf-8"));
    } catch (e){
      onFailure("");
    }
    if(response!=null){
      var stauscode = response.statusCode;
      if(stauscode == 200){
        var resBody = jsonDecode(response.body) as LinkedHashMap<String, dynamic>;
        if(resBody.containsKey("responseCode")&&(resBody["responseCode"].toString()) == "200"){
          if(T == BasicResponse){
            onSuccess(creator().fromJson(resBody));
          } else {
            onSuccess(creator().fromJson(resBody["data"] as LinkedHashMap<String, dynamic>));
          }
        } else if(resBody.containsKey("responseMessage")){
          if(resBody["responseMessage"] is String)
            onFailure(resBody["responseMessage"]);
          else if (resBody["responseMessage"] is LinkedHashMap && (resBody["responseMessage"] as LinkedHashMap<String, dynamic>).containsKey("error_msg")){
            onFailure((resBody["responseMessage"] as LinkedHashMap<String, dynamic>)["error_msg"]);
          }
        } else {
          onFailure("");
        }
      } else {
        onFailure("");
      }
    }
  }
}