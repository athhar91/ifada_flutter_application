import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:ifada/helpers/app_color.dart';
import 'package:ifada/model/country_code.dart';

import 'navigation_helper.dart';
import 'preference_helper.dart';

class DialogHelper {
  static GlobalKey<State> key = new GlobalKey<State>();
  static Color themeColor = Colors.red;
  static LinearGradient linearGradient = LinearGradient(
    colors: <Color>[Colors.yellowAccent, Colors.red, Colors.purple],
  );
  static Shader gradientShader = LinearGradient(
    colors: <Color>[Colors.yellowAccent, Colors.red, Colors.purple],
  ).createShader(Rect.fromLTWH(0.0, 0.0, 400.0, 1000.0));
  static String logo = 'assets/heart.png';

  static Future<void> showLoadingDialog(
      BuildContext context, bool dismissible) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: dismissible,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)),
                  elevation: 2,
                  key: key,
                  backgroundColor: Colors.transparent,
                  children: <Widget>[
                    Center(
                        child: SpinKitCircle(
                      color: AppColor.appYellow,
                      size: 90,
                    ))
                  ]));
        });
  }

  static void closeLoadingDialog() {
    if (key.currentContext != null) Navigator.of(key.currentContext).pop();
  }

  static void showCountryCodePicker(
      BuildContext context, String selected, ValueChanged<String> onChanged) {
    showModalBottomSheet(
        context: context,
        barrierColor: Colors.transparent,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(30.0))),
        builder: (BuildContext context) {
          var countries = CountryCode.getCountries();
          return Container(
            padding: EdgeInsets.all(20),
            //decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.only(topLeft: Radius.circular(40.0),topRight: Radius.circular(40.0))),
            height: 300,
            child: Center(
              child: ListView.builder(
                  itemCount: countries.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      child: Container(
                          padding: EdgeInsets.all(10),
                          child: Row(children: [
                            Image.asset(
                              countries[index].flag,
                              height: 20,
                              width: 20,
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 20),
                              child: Text(countries[index].cc),
                              width: 100,
                            ),
                            Expanded(
                              child: Text(countries[index].country),
                              flex: 1,
                            ),
                            Visibility(
                              child: Image.asset(
                                "assets/tick_black.png",
                                height: 20,
                                width: 20,
                                color: Colors.blue,
                              ),
                              visible: selected == countries[index].cc,
                            ),
                          ])),
                      onTap: () {
                        onChanged(countries[index].cc);
                        Navigator.pop(context);
                      },
                    );
                  }),
            ),
          );
        });
  }

  // Popup for Logout

  static logoutAlertDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return Expanded(
            flex: 1,
            child: Center(
              child: Container(
                height: 120,
                width: double.infinity,
                margin: EdgeInsets.only(left: 35, right: 35),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5)),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 30,
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 10, left: 65,),
                      child: Text(
                        "Do you want to logout ?",
                        style: TextStyle(
                            decoration: TextDecoration.none, //Removes the yellow line
                            color: Color(0xff3742FE),
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500,
                            fontSize: 22),
                      ),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(
                                  left: 40, right: 40, top: 35, bottom: 5),
                              height: 30,
                              width: 70,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Color(0xff3742FE)),
                              child: RaisedButton(
                                color: Color(0xff3742FE),
                                onPressed: () {
                                  //Navigator.push(context, route);
                                  PreferenceHelper.clearUser();
                                  NavigationHelper.launchNewScreen(context, NavigationHelper.loginRoute);
                                },
                                child: Text(
                                  "YES",
                                  style: TextStyle(
                                      decoration: TextDecoration.none,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "Poppins",
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          Container(
                            margin: EdgeInsets.only(
                                left: 80, right: 30, top: 35, bottom: 5),
                            height: 30,
                            width: 70,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Color(0xff3742FE)),
                            child: RaisedButton(
                                color: Color(0xff3742FE),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text(
                                  "NO",
                                  style: TextStyle(
                                      decoration: TextDecoration.none,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "Poppins",
                                      color: Colors.white),
                                )),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  static void showAlertDialog(String title, String content, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Html(data : content),
          actions: <Widget>[
            FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
