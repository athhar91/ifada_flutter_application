import 'package:flutter/cupertino.dart';

class AppColor {
  static Color bg = Color(0xffF4F4F4);
  static Color appYellow = Color(0xfff8cb00);
  static Color appBlue = Color(0xff3742FA);
  static Color darkText = Color(0xff001833);
  static Color bodyText = Color(0xff8D8F92);
  static Color shadowColor = Color(0xcdeeeeee);
}