import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:ifada/screens/Products_screen.dart';
import 'package:ifada/screens/add_address_screen.dart';
import 'package:ifada/screens/category_screen.dart';
import 'package:ifada/screens/main_screen.dart';
import 'package:ifada/screens/login_screen.dart';
import 'package:ifada/screens/myOrders_screen.dart';
import 'package:ifada/screens/my_address_screen.dart';
import 'package:ifada/screens/my_rewards.dart';
import 'package:ifada/screens/track_order_screen.dart';
import 'package:ifada/screens/my_account.dart';
import 'package:ifada/screens/my_cart.dart';
import 'package:ifada/screens/productdetails_screen.dart';
import 'package:ifada/screens/signup_screen.dart';
import 'package:ifada/screens/otpverification_screen.dart';
import 'package:ifada/screens/reset_password_screen.dart';
import 'package:ifada/screens/recovery_password_screen.dart';
import 'package:ifada/screens/otp_For_SignUp.dart';
import 'package:ifada/screens/splash_screen.dart';
import 'package:ifada/screens/sub_category_screen.dart';
import 'package:ifada/screens/sub_category_details_screen.dart';
import 'package:ifada/screens/home_screen.dart';
import 'package:ifada/screens/order_details_screen.dart';
import 'package:ifada/screens/rate_the_product_screen.dart';
import 'package:ifada/screens/orderCancel_screen.dart';
import 'package:ifada/screens/orderConfirm_screen.dart';
import 'package:ifada/screens/myOrders_screen.dart';

import '../screens/sub_category_screen.dart';

class NavigationHelper {
  static const String initialRoute = "/";
  static const String loginRoute = "/login";
  static const String myAccountRoute = "/myAccount";
  static const String signupRote = "/signup";
  static const String otpRoute = "/otp";
  static const String resetroute = "/reset";
  static const String recoveryroute = "/recovery";
  static const String otpSignuproute = "/otpSignup";
  static const String categoryroute = "/category";
  static const String productsroute = "/products";
  static const String productdetailsroute = "/productDetails";
  static const String subCategoryroute = "/subCategory";
  static const String subCategoryDetailsroute = "/subCategoryDetail";
  static const String homeScreenroute = "/homeScreen";
  static const String myOrdersroute = "/myorders";
  static const String trackOrderroute = "/trackOrder";
  static const String orderDetailsroute = "/orderDetails";
  static const String orderCancelroute = "/orderCancel";
  static const String orderConfirmroute = "/orderConfirm";
  static const String myCartroute = "/mycart";
  static const String rateProductroute = "/rateTheProduct";
  static const String homeScreen = "/home";
  static const String myReward = "/myReward";
  static const String myAddress = "/myAddress";
  static const String addAddress = "/addAddress";
  static String _navigation = initialRoute;
  static void screenWillPop(String name) {
    print("screenWillPop: $name");
    if (_navigation.endsWith(name)) {
      _navigation = _navigation.replaceRange(
          _navigation.length - name.length, _navigation.length, "");
    }
    if (_navigation.isEmpty) _navigation = "/";
  }

  static Object openScreen(BuildContext context, String name,
      {HashMap args, bool forceNew = false, Function result}) async {
    bool containAlready = _navigation.contains(name);
    if (forceNew || !containAlready) {
      print("pushNamed($context, $name, $args");
      if (_navigation == "/")
        _navigation = name;
      else
        _navigation += name;
      var result = await Navigator.pushNamed(context, name, arguments: args);
      return result;
    } else {
      print("popNamed($context, $name, $args");
      var names = _navigation.split("/");
      int popCount = names.length - names.indexOf(name.replaceAll("/", "")) - 1;
      for (int i = 0; i < popCount; i++) {
        if (i == popCount - 1 && args != null)
          Navigator.pop(context, args);
        else
          Navigator.pop(context);
        if (names[names.length - i - 1].isNotEmpty)
          screenWillPop("/" + names[names.length - i - 1]);
      }
    }
    return null;
  }

  static void onBackPress(BuildContext context,  {HashMap args = null}) {
    print("onBackPress");
    var names = _navigation.split("/");
    if(args != null)
      Navigator.pop(context, args);
    else Navigator.pop(context);
    if (names[names.length - 1].isNotEmpty)
      screenWillPop("/" + names[names.length - 1]);
  }

  static void launchNewScreen(BuildContext context, String name, {Map args}) {
    print("launchNewScreen");
    _navigation = "/";
    Navigator.of(context).pushNamedAndRemoveUntil(
        name, (Route<dynamic> route) => false, arguments: args);
    _navigation = _navigation + name;
  }

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case NavigationHelper.initialRoute:
        return MaterialPageRoute(
            builder: (context) => SplashScreen(settings.name));
      case NavigationHelper.loginRoute:
        return MaterialPageRoute(
            builder: (context) => LoginScreen(settings.name));
      case NavigationHelper.myAccountRoute:
        return MaterialPageRoute(
            builder: (context) => MyAccount(settings.name));
      case NavigationHelper.signupRote:
        return MaterialPageRoute(
            builder: (context) => SignUpScreen(settings.name));
      case NavigationHelper.otpRoute:
        return MaterialPageRoute(
            builder: (context) => OtpVerification(settings.name,settings.arguments));
      case NavigationHelper.resetroute:
        return MaterialPageRoute(
            builder: (context) => ResetPassword(settings.name));
      case NavigationHelper.recoveryroute:
        return MaterialPageRoute(
            builder: (context) => RecoveryPassword(settings.name));
      case NavigationHelper.otpSignuproute:
        return MaterialPageRoute(
            builder: (context) => OtpForSignUp(settings.name,settings.arguments));
      case NavigationHelper.categoryroute:
        return MaterialPageRoute(
            builder: (context) => CategoryScreen(settings.name));
        case NavigationHelper.myOrdersroute:
      return MaterialPageRoute(
          builder: (context) => MyOrdersScreen(settings.name));
      case NavigationHelper.trackOrderroute:
        return MaterialPageRoute(
            builder: (context) => TrackOrderScreen(settings.name));
      case NavigationHelper.orderDetailsroute:
        return MaterialPageRoute(
            builder: (context) => OrderDetailsScreen(settings.name));
      case NavigationHelper.orderCancelroute:
        return MaterialPageRoute(
            builder: (context) => OrderCancelScreen(settings.name));
      case NavigationHelper.orderConfirmroute:
        return MaterialPageRoute(
            builder: (context) => OrderConfirmScreen(settings.name));
      case NavigationHelper.productsroute:
        return MaterialPageRoute(
            builder: (context) => ProductsScreen(settings.name));
      case NavigationHelper.productdetailsroute:
        return MaterialPageRoute(
            builder: (context) => ProductDetails(settings.name,settings.arguments));
      case NavigationHelper.myCartroute:
        return MaterialPageRoute(
            builder: (context) => MyCart(settings.name));
      case NavigationHelper.rateProductroute:
        return MaterialPageRoute(
            builder: (context) => RateTheProduct(settings.name));
      case NavigationHelper.subCategoryroute:
        return MaterialPageRoute(
            builder: (context) => SubCategoryScreen(settings.name,settings.arguments));
      case NavigationHelper.subCategoryDetailsroute:
        return MaterialPageRoute(
            builder: (context) => SubCategoryDetailsScreen(settings.name,settings.arguments));
      case NavigationHelper.homeScreenroute:
        return MaterialPageRoute(
            builder: (context) => HomeScreen(settings.name));
      case NavigationHelper.homeScreen:
        return MaterialPageRoute(
            builder: (context) => MainScreen(settings.name));
      case NavigationHelper.myReward:
        return MaterialPageRoute(
            builder: (context) => MyRewards(settings.name));
      case NavigationHelper.myAddress:
        return MaterialPageRoute(
            builder: (context) => MyAddressScreen(settings.name));
      case NavigationHelper.addAddress:
        return MaterialPageRoute(
            builder: (context) => AddAddress(settings.name));
    }
  }
}