import '../model/User.dart';
import 'package:ifada/model/response/home_response.dart';
import 'package:ifada/model/response/all_categories.dart';
class AppDataHelper {
  static User user = null;
  static HomeResponse homeRespone = null;
  static AllCategories allCategories = null;
}