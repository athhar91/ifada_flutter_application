import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ifada/helpers/api_helper.dart';
import 'package:ifada/model/response/product_list_response.dart';
import 'package:ifada/widgets/product_widget.dart';
import 'package:shimmer/shimmer.dart';

import '../helpers/navigation_helper.dart';
import '../model/category.dart';
import 'state_screen.dart';

class SubCategoryDetailsScreen extends StateScreen {
  Category category;

  SubCategoryDetailsScreen(String name, Object args) : super(name) {
    if (args is Map) {
      if (args.containsKey("category")) category = args["category"];
    }
  }

  @override
  SubCategoryDetailsScreenState getState() =>
      new SubCategoryDetailsScreenState();
}

class SubCategoryDetailsScreenState
    extends ScreenState<SubCategoryDetailsScreen> {
  var selectedIndex = 0;
  var selectedName = "All";
  ProductListResponse response = null;

  @override
  void initState() {
    super.initState();
    getProducts(widget.category.id);
  }

  getProducts(String id) async {
    ApiHelper<ProductListResponse>(
        (res) => {
              setState(() {
                response = res;
              })
            },
        (error) => {},
        () => new ProductListResponse()).getProducts(id);
  }

  // int selectedIndex;

  // SubCategoryDetailsScreen(String name, Object args) : super(name) {
  //   if (args is Map) {
  //     if (args.containsKey("category")) category = args["category"];
  //   }
  // }

  _showPopupToSort(context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20.0)),
        ),
        builder: (builder) {
          return new Container(
            height: 200.0,
            color: Colors.transparent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: double.infinity,
                  height: 50,
                  padding: EdgeInsets.only(top: 17, left: 20),
                  decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(width: 2, color: Color(0xffEEEEEE))),
                  ),
                  child: Text(
                    "SORT BY",
                    style: TextStyle(
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      color: Color(0xff001833),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Color(0xffF4F4F4),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              widget.openScreen(
                                  NavigationHelper.subCategoryroute);
                            },
                            child: Container(
                              margin: EdgeInsets.only(
                                top: 60,
                                left: 10,
                                //right: 10,
                              ),
                              width: 40,
                              height: 40,
                              child: Image.asset("assets/Group 1482.png"),
                            ),
                          ),
                          Container(
                            margin:
                                EdgeInsets.only(right: 40, top: 60, left: 15),
                            width: 250,
                            //color: Colors.red,
                            child: Text(
                              widget.category.name,
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Poppins',
                                  color: Colors.black,
                                  fontSize: 18),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      //alignment: Alignment.centerRight,
                      height: 25,
                      width: 25,
                      margin: EdgeInsets.only(left: 20, top: 60, right: 10),
                      padding: EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        color: Color(0xff3742FA),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Image.asset("assets/ic_search.png"),
                    ),
                  ],
                ),
              ),
              Container(
                  //color: Colors.green,
                  height: 50,
                  margin: EdgeInsets.only(top: 10, bottom: 10),
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children:
                        List.generate(widget.category.sub.length + 1, (index) {
                      return InkWell(
                        onTap: () {
                          setState(() {
                            selectedIndex = index;
                            selectedIndex == 0
                                ? selectedName = "All"
                                : selectedName =
                                    widget.category.sub[selectedIndex - 1].name;
                            if (selectedIndex > 0) {
                              getProducts(
                                  widget.category.sub[selectedIndex - 1].id);
                            }
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: index == selectedIndex
                                  ? Color(0xff3742FE)
                                  : Color(0xffEEEFFF)),
                          alignment: Alignment.center,
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.only(
                              top: 5, bottom: 5, left: 15, right: 15),
                          child: Text(
                            index == 0
                                ? "All"
                                : widget.category.sub[index - 1].name,
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                                color: index == selectedIndex
                                    ? Colors.white
                                    : Color(0xff8D8F92)),
                          ),
                        ),
                      );
                    }),
                  )),
              Container(
                //color: Colors.red,
                margin: EdgeInsets.only(left: 10, right: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 250,
                      //color: Colors.red,
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.all(5),
                            child: Text(
                              widget.category.name,
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontFamily: "Poppins",
                                color: Color(0xff001833),
                              ),
                            ),
                          ),
                          Container(
                            height: 20,
                            padding: EdgeInsets.all(4),
                            child: Image.asset("assets/sub_category_arrow.png"),
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                selectedName,
                                style: TextStyle(
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  color: Color(0xffD6D6D6),
                                ),
                              )),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              _showPopupToSort(context);
                            },
                            child: Container(
                              height: 30,
                              width: 30,
                              margin: EdgeInsets.only(
                                  top: 5, bottom: 5, left: 65, right: 5),
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color(0xffEEEFFF)),
                              child: Image.asset("assets/ic_subCat_sort.png"),
                            ),
                          ),
                          Container(
                            height: 30,
                            width: 30,
                            margin: EdgeInsets.all(5),
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xffEEEFFF)),
                            child: Image.asset("assets/ic_subCat_filter.png"),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  height: double.infinity,
                  margin: EdgeInsets.only(right: 5),
                  child: response == null
                      ? Shimmer.fromColors(
                          child: GridView.count(
                            crossAxisCount: 3,
                            children: List.generate(12, (index) {
                              return Container(
                                child: ClipRRect(
                                  child: Container(
                                    color: Colors.black26,
                                  ),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                margin:
                                    EdgeInsets.only(bottom: 8, left: 4, right: 4),
                              );
                            }),
                          ),
                          baseColor: Colors.black26,
                          highlightColor: Colors.white)
                      : GridView.count(
                          padding: EdgeInsets.all(0),
                          crossAxisCount: 2,
                          childAspectRatio:
                              MediaQuery.of(context).size.width / (2 * 240),
                          children: List.generate(
                              response.category_product.products.length, (index) {
                            return ProductWidget(
                                response.category_product.products[index], "");
                          }),
                        ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
