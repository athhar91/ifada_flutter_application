import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:ifada/helpers/navigation_helper.dart';

import '../widgets/image_view.dart';
import 'stateless_screen.dart';

class OrderDetailsScreen extends StateLessScreen {
  OrderDetailsScreen(String name) : super(name);

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        color: Color(0xffF4F4F4),
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      //color: Colors.red,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Order ID 2776",
                              style: TextStyle(
                                  color: Color(0xff3742FE),
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14),
                            ),
                          ),
                          Container(
                            child: Text(
                              "Payment failed",
                              style: TextStyle(
                                  color: Color(0xffFFAF50),
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      openScreen(NavigationHelper.orderConfirmroute);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(3),
                          color: Color(0xff3742FE),
                        ),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "REORDER",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "Poppins"),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SingleChildScrollView(
              child: Container(
                height: 350,
                child: Container(
                  width: double.infinity,
                  child: ListView.builder(
                      itemCount: 3,
                      itemBuilder: (_, index) {
                        var dollars = 475.22;
                        return Container(
                          width: double.infinity,
                          height: 110,
                          // margin: EdgeInsets.only(left: 15, right: 15),
                          child: Card(
                            child: Container(
                              // width: double.infinity,
                              // height: double.infinity,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Row(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(left: 7, right: 7),
                                          child: ImageView(
                                            placeHolder: "assets/creame_fruit.png",
                                            // height: 70.0,
                                            // width: 60.0,
                                          ),
                                        ),
                                        Container(
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                child: Text(
                                                  "Creame Fruit Cake",
                                                  style: TextStyle(
                                                      color: Color(0xff3742FE),
                                                      fontFamily: "Poppins",
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: 14),
                                                ),
                                                margin: EdgeInsets.only(
                                                    top: 16, left: 7, right: 7),
                                                alignment: Alignment.topLeft,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Column(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Text(
                                                          "1 Kg",
                                                          style: TextStyle(
                                                              color: Color(0xff8D8F92),
                                                              fontSize: 10,
                                                              fontWeight:
                                                              FontWeight.w400,
                                                              fontFamily: "Poppins"),
                                                        ),
                                                        margin: EdgeInsets.only(
                                                          right: 42,
                                                          top: 7,
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Text(
                                                          "SAR 90.00",
                                                          style: TextStyle(
                                                              color: Color(0xff8D8F92),
                                                              fontSize: 10,
                                                              fontWeight:
                                                              FontWeight.w400,
                                                              fontFamily: "Poppins"),
                                                        ),
                                                        margin: EdgeInsets.only(
                                                            right: 18, top: 2),
                                                      ),
                                                    ],
                                                  ),
                                                  Column(
                                                    children: <Widget>[
                                                      Container(
                                                        child: Text(
                                                          "1 Each",
                                                          style: TextStyle(
                                                              color: Color(0xff8D8F92),
                                                              fontSize: 10,
                                                              fontWeight:
                                                              FontWeight.w400,
                                                              fontFamily: "Poppins"),
                                                        ),
                                                        margin: EdgeInsets.only(
                                                            top: 7, right: 5),
                                                      ),
                                                      Container(
                                                        child: Text(
                                                          "VAT 1.95",
                                                          style: TextStyle(
                                                              color: Color(0xff8D8F92),
                                                              fontSize: 10,
                                                              fontWeight:
                                                              FontWeight.w400,
                                                              fontFamily: "Poppins"),
                                                        ),
                                                        margin: EdgeInsets.only(
                                                            left: 8, top: 2),
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      right: 20,
                                    ),
                                    padding: EdgeInsets.only(left: 10),
                                    width: 80,
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          child: Text(
                                            "\$$dollars",
                                            style: TextStyle(
                                                color: Color(0xff3742FE),
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                fontFamily: "Poppins"),
                                          ),
                                          margin:
                                              EdgeInsets.only(top: 22, left: 10),
                                        ),
                                        Container(
                                          height: 20,
                                          width: 80,
                                          margin:
                                              EdgeInsets.only(bottom: 5, top: 5),
                                          child: RatingBar.builder(
                                            itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: Color(0xffFC8621),
                                            ),
                                            itemCount: 5,
                                            initialRating: 0,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemSize: 13,
                                            onRatingUpdate: (rating) {
                                              print(rating);
                                            },
                                          ),
                                          // child: Row(
                                          //   children: <Widget>[
                                          //     Image.asset(
                                          //         "assets/ic_grey_star.png"),
                                          //     Image.asset(
                                          //         "assets/ic_grey_star.png"),
                                          //     Image.asset(
                                          //         "assets/ic_grey_star.png"),
                                          //     Image.asset(
                                          //         "assets/ic_grey_star.png"),
                                          //     Image.asset(
                                          //         "assets/ic_grey_star.png"),
                                          //   ],
                                          // ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(5)),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              child: Text(
                                "Payment",
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                    color: Color(0xff8D8F92)),
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              "Credit",
                              style: TextStyle(
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: Color(0xff001833)),
                            ),
                          )
                        ],
                      ),
                      // margin: EdgeInsets.only(left: 15, right: 15, top: 15),
                      // padding: EdgeInsets.only(bottom: 5),
                      // decoration: BoxDecoration(
                      //     border: Border(
                      //         bottom: BorderSide(
                      //             width: 2, color: Color(0xffEEEEEE)))),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              child: Text(
                                "VAT",
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                    color: Color(0xff8D8F92)),
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              "SAR 10.00",
                              style: TextStyle(
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: Color(0xff001833)),
                            ),
                          )
                        ],
                      ),
                      // margin: EdgeInsets.only(left: 15, right: 15, top: 15),
                      // padding: EdgeInsets.only(bottom: 5),
                      // decoration: BoxDecoration(
                      //     border: Border(
                      //         bottom: BorderSide(
                      //             width: 2, color: Color(0xffEEEEEE)))),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              child: Text(
                                "Deliver to",
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                    color: Color(0xff8D8F92)),
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              "SASOC MI",
                              style: TextStyle(
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: Color(0xff001833)),
                            ),
                          )
                        ],
                      ),
                      // margin: EdgeInsets.only(left: 15, right: 15, top: 15),
                      // padding: EdgeInsets.only(bottom: 5),
                      // decoration: BoxDecoration(
                      //     border: Border(
                      //         bottom: BorderSide(
                      //             width: 2, color: Color(0xffEEEEEE)))),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              child: Text(
                                "Mobile Number",
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                    color: Color(0xff8D8F92)),
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              "12354698",
                              style: TextStyle(
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: Color(0xff001833)),
                            ),
                          )
                        ],
                      ),
                      // margin: EdgeInsets.only(left: 15, right: 15, top: 15),
                      // padding: EdgeInsets.only(bottom: 5),
                      // decoration: BoxDecoration(
                      //     border: Border(
                      //         bottom: BorderSide(
                      //             width: 2, color: Color(0xffEEEEEE)))),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              child: Text(
                                "Total",
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                    color: Color(0xff001833)),
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              "SAR 260",
                              style: TextStyle(
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: Color(0xff3742FE)),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              //color: Colors.red,
              // margin: EdgeInsets.only(top: 60, bottom: 15, left: 15, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      openScreen(NavigationHelper.orderCancelroute);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(3),
                            color: Colors.white,
                            border: Border.all(width: 1, color: Color(0xff3742FE))),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "CANCEL ORDER",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "Poppins",
                                  color: Color(0xff3742FE)),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xff3742FE)),
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "TRACK ORDER",
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontFamily: "Poppins",
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
