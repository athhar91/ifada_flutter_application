import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ifada/model/product.dart';
import 'package:ifada/screens/stateless_screen.dart';
import 'package:ifada/widgets/image_view.dart';

class ProductDetails extends StateLessScreen {
  Product _product = null;
  ProductDetails(String name, Object args) : super(name){
    if (args is Map) {
      if (args.containsKey("product")) 
        _product = args["product"];
    }
  }

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Color(0xffE5E5E5),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                // alignment: Alignment.topCenter,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      boxShadow: [
                        //background color of box
                        BoxShadow(
                          color: Colors.grey,
                          //blurRadius: 20.0, // soften the shadow
                          spreadRadius: 0.5, //extend the shadow
                          //offset: Offset(2, 2), //right and bottom
                        )
                      ],
                    ),
                    height: 441,
                    width: double.infinity,
                    margin: EdgeInsets.only(
                        top: 220,right: 10,left: 10),
                    child: Container(
                      margin: EdgeInsets.only(top: 120),
                      padding: EdgeInsets.all(3),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                     _product.menu_name,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xff001833)),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            child: Text(
                              "Lorem ipsum dolor sit amet, consectetur adipiloin scing elit. Duis blandit nec mauris",
                              style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff8D8F92)),
                              // maxLines: 1,
                              // overflow: TextOverflow.ellipsis,
                            ),

                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 4),
                                  height: 16,
                                  width: 33,
                                  decoration: BoxDecoration(
                                      color: Color(0xff3742FE),
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Center(
                                    child: Text(
                                      "1KG",
                                      style: TextStyle(
                                        fontSize: 8,
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 4),
                                  height: 16,
                                  width: 33,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(5),
                                      border: Border.all(
                                        color: Color(0xff3742FE),
                                      )),
                                  child: Center(
                                    child: Text(
                                      "2KG",
                                      style: TextStyle(
                                        fontSize: 8,
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xff3742FE),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal:4),
                                  height: 16,
                                  width: 33,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(5),
                                      border: Border.all(
                                        color: Color(0xff3742FE),
                                      )),
                                  child: Text(
                                    "5KG",
                                    style: TextStyle(
                                      fontSize: 8,
                                      fontFamily: "Poppins",
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xff3742FE),
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    child: Text(
                                      "SAR 90.00",
                                      style: TextStyle(
                                          color: Color(0xff2F3542),
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18),
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    "Qty :",
                                    style: TextStyle(
                                        color: Color(0xff3C3C3C),
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w400,
                                        fontSize: 18),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 2),
                                  child: Text(
                                    "Customer Reviews(2)",
                                    style: TextStyle(
                                        color: Color(0xff001833),
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 5),
                                  child: Row(
                                    children: [
                                      Container(
                                        child: Text(
                                          "4.5",
                                          style: TextStyle(
                                              color: Color(0xff8D8F92),
                                              fontFamily: "Poppins",
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12),
                                        ),
                                      ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 4),
                                    height: 12,
                                    width: 12,
                                    child: Image.asset("assets/ic_green_star.png"),
                                  ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            height: 15,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Image.asset("assets/ic_orange_star.png"),
                                Image.asset("assets/ic_orange_star.png"),
                                Image.asset("assets/ic_orange_star.png"),
                                Image.asset("assets/ic_orange_star.png"),
                                Image.asset("assets/ic_grey_star.png"),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    child: Text(
                                      "Lorem ipsum dolor sit amet, consectetur adipiloin scing elit. Duis blandit nec mauris",
                                      style: TextStyle(
                                        color: Color(0xff8D8F92),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w300,
                                        fontFamily: "Poppins",
                                      ),
                                    ),
                                  ),
                                ),

                                Container(
                                  margin: EdgeInsets.only(top:20),
                                  height: 24,
                                  child: Text("See All",
                                      style: TextStyle(
                                          color: Color(0xff3742FE),
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.w500,
                                          fontSize: 10)),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  height: 40,
                                  width: 150,
                                  decoration: BoxDecoration(
                                    color: Color(0xff3742FE),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "ADD TO CART",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "Poppins"),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      margin: EdgeInsets.only(left: 75, right: 75, top: 100),
                      height: 230,
                      decoration: BoxDecoration(
                        boxShadow: [
                          //background color of box
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 10, // soften the shadow
                            spreadRadius: 0.1, //extend the shadow
                            //offset: Offset(2, 0), //right and bottom
                          )
                        ],
                        color: Colors.white,

                      ),
                      child: ImageView(
                        url: _product.image,
                      ),
                    ),
                  ),
                ],
              ),
              Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: Text(
                              "Similar Products",
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 16,
                                color: Color(0xff2F3542),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          child: Text("See More",
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  color: Color(0xff3742FE),
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w500,
                                  fontSize: 10)),
                        )
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Stack(children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                width: 118,
                                height: 158,
                                margin: EdgeInsets.only(
                                    left: 15, bottom: 5, top: 40),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.white,
                                ),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.topLeft,
                                      height: 12,
                                      width: 30,
                                      margin:
                                          EdgeInsets.only(right: 85, top: 5),
                                      padding: EdgeInsets.only(left: 7, top: 1),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Color(0x73C4C4C4),
                                      ),
                                      child: Text(
                                        "1kg",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 8,
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w400),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Container(
                                      margin:
                                          EdgeInsets.only(top: 7, bottom: 3),
                                      width: 118,
                                      height: 98,
                                      child: Image.asset(
                                          "assets/creame_fruit.png"),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          right: 2, left: 2, bottom: 2),
                                      padding: EdgeInsets.all(5),
                                      child: Row(
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                "Creame Fruit",
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: "Poppins",
                                                  fontSize: 10,
                                                ),
                                              )
                                            ],
                                          ),
                                          Container(
                                            height: 15,
                                              alignment: Alignment.centerRight,
                                              margin: EdgeInsets.only(
                                                  left: 25,
                                                  right: 5),
                                              child: Image.asset(
                                                  "assets/blue_cart.svg")),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  left: 15,
                                  bottom: 15,
                                ),
                                padding: EdgeInsets.only(bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "SAR 60.00",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "Poppins"),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        left: 20,
                                        top: 5,
                                      ),
                                      child: Text(
                                        "4.5",
                                        style: TextStyle(
                                            color: Color(0xff8D8F92),
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w500,
                                            fontSize: 8),
                                      ),
                                    ),
                                    Container(
                                      height: 10,
                                      margin: EdgeInsets.only(
                                        top: 5,
                                      ),
                                      child: Image.asset(
                                          "assets/ic_orange_star.png"),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              margin: EdgeInsets.only(left: 120, top: 30),
                              padding: EdgeInsets.all(3),
                              height: 28,
                              width: 28,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(20)),
                              child:
                              SvgPicture.asset("assets/likedButton_image.svg"),
                            ),
                          )
                        ]),
                        Stack(children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                width: 118,
                                height: 158,
                                margin: EdgeInsets.only(
                                    left: 15, bottom: 5, top: 40),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.white,
                                ),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.topLeft,
                                      height: 12,
                                      width: 30,
                                      margin:
                                          EdgeInsets.only(right: 85, top: 5),
                                      padding: EdgeInsets.only(left: 2, top: 1),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Color(0x73C4C4C4),
                                      ),
                                      child: Text(
                                        "300gm",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 8,
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w400),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Container(
                                      margin:
                                          EdgeInsets.only(top: 7, bottom: 3),
                                      width: 118,
                                      height: 98,
                                      child:
                                          Image.asset("assets/cream_bun.png"),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          right: 2, left: 2, bottom: 2),
                                      padding: EdgeInsets.all(5),
                                      child: Row(
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                "Cream Bun",
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: "Poppins",
                                                  fontSize: 10,
                                                ),
                                              )
                                            ],
                                          ),
                                          Container(
                                            height: 15,
                                              alignment: Alignment.centerRight,
                                              margin: EdgeInsets.only(
                                                  left: 30,
                                                  right: 5),
                                              child: Image.asset(
                                                  "assets/blue_cart.svg")),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  left: 15,
                                  bottom: 15,
                                ),
                                padding: EdgeInsets.only(bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "SAR 60.00",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "Poppins"),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        left: 20,
                                        top: 5,
                                      ),
                                      child: Text(
                                        "4.5",
                                        style: TextStyle(
                                            color: Color(0xff8D8F92),
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w500,
                                            fontSize: 8),
                                      ),
                                    ),
                                    Container(
                                      height: 10,
                                      margin: EdgeInsets.only(
                                        top: 5,
                                      ),
                                      child: Image.asset(
                                          "assets/ic_orange_star.png"),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              margin: EdgeInsets.only(left: 120, top: 30),
                              padding: EdgeInsets.all(3),
                              height: 28,
                              width: 28,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(20)),
                              child:
                              SvgPicture.asset("assets/likedButton_image.svg"),
                            ),
                          )
                        ]),
                        Stack(children: <Widget>[
                          Column(
                            children: <Widget>[
                              Container(
                                width: 118,
                                height: 158,
                                margin: EdgeInsets.only(
                                    left: 15, bottom: 5, top: 40),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.white,
                                ),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.topLeft,
                                      height: 12,
                                      width: 30,
                                      margin:
                                      EdgeInsets.only(right: 85, top: 5),
                                      padding: EdgeInsets.only(left: 2, top: 1),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Color(0x73C4C4C4),
                                      ),
                                      child: Text(
                                        "300gm",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 8,
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w400),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Container(
                                      margin:
                                      EdgeInsets.only(top: 7, bottom: 3),
                                      width: 118,
                                      height: 98,
                                      child:
                                      Image.asset("assets/choco_cake2.png"),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                          right: 2, left: 2, bottom: 2),
                                      padding: EdgeInsets.all(5),
                                      child: Row(
                                        children: <Widget>[
                                          Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                "Choco Cake",
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: "Poppins",
                                                  fontSize: 10,
                                                ),
                                              )
                                            ],
                                          ),
                                          Container(
                                              height: 15,
                                              alignment: Alignment.centerRight,
                                              margin: EdgeInsets.only(
                                                  left: 27,
                                                  right: 5),
                                              child: Image.asset(
                                                  "assets/blue_cart.svg")),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  left: 15,
                                  bottom: 15,
                                ),
                                padding: EdgeInsets.only(bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "SAR 60.00",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "Poppins"),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(
                                        left: 20,
                                        top: 5,
                                      ),
                                      child: Text(
                                        "4.5",
                                        style: TextStyle(
                                            color: Color(0xff8D8F92),
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w500,
                                            fontSize: 8),
                                      ),
                                    ),
                                    Container(
                                      height: 10,
                                      margin: EdgeInsets.only(
                                        top: 5,
                                      ),
                                      child: Image.asset(
                                          "assets/ic_orange_star.png"),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              margin: EdgeInsets.only(left: 120, top: 30),
                              padding: EdgeInsets.all(3),
                              height: 28,
                              width: 28,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(20)),
                              child:
                              SvgPicture.asset("assets/likedButton_image.svg"),
                            ),
                          )
                        ]),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
