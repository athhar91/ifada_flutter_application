import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ifada/screens/state_screen.dart';
import 'package:ifada/widgets/top_bar.dart';

class MyRewards extends StateScreen {
  MyRewards(String name) : super(name);

  @override
  ScreenState<StateScreen> getState() {
    return MyRewardsState();
  }
}

class MyRewardsState extends ScreenState<MyRewards> {
  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              TopBar(
                "My Rewards",
                onBack: () {
                  widget.onBackPressed(forceBack: true);
                },
              ),
              Container(
                margin: EdgeInsets.all(8),
                child: Stack(
                  children: [
                    Container(
                      height: 100,
                      child: Card(
                        elevation: 3,
                        child: Row(
                          children: [
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(left: 16, top: 8),
                              child: Text(
                                "Referral Code",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16,
                                    fontFamily: "Poppins"),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              margin: EdgeInsets.only(left: 8, top: 16),
                              child: Text(
                                "--/--",
                                style: TextStyle(
                                    color: Colors.green,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16,
                                    fontFamily: "Poppins"),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 50, left: 16, right: 16),
                      child: Card(
                        elevation: 3,
                        child: Row(
                          children: [
                            Expanded(
                              child: Column(
                                children: [
                                  Container(
                                    alignment: Alignment.topLeft,
                                    margin: EdgeInsets.only(left: 16, top: 10),
                                    child: Text(
                                      "100",
                                      style: TextStyle(
                                          color: Colors.green,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 20,
                                          fontFamily: "Poppins"),
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.topLeft,
                                    margin: EdgeInsets.only(left: 16),
                                    child: Text(
                                      "Redeem your Points",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                          fontFamily: "Poppins"),
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.topLeft,
                                    margin: EdgeInsets.only(left: 16),
                                    padding: EdgeInsets.only(bottom: 25),
                                    child: Text(
                                      "Note: Your points should be 200 to redeem",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.normal,
                                          fontSize: 9,
                                          fontFamily: "Poppins"),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              child: Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 40, left: 15, right: 15),
                                    padding: EdgeInsets.only(
                                        left: 4, right: 4, top: 8, bottom: 8),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(6.0),
                                      ),
                                      color: Color(0xff3742FA),
                                    ),
                                    child: Text(
                                      "Redeem Points",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 10,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 10, left: 15, right: 15),
                                    padding: EdgeInsets.only(bottom: 10),
                                    child: Text(
                                      "Point history",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 9,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(8),
                child: Card(
                  elevation: 3,
                  child: ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: 15,
                      itemBuilder: (_, index) {
                        return Container(
                          margin: EdgeInsets.only(top: 5,bottom: 5),
                          child: Row(
                            children: [
                              Container(
                                height: 45,
                                width: 45,
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(left: 10),
                                decoration: BoxDecoration(
                                    color: Color(0xff3742FA),
                                    borderRadius: BorderRadius.circular(40)),
                                child: Text("S",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                    color: Colors.white,
                                  ),),
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.topLeft,
                                      margin: EdgeInsets.only(left: 10,top: 5),
                                      child: Text("Redeem name",
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                          fontFamily: "Poppins"
                                      ),),
                                    ),
                                    Container(
                                      alignment: Alignment.topLeft,
                                      margin: EdgeInsets.only(left: 10,top: 5),
                                      child: Text("Date",
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w300,
                                            fontFamily: "Poppins"
                                        ),),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10,top: 5, right: 10),
                                child: Text("\$ 200.00",
                                  style: TextStyle(
                                    color: Color(0xff626bf8),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "Poppins"
                                  ),),
                              ),
                            ],
                          ),
                        );
                      }),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
