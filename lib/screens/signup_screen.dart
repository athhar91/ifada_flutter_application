import 'dart:collection';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ifada/helpers/api_helper.dart';
import 'package:ifada/helpers/dialog_helper.dart';
import 'package:ifada/helpers/navigation_helper.dart';
import 'package:ifada/helpers/preference_helper.dart';
import 'package:ifada/model/User.dart';
import 'package:ifada/widgets/cc_picker.dart';

import '../utils.dart';
import 'state_screen.dart';

class SignUpScreen extends StateScreen {
  SignUpScreen(String name) : super(name);

  @override
  SignUpScreenSate getState() => new SignUpScreenSate();
}

class SignUpScreenSate extends ScreenState {
  var isCheckboxSelected = false;
  var isSecondBoxChecked = false;
  String password = "";
  String email = "";
  String firstName = "";
  String lastName = "";
  String num = "";
  String cc = "+971";
  String confirmPassword = "";

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image:
                  new ExactAssetImage('assets/ic_login_background_image.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Container(
            color: Color(0xff80000000),
            child: Center(
              child: ListView(
                children: <Widget>[
                  Container(
                      width: 100,
                      height: 100,
                      child: Image.asset(
                        "assets/ic_ifada.png",
                      )),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Text("SIGNUP",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 35,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w500,
                          color: Colors.white,
                        )),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      left: 25,
                      right: 25,
                      top: 15,
                    ),
                    child: TextField(
                      onChanged: (String value) {
                        firstName = value;
                      },
                      maxLines: 1,
                      decoration: InputDecoration(
                        hintText: "",
                        labelText: "First Name",
                        labelStyle: TextStyle(color: Colors.white),
                        floatingLabelBehavior: FloatingLabelBehavior.auto,
                        hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500),
                        hintMaxLines: 1,
                        enabledBorder: UnderlineInputBorder(
                          borderSide: new BorderSide(color: Colors.white),
                        ),
                      ),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      left: 25,
                      right: 25,
                      top: 15,
                    ),
                    child: TextField(
                      onChanged: (String value) {
                        lastName = value;
                      },
                      maxLines: 1,
                      decoration: InputDecoration(
                          hintText: "",
                          labelText: "Last Name",
                          labelStyle: TextStyle(color: Colors.white),
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white),
                          )),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      left: 25,
                      right: 25,
                      top: 15,
                    ),
                    child: TextField(
                      onChanged: (String value) {
                        num = value;
                        return num;
                      },
                      maxLines: 1,
                      maxLength: 10,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          hintText: "",
                          labelText: "Mobile Number",
                          prefix: CCPicker(color: Colors.white,cc:cc,onChanged: (value){
                            this.cc = value;
                          },),
                          labelStyle: TextStyle(color: Colors.white),
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white),
                          )),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      left: 25,
                      right: 25,
                    ),
                    child: TextField(
                      onChanged: (String value) {
                        email = value;
                      },
                      maxLines: 1,
                      decoration: InputDecoration(
                          hintText: "",
                          labelText: "Email ID",
                          labelStyle: TextStyle(color: Colors.white),
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white),
                          )),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      left: 25,
                      right: 25,
                      top: 15,
                    ),
                    child: TextField(
                      maxLines: 1,
                      obscureText: true,
                      onChanged: (String value) {
                        password = value;
                      },
                      decoration: InputDecoration(
                          hintText: "",
                          labelText: "Password",
                          labelStyle: TextStyle(color: Colors.white),
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white),
                          )),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      left: 25,
                      right: 25,
                      top: 15,
                    ),
                    child: TextField(
                      onChanged: (String value) {
                        confirmPassword = value;
                      },
                      maxLines: 1,
                      obscureText: true,
                      decoration: InputDecoration(
                          hintText: "",
                          labelText: "Confirm Password",
                          labelStyle: TextStyle(color: Colors.white),
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white),
                          )),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      left: 25,
                      right: 25,
                      top: 15,
                    ),
                    child: TextField(
                      maxLines: 1,
                      decoration: InputDecoration(
                          hintText: "",
                          labelText: "Referral Code",
                          labelStyle: TextStyle(color: Colors.white),
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white),
                          )),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  // Container(
                  //     padding: EdgeInsets.all(2),
                  //     //margin: EdgeInsets.only(left: 230, bottom: 15),
                  //     child: Text(
                  //       "Forgot Password?",
                  //       textAlign: TextAlign.center,
                  //       style: TextStyle(
                  //         color: Colors.white,
                  //         fontSize: 15,
                  //       ),
                  //     )),
                  Container(
                    height: 35,
                    child: Theme(
                      data: ThemeData(
                        unselectedWidgetColor: Colors.white,
                      ),
                      child: CheckboxListTile(
                        value: isCheckboxSelected,
                        onChanged: (bool val) {
                          setState(() {
                            isCheckboxSelected = val;
                          });
                        },
                        title: Text(
                          "I am Retailer",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        controlAffinity: ListTileControlAffinity.leading,
                      ),
                    ),
                  ),
                  Container(
                    height: 30,
                    child: Theme(
                      data: ThemeData(
                        unselectedWidgetColor: Colors.white,
                      ),
                      child: CheckboxListTile(
                        value: isSecondBoxChecked,
                        onChanged: (bool val) {
                          setState(() {
                            isSecondBoxChecked = val;
                          });
                        },
                        title: Text(
                          "I accept Terms and Conditions",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        controlAffinity: ListTileControlAffinity.leading,
                      ),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 90, left: 16, right: 16),
                    height: 45,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(3.0)),
                    ),
                    child: RaisedButton(
                      color: Color(0xff3742FA),
                      child: Text(
                        "SIGNUP",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 20,
                          fontFamily: 'Poppins',
                        ),
                      ),
                      onPressed: () {
                        if (isValidateAlldatas(context)) {
                          DialogHelper.showLoadingDialog(context, false);
                          signup();
                          //widget.openScreen(NavigationHelper.otpSignuproute, args: HashMap.from({'phone': num}));
                          //Navigator.push(context, otpSignuproute(builder: (context) => OtpForSignUp(number: num)));
                        }
                      },
                    ),
                  ),
                  // Container(
                  //   padding: EdgeInsets.only(top: 60),
                  //   child: Text("Or Login With",
                  //     textAlign: TextAlign.center,
                  //     style: TextStyle(
                  //       color: Colors.white,
                  //     ),
                  //   ),
                  // ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 16, bottom: 16),
                          child: Text(
                            "Already a user",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Poppins',
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            widget.openScreen(NavigationHelper.loginRoute);
                          },
                          child: Container(
                            padding: EdgeInsets.only(top: 16, bottom: 16),
                            child: Text(
                              " Sign in",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color(0xffFFAF50),
                                fontFamily: 'Poppins',
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  void signup(){
    ApiHelper<User>(
            (res) => {
          DialogHelper.closeLoadingDialog(),
              log("Otp is "+res.otp.toString()),
          PreferenceHelper.saveUserData(res),
              widget.openScreen(NavigationHelper.otpRoute, args: HashMap.from({'phone': num}))
        },
            (error) => {
          DialogHelper.closeLoadingDialog(),
          _showDialog("Alert", error.isEmpty ? "Failed to Login" : error, context)
        },
            () => new User())
        .signup(firstName,lastName,email,password,cc,num);
  }

  bool isValidateAlldatas(BuildContext context) {
    if (isNullOrWhiteSpace(firstName)) {
      _showDialog("Alert", "First Name can't be blank", context);
      return false;
    } else if (isNullOrWhiteSpace(lastName)) {
      _showDialog("Alert", "Last Name can't be blank", context);
      return false;
    } else if (isNullOrEmpty(num) || num.length < 10) {
      _showDialog("Alert", "Enter a valid phone number", context);
      return false;
    } else if (isNullOrWhiteSpace(email) || !isValidEmail(email)) {
      _showDialog("Alert", "Enter a valid email address", context);
      return false;
    } else if (isNullOrWhiteSpace(password)) {
      _showDialog("Alert", "Enter a valid password", context);
      return false;
    } else if (isNullOrWhiteSpace(confirmPassword)) {
      _showDialog("Alert", "Enter the same password to confirm", context);
      return false;
    } else if (password != confirmPassword) {
      _showDialog("Alert", "Password didn't matched", context);
      return false;
    } else if (isSecondBoxChecked == false) {
  _showDialog("Alert", "Please accept the Terms and Conditions", context);
  return false;
  }
    return (true);
  }

  void _showDialog(String title, String content, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Text(content),
          actions: <Widget>[
            FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
