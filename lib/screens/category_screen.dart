import 'dart:collection';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ifada/helpers/api_helper.dart';
import 'package:ifada/helpers/app_data_helper.dart';
import 'package:ifada/helpers/navigation_helper.dart';
import 'package:ifada/model/category.dart';
import 'package:ifada/model/response/all_categories.dart';
import 'package:ifada/screens/state_screen.dart';
import 'package:ifada/widgets/image_view.dart';
import 'package:shimmer/shimmer.dart';

class CategoryScreen extends StateScreen {
  CategoryScreen(String name) : super(name);

  @override
  ScreenState<StateScreen> getState() {
    return CategoryScreenState();
  }
}

// static List<Category> getCategories() {
//   var category = List<Category>();
//   category.add(Categories("assets/babystorecontainer.png",
//       "assets/babystore_image.png", "Baby Store"));
//   category.add(Categories("assets/footwearcontainer_image.png",
//       "assets/footwear_image.png", "Footwears"));
//   category.add(Categories("assets/grocerycontainer_image.png",
//       "assets/grocery_image.png", "Grocery"));
//   category.add(Categories("assets/healthcontainer_image.png",
//       "assets/health_image.png", "Health and Beauty"));
//   category.add(Categories("assets/homeappcontainer_image.png",
//       "assets/homeappliance_image.png", "Home Appliance"));
//   category.add(Categories(
//       "assets/meatcontainer_image.png", "assets/meat_image.png", "Meat"));
//   category.add(Categories("assets/officecontainer_image.png",
//       "assets/office_image.png", "Office Materials"));
//   category.add(Categories("assets/petstorecontainer_image.png",
//       "assets/petstore_image.png", "Pet Store"));
//   category.add(Categories("assets/sportscontainer_image.png",
//       "assets/sports_image.png", "Sports and Fitness"));
//   category.add(Categories("assets/stationarycontainer_image.png",
//       "assets/stationary_image.png", "Stationary"));
//   category.add(Categories("assets/textilecontainer_image.png",
//       "assets/textile_image.png", "Textile"));
//   return category;
// }

class CategoryScreenState extends ScreenState<CategoryScreen> {
  List<Category> categories = AppDataHelper.allCategories != null
      ? AppDataHelper.allCategories.category
      : [];
  List colors = [
    Color(0x44C84E89),
    Color(0x44004E8F),
    Color(0x44A811DA),
    Color(0x446FB1FC),
    Color(0x44FFA751)
  ];
  Random random = new Random();

  @override
  void initState() {
    getCategories();
    super.initState();
  }

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            Stack(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      alignment: Alignment.topCenter,
                      height: 190,
                      decoration: new BoxDecoration(
                        color: Colors.red,
                        image: new DecorationImage(
                          image: new ExactAssetImage(
                              'assets/category_background.png'),
                          fit: BoxFit.fitWidth,
                        ),
                      )),
                ),
                Container(
                  width: double.infinity,
                  height: 190,
                  color: Colors.black26,
                ),
                Positioned(
                    bottom: 0,
                    child: Container(
                      width: double.infinity,
                      height: 80,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Color(0x01FFFFFF),
                              Colors.white30,
                              Colors.white70
                            ]),
                      ),
                    )),
                Positioned(
                  bottom: 10,
                  left: 10,
                  child: Container(
                      child: Text(
                    "Categories",
                    style: TextStyle(
                      color: Color(0xff001833),
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Poppins",
                    ),
                  )),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                      margin: EdgeInsets.only(right: 5, top: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.all(5),
                              child: SvgPicture.asset(
                                "assets/ic_search.svg",
                                height: 25,
                                width: 25,
                              )),
                          Container(
                              margin: EdgeInsets.all(5),
                              child: SvgPicture.asset(
                                "assets/ic_notifications.svg",
                                height: 25,
                                width: 25,
                              )),
                          Container(
                              margin: EdgeInsets.all(5),
                              child: SvgPicture.asset(
                                "assets/white_cart.svg",
                                height: 25,
                                width: 25,
                              ))
                        ],
                      )),
                )
              ],
            ),
            Flexible(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.only(left: 4, right: 4),
                  child: categories.length == 0
                      ? Shimmer.fromColors(
                          child: GridView.count(
                            padding: EdgeInsets.only(top: 6),
                            crossAxisCount: 3,
                            children: List.generate(12, (index) {
                              return Container(
                                child: ClipRRect(
                                  child: Container(
                                    color: Colors.black26,
                                  ),
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                margin: EdgeInsets.all(6),
                              );
                            }),
                          ),
                          baseColor: Colors.black26,
                          highlightColor: Colors.white)
                      : GridView.count(
                          crossAxisCount: 3,
                          padding: EdgeInsets.only(top: 6),
                          children: List.generate(categories.length, (index) {
                            return Container(
                                margin: EdgeInsets.all(6),
                              child: InkWell(
                                onTap: () {
                                  widget.openScreen(
                                      NavigationHelper.subCategoryroute,
                                      args: HashMap.from({
                                        "category": categories[index],
                                        "categories": categories
                                      }));
                                },
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(12.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: colors[index % colors.length],
                                    ),
                                    child: Stack(
                                      children: <Widget>[
                                        Container(
                                          child: ImageView(
                                            height: double.infinity,
                                            width: double.infinity,
                                            url: categories[index].cat_image,
                                            placeHolder:
                                                "assets/shopping-bag.png",
                                          ),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            color:
                                                colors[index % colors.length],
                                          ),
                                          child: Container(
                                              decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                    colors: [
                                                      Color(0x01FFFFFF),
                                                      Colors.black26,
                                                      Colors.black
                                                    ],
                                                    begin: Alignment.topCenter,
                                                    end:
                                                        Alignment.bottomCenter),
                                              ),
                                              alignment: Alignment.bottomCenter,
                                              child: Row(children: [
                                                Container(
                                                    padding: EdgeInsets.all(4),
                                                    child: Text(
                                                      categories[index].name,
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: Colors.white,
                                                          fontFamily: 'Poppins',
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ))
                                              ])),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }),
                        ),
                )),
          ],
        ),
      ),
    );
  }

  List<Category> getCategories() {
    ApiHelper<AllCategories>(
        (res) => {
              setState(() {
                AppDataHelper.allCategories = res;
                categories = res.category;
              })
            },
        (error) => {},
        () => new AllCategories()).getCategories();
  }
}
