import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ifada/helpers/navigation_helper.dart';
import 'package:ifada/screens/Products_screen.dart';
import 'package:ifada/screens/category_screen.dart';
import 'package:ifada/screens/home_screen.dart';
import 'package:ifada/screens/myOrders_screen.dart';
import 'package:ifada/screens/my_account.dart';
import 'package:ifada/screens/productdetails_screen.dart';
import 'package:ifada/screens/state_screen.dart';

import '../helpers/navigation_helper.dart';

class MainScreen extends StateScreen{
  var currentPageValue = 2;
  MainScreen(String name) : super(name){
    controller =
    new PageController(initialPage: currentPageValue, keepPage: true);
  }

  @override
  ScreenState<StateScreen> getState() {
    return new MainScreenState();
  }
  PageController controller = null;

}

class MainScreenState extends ScreenState<MainScreen>{
  List<Widget> pages;

  @override
  void initState() {
    pages = [
      CategoryScreen(NavigationHelper.categoryroute),
      ProductsScreen(NavigationHelper.productsroute),
      HomeScreen(NavigationHelper.homeScreenroute),
      MyOrdersScreen(NavigationHelper.myOrdersroute),
      MyAccount(NavigationHelper.myAccountRoute)
    ];
    super.initState();
  }

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(left: 16,right: 16),
        width: double.infinity,
        height: 60,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(20),
              topLeft: Radius.circular(20)),
          color: Color(0xff3742FE),
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: InkWell(
                onTap: ()=>{
                  widget.controller.jumpToPage(0)
                },
                child:  SvgPicture.asset(
                  widget.currentPageValue==0?"assets/filled_category.svg":"assets/unfilled_category.svg",
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: InkWell(
                onTap: ()=>{
                  widget.controller.jumpToPage(1)
                },
                child: SvgPicture.asset(
                  widget.currentPageValue==1?"assets/filled_heart.svg":"assets/unfilled_heart.svg",
                  // color: Colors.red,
                  // semanticsLabel: 'A red up arrow'
                )
                // Image.asset(
                //   widget.currentPageValue == 1?"assets/filled_heart.png":"assets/heart_image.png",
                //   height: 30,
                //   width: 30,
                // ),
              ),
            ),
            SizedBox(width: 100,height: 60,),
            Expanded(
              flex: 1,
              child: InkWell(
                onTap: ()=>{
                  widget.controller.jumpToPage(3)
                },
                child: SvgPicture.asset(
                    widget.currentPageValue==3?"assets/shopping_bag_fill.svg":"assets/shopping_bag_unfill.svg",
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: InkWell(
                onTap: ()=>{
                  widget.controller.jumpToPage(4)
                },
                child: SvgPicture.asset(
                  widget.currentPageValue==4?"assets/user_image_fill.svg":"assets/user_image_unfill.svg",
                ),

                // Image.asset(
                //   widget.currentPageValue==4?"assets/ic_filled_user.png":"assets/user_image.png",
                //   height: 30,
                //   width: 30,
                // ),
              ),
            ),
          ],
        ),
      ),
      // Box Navigation Bar
      // bottomNavigationBar: BottomNavigationBar(
      //   currentIndex: _currentIndex,
      //     backgroundColor: Color(0xff3742FE),
      //   iconSize: 40,
      //   // selectedItemColor: Colors.red,
      //   // unselectedItemColor: Colors.white,
      //   type: BottomNavigationBarType.fixed,
      //   items : [
      //     BottomNavigationBarItem(
      //       // icon: ImageIcon(AssetImage(_currentIndex != 0 ? "assets/box_image.png" : "assets/filled_box_image.png"),),
      //       icon: Container(
      //         child: Image.asset(_currentIndex != 0 ? "assets/box_image.png" : "assets/filled_box_image.png"),
      //       ),
      //       title: Text(""),
      //       backgroundColor: Colors.white
      //     ),
      //     BottomNavigationBarItem(
      //       // icon: Icon(_currentIndex != 1 ? Icons.home : Icons.image),
      //         icon: ImageIcon(NetworkImage(_currentIndex != 1 ? "assets/heart_image.png" : "assets/heart_image.png")),
      //       title: Text(""),
      //     ),
      //     BottomNavigationBarItem(
      //         icon: ImageIcon(NetworkImage(_currentIndex != 2 ? "assets/shopping-bag_image.png" : "assets/shopping-bag_image.png")),
      //       title: Text(""),
      //     ),
      //     BottomNavigationBarItem(
      //         icon: ImageIcon(NetworkImage(_currentIndex != 4 ? "assets/user_image.png" : "assets/box_image.png")),
      //       title: Text(""),
      //     ),
      //   ],
      //   onTap: (index) {
      //     setState(() {
      //       _currentIndex = index;
      //       widget.controller.jumpToPage(_currentIndex);
      //     });
      //   },
      // ),
        floatingActionButton: Padding(
          padding: EdgeInsets.only(top: 10),
          child: Container(
            height: 70,
            width: 70,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Color(0x1f3742FA),
            ),
            child: FloatingActionButton(
              backgroundColor: Colors.transparent,
              elevation: 8,
              onPressed: () {},
              child: InkWell(
                onTap: () => {
                  widget.controller.jumpToPage(2)
                },
                child: Container(
                  height: 60,
                  width: 60,
                  padding: EdgeInsets.all(12),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                  ),
                  child: SvgPicture.asset(widget.currentPageValue==2?"assets/filled_home.svg":"assets/unfilled_home.svg",
                  ),
                ),
              ),
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: PageView(
        children: pages,
        controller: widget.controller,
          onPageChanged: (int value) {
            widget.currentPageValue = value;
            print("Update position: $value");
            setState(() {
            });
          }
      ),
    );
  }
}