import 'package:flutter/material.dart';
import 'package:ifada/helpers/navigation_helper.dart';
import 'package:ifada/screens/state_screen.dart';

class ResetPassword extends StateScreen {
  ResetPassword(String name) : super(name);

  @override
  ResetPasswordState getState() => new ResetPasswordState();
}

class ResetPasswordState extends ScreenState {
  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Color(0xffE5E5E5),
        child: SingleChildScrollView(
          child: Container(
            child: Center(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            widget.openScreen(NavigationHelper.otpRoute);
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                                top: 60, left: 10, right: 10, bottom: 40),
                            width: 40,
                            height: 40,
                            child: Image.asset("assets/Group 1482.png"),
                          ),
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(right: 80, top: 60, bottom: 40),
                          child: Text(
                            "Password Reset",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontFamily: 'Poppins',
                                color: Colors.black,
                                fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 100, bottom: 10),
                    child: Text(
                      "Reset Password",
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Poppins',
                          fontSize: 21,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 40),
                    child: Text(
                      "You can reset your Password now!",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff8D8F92),
                          fontFamily: 'Poppins',
                          fontSize: 16),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(3)),
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.all(10),
                    child: Column(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                left: 10,
                                right: 10,
                                top: 15,
                              ),
                              child: TextField(
                                maxLines: 1,
                                obscureText: true,
                                decoration: InputDecoration(
                                    hintText: "",
                                    labelText: "New Password",
                                    labelStyle:
                                        TextStyle(color: Color(0xff8D8F92)),
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.auto,
                                    hintStyle: TextStyle(
                                      color: Color(0xff8D8F92),
                                      fontSize: 14,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500,
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: new BorderSide(
                                        color: Color(0xffC4C4C4),
                                      ),
                                    )),
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                left: 10,
                                right: 10,
                                top: 15,
                              ),
                              child: TextField(
                                maxLines: 1,
                                obscureText: true,
                                decoration: InputDecoration(
                                    hintText: "",
                                    labelText: "Confirm Password",
                                    labelStyle:
                                        TextStyle(color: Color(0xff8D8F92)),
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.auto,
                                    hintStyle: TextStyle(
                                      color: Color(0xff8D8F92),
                                      fontSize: 14,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500,
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: new BorderSide(
                                        color: Color(0xffC4C4C4),
                                      ),
                                    )),
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                        Container(
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.only(left: 16, right: 16, top: 35, bottom: 20),
                            height: 45,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(3.0)),
                              color: Color(0xff3742FA),
                            ),
                            child: Text(
                              "CHANGE PASSWORD",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 20,
                                fontFamily: 'Poppins',
                              ),
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
