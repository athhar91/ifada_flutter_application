import 'dart:async';

import 'package:flutter/material.dart';
import 'package:ifada/helpers/navigation_helper.dart';
import 'package:ifada/screens/state_screen.dart';
import 'package:ifada/widgets/safe_scroll_container.dart';

class OtpVerification extends StateScreen {
  bool check = false;
  String selectedNum;
  Timer _timer;
  int _start = 30;

  @override
  void dispose() {}

  OtpVerification(String name, Object args) : super(name) {
    if (args is Map) {
      if (args.containsKey("phone")) selectedNum = args["phone"];
    }
  }

  @override
  OtpVerificationState getState() => new OtpVerificationState();
}

class OtpVerificationState extends ScreenState<OtpVerification> {
  FocusNode pin2FocusNode;
  FocusNode pin3FocusNode;
  FocusNode pin4FocusNode;
  bool _reSendSMS = false;

  @override
  void initState() {
    pin2FocusNode = FocusNode();
    pin3FocusNode = FocusNode();
    pin4FocusNode = FocusNode();
    super.initState();

  }

  void nextField({String value, FocusNode focusNode}) {
    if (value.length == 1) {
      focusNode.requestFocus();
    }
  }

  @override
  void dispose() {
    pin2FocusNode.dispose();
    pin3FocusNode.dispose();
    pin4FocusNode.dispose();
    super.dispose();
  }

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Color(0xffE5E5E5),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                Container(
                  child: Row(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          widget.onBackPressed();
                        },
                        child: Container(
                          margin: EdgeInsets.only(top: 60, left: 10, right: 10),
                          width: 40,
                          height: 40,
                          child: Image.asset("assets/Group 1482.png"),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 60, left: 10, right: 10),
                        child: Text(
                          "Otp Verification",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontFamily: 'Poppins',
                              color: Colors.black,
                              fontSize: 18),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 120, bottom: 20),
                  child: Text(
                    "Verify OTP",
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Poppins',
                        fontSize: 21,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: Text(
                    "Please type verification code sent to",
                    style: TextStyle(
                        color: Color(0xff8D8F92), fontFamily: 'Poppins'),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 40),
                  child: Text(
                    "${widget.selectedNum}",
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(3)),
                  padding: EdgeInsets.only(top: 30, left: 10, right: 10),
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            width: 35,
                            height: 45,
                            decoration: BoxDecoration(
                                color: Color(0xffEEEFFF),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3))),
                            margin: EdgeInsets.only(
                              left: 100,
                              right: 10,
                            ),
                            child: TextField(
                              autofocus: true,
                              onChanged: (number) {
                                nextField(
                                    value: number, focusNode: pin2FocusNode);
                              },
                              textInputAction: TextInputAction.next,
                              maxLines: 1,
                              maxLength: 1,
                              keyboardType: TextInputType.number,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                border: const OutlineInputBorder(),
                                enabledBorder: InputBorder.none,
                                contentPadding: EdgeInsets.all(12),
                                hintStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 40,
                                  fontFamily: 'Poppins',
                                ),
                                counterText: "",
                              ),
                            ),
                          ),
                          Container(
                            width: 35,
                            height: 45,
                            decoration: BoxDecoration(
                                color: Color(0xffEEEFFF),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3))),
                            margin: EdgeInsets.only(
                              right: 10,
                            ),
                            child: TextField(
                              focusNode: pin2FocusNode,
                              onChanged: (number) {
                                nextField(
                                    value: number, focusNode: pin3FocusNode);
                              },
                              maxLines: 1,
                              maxLength: 1,
                              keyboardType: TextInputType.number,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                border: const OutlineInputBorder(),
                                enabledBorder: InputBorder.none,
                                contentPadding: EdgeInsets.all(12),
                                hintStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 40,
                                  fontFamily: 'Poppins',
                                ),
                                counterText: "",
                              ),
                            ),
                          ),
                          Container(
                            width: 35,
                            height: 45,
                            decoration: BoxDecoration(
                                color: Color(0xffEEEFFF),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3))),
                            margin: EdgeInsets.only(
                              right: 10,
                            ),
                            child: TextField(
                              focusNode: pin3FocusNode,
                              onChanged: (number) {
                                nextField(
                                    value: number, focusNode: pin4FocusNode);
                              },
                              maxLines: 1,
                              maxLength: 1,
                              keyboardType: TextInputType.number,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                border: const OutlineInputBorder(),
                                enabledBorder: InputBorder.none,
                                contentPadding: EdgeInsets.all(12),
                                hintStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 40,
                                  fontFamily: 'Poppins',
                                ),
                                counterText: "",
                              ),
                            ),
                          ),
                          Container(
                            width: 35,
                            height: 45,
                            decoration: BoxDecoration(
                                color: Color(0xffEEEFFF),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3))),
                            child: TextField(
                              focusNode: pin4FocusNode,
                              onChanged: (number) {
                                pin4FocusNode.unfocus();
                              },
                              maxLines: 1,
                              maxLength: 1,
                              keyboardType: TextInputType.number,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                border: const OutlineInputBorder(),
                                enabledBorder: InputBorder.none,
                                contentPadding: EdgeInsets.all(12),
                                hintStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 40,
                                  fontFamily: 'Poppins',
                                ),
                                counterText: "",
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 40, bottom: 10),
                        padding: EdgeInsets.only(left: 250),
                        child: _reSendSMS? InkWell(
                          onTap: () {
                            setState(() {
                              _reSendSMS = false;
                            });
                          },
                          child: Container(
                            padding: EdgeInsets.only(left: 15),
                            child: Text("Resend OTP",
                              style: TextStyle(
                                  color: Color(0xff3742FA),
                                  fontFamily: 'Poppins'),),
                            // margin: EdgeInsets.only(top: 60, left: 10, right: 10),
                            // width: 40,
                            // height: 40,
                            // child: Image.asset("assets/Group 1482.png"),
                          ),
                        ): TweenAnimationBuilder(
                          tween: Tween(begin: 30.0, end: 0.0),
                          duration: Duration(seconds: 30),
                          builder: (context, value, child) {
                            if(value.toInt() == 0)
                              resendInMin();
                            return Text(
                              value.toInt()>9?
                              "Resend in 00:${ value.toInt()}":"Resend in 00:0${ value.toInt()}",
                              style: TextStyle(
                                  color: Color(0xff8D8F92),
                                  fontFamily: 'Poppins'),
                            );
                          }
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          widget.openScreen(NavigationHelper.resetroute);
                        },
                        child: Container(
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.only(
                                left: 16, right: 16, bottom: 20),
                            height: 45,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(3.0)),
                              color: Color(0xff3742FA),
                            ),
                            child: Text(
                              "CONFIRM",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 20,
                                fontFamily: 'Poppins',
                              ),
                            )),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void resendInMin(){
    Future.delayed(const Duration(seconds: 1), () {
      setState(() {
        _reSendSMS = true;
      });
    });
  }
}
