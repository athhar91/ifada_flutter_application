import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'stateless_screen.dart';

class RateTheProduct extends StateLessScreen {
  RateTheProduct(String name) : super(name);

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Color(0xffE5E5E5),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                width: double.infinity,
                height: 116,
                margin: EdgeInsets.only(top: 120, left: 15, right: 15),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.white, borderRadius: BorderRadius.circular(5)),
                child: Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(top: 5, right: 200),
                      padding: EdgeInsets.all(5),
                      child: Text(
                        "Item 2",
                        style: TextStyle(
                            color: Color(0xff5B5B5B),
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500,
                            fontSize: 14),
                      ),
                    ),
                    Container(
                      width: 200,
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(right: 170),
                      padding: EdgeInsets.all(5),
                      child: Text(
                        "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.",
                        style: TextStyle(
                            color: Color(0xff5B5B5B),
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            fontSize: 10),
                      ),
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(bottom: 5),
                      padding: EdgeInsets.all(5),
                      child: Text(
                        "Delivery 01 Feb 2021",
                        style: TextStyle(
                            color: Color(0xff5B5B5B),
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w400,
                            fontSize: 14),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 35, bottom: 10),
                child: Text(
                  "Rate Us",
                  style: TextStyle(
                      color: Color(0xff5B5B5B),
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w500,
                      fontSize: 18),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(bottom: 5, top: 5),
                child: RatingBar.builder(
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Color(0xffFFAF50),
                  ),
                  itemCount: 5,
                  initialRating: 0,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                width: double.infinity,
                height: 120,
                margin: EdgeInsets.only(top: 10, left: 15, right: 15),
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5)),
                child: TextField(
                  maxLines: 8,
                  textAlign: TextAlign.start,
                  decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    alignLabelWithHint: true,
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none
                    ),
                    labelText: "Write your comments",
                    labelStyle: TextStyle(
                        color: Color(0xff5B5B5B),
                        fontFamily: "Poppins",
                        fontWeight: FontWeight.w400,
                        fontSize: 12),
                    hintText: "",
                    hintStyle: TextStyle(
                        color: Color(0xff5B5B5B),
                        fontFamily: "Poppins",
                        fontWeight: FontWeight.w400,
                        fontSize: 12),
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.only(left: 22, right: 22, top: 260, bottom: 20),
                  height: 45,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.all(Radius.circular(3.0)),
                    color: Color(0xff3742FA),
                  ),
                  child: Text(
                    "SUBMIT",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                      fontFamily: 'Poppins',
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
