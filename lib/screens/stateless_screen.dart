import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:ifada/helpers/navigation_helper.dart';

abstract class StateLessScreen extends StatelessWidget{
  BuildContext _context;

  final String name;
  StateLessScreen(this.name);

  @override
  Widget build(BuildContext context) {
    _context = context;
    return WillPopScope(
        onWillPop: (){
          return onBackPressed();
        },
        child:getWidget(context));
  }

  Widget getWidget(BuildContext context);

  Future<bool> onBackPressed(){
    NavigationHelper.screenWillPop(name);
    //Navigator.pop(_context);
    return  new Future(() => true);
  }

  void openScreen(String name,{HashMap args,bool forceNew = false}){
    NavigationHelper.openScreen(_context, name,args: args,forceNew: forceNew);
  }

}