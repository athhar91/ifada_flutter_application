import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ifada/screens/state_screen.dart';
import 'package:ifada/widgets/cc_picker.dart';
import 'package:ifada/widgets/safe_scroll_container.dart';
import 'package:ifada/widgets/top_bar.dart';

import '../utils.dart';

class AddAddress extends StateScreen {
  AddAddress(String name) : super(name);


  @override
  ScreenState<StateScreen> getState() {
    return AddAddressState();
  }
}

class AddAddressState extends ScreenState<AddAddress> {
  String Name="";
  String phone="";
  String email="";
  String state="";
  String country="";
  String street="";
  String postal_code="";
  String cc="(305)";
  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        height: double.infinity,
        margin: EdgeInsets.only(top:8),
        child: Container(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children:[
              TopBar("Add Address", onBack: (){
                widget.onBackPressed(forceBack: true);
              },),
            Expanded(
              child: SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                child: Expanded(
                  child: Card(
                      elevation: 4,
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          borderRadius:BorderRadius.circular(5),
                          color: Colors.white,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Row(
                                // mainAxisAlignment:MainAxisAlignment.center,
                                children: [
                                  Container(
                                    child: Icon(Icons.my_location_outlined,
                                        color: Colors.grey, size: 30),
                                    height: 40,
                                    width: 40,
                                  ),
                                     Container(child:Column(
                                       crossAxisAlignment: CrossAxisAlignment.start,
                                       mainAxisAlignment: MainAxisAlignment.spaceAround,
                                       children:[   Text(
                                          "Current Location Using Gps",
                                          style: TextStyle(color: Colors.grey, fontSize: 10),
                                        ),
                                        Text(
                                        "2715 Ash Dr.San South Dakata 83475",
                                          style: TextStyle(color: Colors.black, fontSize: 10),
                                        ),],
                                     ),
                                     )

                                ],
                              ),
                            ),
                            SingleChildScrollView(
                              child: Container(
                                child: ListView(
                                  shrinkWrap: true,
                                  children: <Widget>[
                                    Container(
                                      child: TextField(
                                        onChanged: (String value) {
                                          Name = value;
                                        },
                                        maxLines: 1,
                                        decoration: InputDecoration(
                                          hintText: "",
                                          labelText: "Name",
                                          labelStyle: TextStyle(color: Colors.black),
                                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                                          hintStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 10,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500),
                                          hintMaxLines: 1,
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide: new BorderSide(color: Colors.black87),
                                          ),
                                        ),
                                        style: TextStyle(color: Colors.black87),
                                      ),
                                    ),
                                    Container(
                                      child: TextField(
                                        onChanged: (String value) {
                                           phone = value;
                                          // return phone;
                                        },
                                        maxLines: 1,
                                        // maxLength: 10,
                                        keyboardType: TextInputType.number,
                                        decoration: InputDecoration(
                                            hintText: "",
                                            labelText: "Phone",
                                            prefix: CCPicker(
                                              color: Colors.black,
                                              cc: cc,
                                              onChanged: (value) {
                                                this.cc = value;
                                              },
                                            ),
                                            labelStyle: TextStyle(color: Colors.black),
                                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 10,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: new BorderSide(color: Colors.black),
                                            )),
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    ),
                                    Container(
                                      child: TextField(
                                        onChanged: (String value) {
                                          email = value;
                                        },
                                        maxLines: 1,
                                        decoration: InputDecoration(
                                            hintText: "",
                                            labelText: "Email ID",
                                            labelStyle: TextStyle(color: Colors.black),
                                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 10,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: new BorderSide(color: Colors.black),
                                            )),
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    ),
                                    Container(
                                      child: TextField(
                                        onChanged: (String value) {
                                           state = value;
                                        },
                                        decoration: InputDecoration(
                                            hintText: "",
                                            labelText: "State",
                                            labelStyle: TextStyle(color: Colors.black),
                                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 10,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: new BorderSide(color: Colors.black),
                                            )),
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    ),
                                    Container(
                                      child: TextField(
                                        onChanged: (String value) {
                                           country = value;
                                        },
                                        decoration: InputDecoration(
                                            hintText: "",
                                            labelText: "Country",
                                            labelStyle: TextStyle(color: Colors.black),
                                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 10,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: new BorderSide(color: Colors.black),
                                            )),
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    ),
                                    Container(
                                      child: TextField(
                                        onChanged: (String value) {
                                           street = value;
                                        },
                                        decoration: InputDecoration(
                                            hintText: "",
                                            labelText: "Street",
                                            labelStyle: TextStyle(color: Colors.black),
                                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 10,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: new BorderSide(color: Colors.black),
                                            )),
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(bottom: 10),
                                      child: TextField(
                                        onChanged: (String value) {
                                           postal_code = value;
                                        },
                                        decoration: InputDecoration(
                                            hintText: "",
                                            labelText: "Postal Code",
                                            labelStyle: TextStyle(color: Colors.black),
                                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 10,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w500,
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: new BorderSide(color: Colors.black),
                                            )),
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    ),

                                    // Container(
                                    //     padding: EdgeInsets.all(2),
                                    //     //margin: EdgeInsets.only(left: 230, bottom: 15),
                                    //     child: Text(
                                    //       "Forgot Password?",
                                    //       textAlign: TextAlign.center,
                                    //       style: TextStyle(
                                    //         color: Colors.black,
                                    //         fontSize: 15,
                                    //       ),
                                    //     )),

                                    // ),
                                    // Container(
                                    //   padding: EdgeInsets.only(top: 60),
                                    //   child: Text("Or Login With",
                                    //     textAlign: TextAlign.center,
                                    //     style: TextStyle(
                                    //       color: Colors.black,
                                    //     ),
                                    //   ),
                                    // ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                ),
              ),
            ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 45,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(3.0),
                ),
                color: Color(0xff3742FA),
              ),
              child: Center(
                child: Text(
                  "SAVE",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 20,
                    fontFamily: 'Poppins',
                  ),
                ),
              ),
            ),
          ),
            ],
          ),
        ),
      ),

    );
  }



  // bool isValidateAlldatas(BuildContext context) {
  //   if (isNullOrblackSpace(Name)) {
  //     _showDialog("Alert", "First Name can't be blank", context);
  //     return false;
  //   }  else if (isNullOrEmpty(phone) || phone.length < 10) {
  //     _showDialog("Alert", "Enter a valid phone number", context);
  //     return false;
  //   } else if (isNullOrblackSpace(email) || !isValidEmail(email)) {
  //     _showDialog("Alert", "Enter a valid email address", context);
  //     return false;
  //   } else if (isNullOrblackSpace(state)) {
  //     _showDialog("Alert", "Enter a state", context);
  //     return false;
  //   } else if (isNullOrblackSpace(country)) {
  //     _showDialog("Alert", "Enter the same password to confirm", context);
  //     return false;
  //   } else if (isNullOrblackSpace(street)) {
  //     _showDialog("Alert", "Password didn't matched", context);
  //     return false;
  //   } else if (isSecondBoxChecked == false) {
  //     _showDialog("Alert", "Please accept the Terms and Conditions", context);
  //     return false;
  //   }
  //   return (true);
  // }

  // void _showDialog(String title, String content, BuildContext context) {
  //   showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return AlertDialog(
  //         title: new Text(title),
  //         content: new Text(content),
  //         actions: <Widget>[
  //           FlatButton(
  //             child: new Text("OK"),
  //             onPressed: () {
  //               Navigator.of(context, rootNavigator: true).pop();
  //             },
  //           ),
  //         ],
  //       );
  //     },
  //   );
  // }
}
