import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ifada/screens/stateless_screen.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';

class TrackOrderScreen extends StateLessScreen {
  TrackOrderScreen(String name) : super(name);

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xffF4F4F4),
        width: double.infinity,
        height: double.infinity,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(left: 50, right: 15, top: 60),
              width: double.infinity,
              height: 80,
              //color: Colors.red,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                      Container(
                        child: Text(
                          "Ordered And Approved",
                          style: TextStyle(
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                            color: Color(0xff001833),
                          ),
                        ),
                      ),
                  //LinearProgressIndicator(),
                  FAProgressBar(
                    direction: Axis.vertical,
                    currentValue: 100,
                  ),
                  Container(
                    child: Text(
                      "Monday, Nov 10th 12:00 PM",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: "Poppins",
                        color: Color(0xff8D8F92),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 50, right: 15,),
              width: double.infinity,
              height: 80,
              //color: Colors.red,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      "Packed",
                      style: TextStyle(
                        fontFamily: "Poppins",
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: Color(0xff001833),
                      ),
                    ),
                  ),
                  Container(
                    child: Text(
                      "Monday, Nov 10th 2:00 PM",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: "Poppins",
                        color: Color(0xff8D8F92),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 50, right: 15,),
              width: double.infinity,
              height: 80,
              //color: Colors.red,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      "Shipped",
                      style: TextStyle(
                        fontFamily: "Poppins",
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: Color(0xff001833),
                      ),
                    ),
                  ),
                  Container(
                    child: Text(
                      "Monday, Nov 11 10:00 AM",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: "Poppins",
                        color: Color(0xff8D8F92),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 50, right: 15,),
              width: double.infinity,
              height: 80,
              //color: Colors.red,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      "Delivered",
                      style: TextStyle(
                        fontFamily: "Poppins",
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: Color(0xff001833),
                      ),
                    ),
                  ),
                  Container(
                    child: Text(
                      "Monday, Nov 11 12:00 PM",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: "Poppins",
                        color: Color(0xff8D8F92),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 5),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(5)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 10),
                    child: Text(
                      "Billing Address",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Poppins",
                        color: Color(0xff001833),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 15, right: 15, bottom: 15),
                    child: Text(
                      "1901 Thornridge Cir. Shiloh, Hawaii 81063",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: "Poppins",
                        color: Color(0xff8D8F92),
                      ),
                    ),
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: 150,
                          child: Text(
                            "Payment",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                color: Color(0xff8D8F92)),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 150),
                          child: Text(
                            "Online",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: Color(0xff3742FE)),
                          ),
                        )
                      ],
                    ),
                    margin: EdgeInsets.only(left: 15, right: 15, top: 15),
                    padding: EdgeInsets.only(bottom: 5),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                width: 2, color: Color(0xffEEEEEE)))),
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: 150,
                          child: Text(
                            "Order On",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                color: Color(0xff8D8F92)),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 120),
                          child: Text(
                            "04-01-2021",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: Color(0xff3742FE)),
                          ),
                        )
                      ],
                    ),
                    margin: EdgeInsets.only(left: 15, right: 15, top: 15),
                    padding: EdgeInsets.only(bottom: 5),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                width: 2, color: Color(0xffEEEEEE)))),
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: 150,
                          child: Text(
                            "Deliver To",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                color: Color(0xff8D8F92)),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 127),
                          child: Text(
                            "SASOC MI",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: Color(0xff3742FE)),
                          ),
                        )
                      ],
                    ),
                    margin: EdgeInsets.only(left: 15, right: 15, top: 15),
                    padding: EdgeInsets.only(bottom: 5),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                width: 2, color: Color(0xffEEEEEE)))),
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: 150,
                          child: Text(
                            "Mobile Number",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                color: Color(0xff8D8F92)),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 130, right: 5),
                          child: Text(
                            "12354698",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: Color(0xff3742FE)),
                          ),
                        )
                      ],
                    ),
                    margin: EdgeInsets.only(left: 15, right: 15, top: 15),
                    padding: EdgeInsets.only(bottom: 12),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}