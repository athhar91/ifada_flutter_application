import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ifada/helpers/app_color.dart';
import 'package:ifada/helpers/app_data_helper.dart';
import 'package:ifada/model/response/home_response.dart';
import 'package:ifada/widgets/banner_paginate_widget.dart';
import 'package:ifada/widgets/banner_widget.dart';
import 'package:ifada/widgets/product_widget.dart';
import 'package:shimmer/shimmer.dart';
import 'package:ifada/widgets/image_view.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../helpers/navigation_helper.dart';

import '../helpers/api_helper.dart';
import 'state_screen.dart';

class HomeScreen extends StateScreen {
  HomeScreen(String name) : super(name);

  @override
  HomeScreenState getState() => new HomeScreenState();
}

class HomeScreenState extends ScreenState {
  HomeResponse response = AppDataHelper.homeRespone;

  @override
  void initState() {
    getHomeData();
    super.initState();
  }

  getHomeData() async {
    ApiHelper<HomeResponse>(
        (res) => {
              setState(() {
                AppDataHelper.homeRespone = res;
                response = res;
              })
            },
        (error) => {},
        () => new HomeResponse()).getHomePage();
  }

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: response != null ?
      SingleChildScrollView(
              child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: [

                    // BANNER
                    Container(
                        width: double.infinity,
                        // color: Colors.red,
                        height: 270,
                        child: BannerPaginateWidget(
                          children: List.generate(response.banner.length,
                                  (index) => BannerWidget(response.banner[index])),
                        )),

                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                          margin: EdgeInsets.only(right: 5, top: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.all(5),
                                  child: SvgPicture.asset("assets/ic_search.svg", height: 25, width: 25,)
                              ),
                              Container(
                                  margin: EdgeInsets.all(5),
                                  child: SvgPicture.asset("assets/ic_notifications.svg", height: 25, width: 25,)
                              ),
                              InkWell(
                                onTap: (){
                                  widget.openScreen(NavigationHelper.myCartroute);
                                },
                                child: Container(
                                    margin: EdgeInsets.all(5),
                                    child: SvgPicture.asset("assets/white_cart.svg", height: 25, width: 25,)
                                ),
                              )
                            ],
                          )
                      ),
                    ),
                    // PRODUCTS CONTAINER

                    Container(
                      margin: EdgeInsets.only(top: 250),
                      decoration: BoxDecoration(
                        color: Color(0xffF4F4F4),
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0))
                      ),
                      child:
                      Container(
                        // color: Colors.green,
                        margin: EdgeInsets.only(top: 15, bottom: 10),
                        child: Column(
                          children: [
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(child: Container(
                                      margin: EdgeInsets.only(left: 15),
                                      child: Text(
                                        "Popular categories",
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "Poppins",
                                          color: AppColor.darkText,
                                        ),
                                      ))),
                                  Container(
                                    margin: EdgeInsets.only(top: 5, left: 25, right: 15),
                                    padding: EdgeInsets.only(bottom: 5),
                                    child: Text(
                                      "See More",
                                      style: TextStyle(
                                        fontFamily: "Poppins",
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12,
                                        color: Color(0xff3742FE),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              height: 140,
                              margin: EdgeInsets.only(left: 4,right: 4),
                              child: ListView(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                children: List.generate(response.popular_category.length, (index) {
                                  return Column(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.all(7),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(15.0),
                                          child: Container(
                                            //margin: EdgeInsets.only(left: 15, right: 5),
                                            height: 100,
                                            width: 90,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(0),
                                              // color: Colors.red,
                                            ),
                                            child: ImageView(url: response.popular_category[index].cat_image,),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 90,
                                        alignment: Alignment.center,
                                        child: Text(
                                          response.popular_category[index].name,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(color: Color(0xff8D8F92),fontSize : 10,),
                                          textAlign: TextAlign.center,
                                          maxLines: 1,
                                        ),
                                      )
                                    ],
                                  );
                                }),
                              ),
                            ),
                            // PRODUCT OF THE DAY

                            Column(
                              children: [
                                Container(
                                  // color: Colors.orange,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: Container(
                                            margin: EdgeInsets.only(left: 15),
                                            child: Text(
                                              "Product of the Day",
                                              style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "Poppins",
                                                color: Color(0xff001833),
                                              ),
                                            )),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 25, right: 15),
                                        padding: EdgeInsets.only(bottom: 5),
                                        child: Text(
                                          "See More",
                                          style: TextStyle(
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                            color: Color(0xff3742FE),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 4, right: 4),
                                  child: SizedBox(
                                      height: 225,
                                      child: GridView.count(
                                          padding: EdgeInsets.zero,
                                          physics: NeverScrollableScrollPhysics(),
                                          crossAxisCount: 2,
                                          childAspectRatio: 1/2,
                                          children: List.generate( 2 , (index) {
                                            return ProductWidget(response.product_of_the_day[index].toProduct(),response.country_data[0].currency);
                                          }))),
                                ),
                              ],
                            ),

                            // PET STORE

                            Column(
                              children: List.generate(response.mobile_app_product.length,
                                      (index) {
                                    return Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              // color: Colors.orange,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Expanded(
                                                    child: Container(
                                                        margin: EdgeInsets.only(top: 5, left: 15),
                                                        padding: EdgeInsets.only(bottom: 5),
                                                        child: Text(
                                                          response.mobile_app_product[index].name,
                                                          style: TextStyle(
                                                            fontSize: 18,
                                                            fontWeight: FontWeight.w500,
                                                            fontFamily: "Poppins",
                                                            color: Color(0xff001833),
                                                          ),
                                                        )),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(top: 5, left: 25, right: 15),
                                                    padding: EdgeInsets.only(bottom: 5),
                                                    child: Text(
                                                      "See More",
                                                      style: TextStyle(
                                                        fontFamily: "Poppins",
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 12,
                                                        color: Color(0xff3742FE),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            SizedBox(
                                                height: 230,
                                                child: Container(
                                                  margin: EdgeInsets.only(left: 4,right: 4),
                                                  child: GridView.count(
                                                      physics: NeverScrollableScrollPhysics(),
                                                      padding: EdgeInsets.zero,
                                                      crossAxisCount: 2,
                                                      childAspectRatio: 1/2,
                                                      children: List.generate(
                                                          response.mobile_app_product[index].product_data.length > 3 ? 3
                                                              :
                                                          response.mobile_app_product[index].product_data.length, (childIndex) {
                                                        //var item = response.mobile_app_product[index].product_data[childIndex];
                                                        return ProductWidget(response.mobile_app_product[index].product_data[childIndex].toProduct(), response.country_data[0].currency);/*Stack(
                                                          children: <Widget>[
                                                            Container(
                                                              // color: Colors.blue,
                                                              height: double.infinity,
                                                              child: Card(
                                                                elevation: 1,
                                                                margin: EdgeInsets.only(left: 8, right: 8),
                                                                child: Column(
                                                                  children: <Widget>[
                                                                    Container(
                                                                      alignment: Alignment.topLeft,
                                                                      height: 14,
                                                                      width: 30,
                                                                      margin: EdgeInsets.only(right: 85, top: 5),
                                                                      padding: EdgeInsets.only(left: 2, top: 2),
                                                                      decoration: BoxDecoration(
                                                                        borderRadius:
                                                                        BorderRadius.circular(20),
                                                                        color: Color(0x73C4C4C4),
                                                                      ),
                                                                      child: Text(
                                                                        item.id + "gm",
                                                                        // item.menu_varient_data.unit_value,
                                                                        // item.menu_varient_data.quantity,
                                                                        style: TextStyle(
                                                                            color: Colors.black,
                                                                            fontSize: 8,
                                                                            fontFamily: "Poppins",
                                                                            fontWeight: FontWeight.w400),
                                                                        textAlign: TextAlign.center,
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          top: 7, bottom: 3),
                                                                      width: 118,
                                                                      height: 98,
                                                                      child: ImageView(
                                                                        url: item.product_image,
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          right: 2, left: 2, bottom: 2),
                                                                      padding: EdgeInsets.all(5),
                                                                      child: Row(
                                                                        children: <Widget>[
                                                                          Container(
                                                                            width: 85,
                                                                            child: Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: <Widget>[
                                                                                Text(
                                                                                  response.country_data[0].currency+" "+response.mobile_app_product[index].product_data[childIndex].menu_varient_data.price,
                                                                                  style: TextStyle(
                                                                                    color:
                                                                                    Color(0xff8D8F92),
                                                                                    fontWeight:
                                                                                    FontWeight.w400,
                                                                                    fontFamily: "Poppins",
                                                                                    fontSize: 8,
                                                                                  ),
                                                                                ),
                                                                                Container(
                                                                                  alignment: Alignment.centerLeft,
                                                                                  height: 30,
                                                                                  child: Text(
                                                                                    item.menu_name,
                                                                                    maxLines: 2,
                                                                                    style: TextStyle(
                                                                                      color: Colors.black,
                                                                                      fontWeight:
                                                                                      FontWeight.w400,
                                                                                      fontFamily: "Poppins",
                                                                                      fontSize: 10,
                                                                                    ),
                                                                                  ),
                                                                                )
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                            alignment: Alignment.center,
                                                                            margin:
                                                                            EdgeInsets.only(left: 2, top: 20),
                                                                            child: Image.asset(
                                                                                "assets/blue_cart.png"),
                                                                            height: 15,
                                                                            width: 20,
                                                                          )
                                                                        ],
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            Align(
                                                              alignment: Alignment.topRight,
                                                              child: Container(
                                                                margin: EdgeInsets.only(left: 110),
                                                                height: 28,
                                                                width: 28,
                                                                decoration: BoxDecoration(
                                                                    color: Colors.white,
                                                                    borderRadius:
                                                                    BorderRadius.circular(20),
                                                                    boxShadow: [
                                                                      BoxShadow(
                                                                        color: Colors.grey,
                                                                        blurRadius: 5.0,
                                                                      ),
                                                                    ]),
                                                                child: Image.asset(
                                                                    "assets/likedButton_image.svg"),
                                                              ),
                                                            ),
                                                          ],
                                                        );*/
                                                      })),
                                                )),
                                          ],
                                        )
                                      ],
                                    );
                                  }),
                            ),

                            // 2nd BANNER

                            Container(
                              width: double.infinity,
                              height: 220,
                              margin: EdgeInsets.all(7),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(15.0),
                              child: BannerPaginateWidget(
                                children: List.generate(response.offer.length, (index){
                                  return Container(
                                      //margin: EdgeInsets.only(left: 15, right: 5),
                                      height: 220,
                                      width: double.infinity,
                                    child: ImageView(
                                      url: response.offer[index].ad_mobile_image,
                                      fit: BoxFit.fill,
                                    ),
                                    );
                                }),
                              ),
                            ),
                            ),

                            // BEST PICK OF THE SEASON

                            Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 15),
                                  // color: Colors.orange,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: Container(
                                            margin: EdgeInsets.only(top: 5, left: 15),
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Text(
                                              "Best Pick of The Season",
                                              style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "Poppins",
                                                color: Color(0xff001833),
                                              ),
                                            )),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 5, left: 25, right: 15),
                                        padding: EdgeInsets.only(bottom: 5),
                                        child: Text(
                                          "See More",
                                          style: TextStyle(
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                            color: Color(0xff3742FE),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                    height: 230,
                                    child: Container(
                                      margin: EdgeInsets.only(left: 4,right: 4),
                                      child: GridView.count(
                                          physics: NeverScrollableScrollPhysics(),
                                          padding: EdgeInsets.zero,
                                          crossAxisCount: 2,
                                          childAspectRatio: 1/2,
                                          children: List.generate( 3 , (index) {
                                            return ProductWidget(response.best_picks_of_the_season[index].toProduct(), response.country_data[0].currency);/*Stack(
                                              children: <Widget>[
                                                Container(
                                                  // color: Colors.blue,
                                                  height: double.infinity,
                                                  child: Card(
                                                    elevation: 1,
                                                    margin: EdgeInsets.only(left: 8, right: 8),
                                                    child: Column(
                                                      children: <Widget>[
                                                        Container(
                                                          alignment: Alignment.topLeft,
                                                          height: 14,
                                                          width: 30,
                                                          margin: EdgeInsets.only(right: 85, top: 5),
                                                          padding: EdgeInsets.only(left: 2, top: 2),
                                                          decoration: BoxDecoration(
                                                            borderRadius:
                                                            BorderRadius.circular(20),
                                                            color: Color(0x73C4C4C4),
                                                          ),
                                                          child: Text(
                                                            response.best_picks_of_the_season[index].menu_varient_data.quantity +" "+"gm",
                                                            // item.menu_varient_data.unit_value,
                                                            // item.menu_varient_data.quantity,
                                                            style: TextStyle(
                                                                color: Colors.black,
                                                                fontSize: 8,
                                                                fontFamily: "Poppins",
                                                                fontWeight: FontWeight.w400),
                                                            textAlign: TextAlign.center,
                                                          ),
                                                        ),
                                                        Container(
                                                          margin: EdgeInsets.only(
                                                              top: 7, bottom: 3),
                                                          width: 118,
                                                          height: 98,
                                                          child: ImageView(
                                                            url: response.best_picks_of_the_season[index].product_image,
                                                          ),
                                                        ),
                                                        Container(
                                                          margin: EdgeInsets.only(
                                                              right: 2, left: 2, bottom: 2),
                                                          padding: EdgeInsets.all(5),
                                                          child: Row(
                                                            children: <Widget>[
                                                              Container(
                                                                //color: Colors.red,
                                                                width: 85,
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: <Widget>[
                                                                    Text(
                                                                      response.country_data[0].currency+" "+response.best_picks_of_the_season[index].menu_varient_data.price,
                                                                      style: TextStyle(
                                                                        color: Color(
                                                                            0xff8D8F92),
                                                                        fontWeight: FontWeight.w400,
                                                                        fontFamily: "Poppins",
                                                                        fontSize: 8,
                                                                      ),
                                                                    ),
                                                                    Container(

                                                                      //color: Colors.green,

                                                                      alignment: Alignment.centerLeft,
                                                                      height: 30,
                                                                      child: Text(
                                                                        response.best_picks_of_the_season[index].menu_name,
                                                                        maxLines: 2,
                                                                        style: TextStyle(
                                                                          color: Colors.black,
                                                                          fontWeight: FontWeight.w400,
                                                                          fontFamily: "Poppins",
                                                                          fontSize: 10,
                                                                        ),
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                              Container(
                                                                //color: Colors.blue,
                                                                alignment: Alignment.center,
                                                                margin: EdgeInsets.only(
                                                                    left: 2, top: 20),
                                                                child: Image.asset(
                                                                  "assets/blue_cart.png",
                                                                  color: Color(0xff57606F),
                                                                ),
                                                                height: 15,
                                                                width: 20,)
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Align(
                                                  alignment: Alignment.topRight,
                                                  child: Container(
                                                    margin: EdgeInsets.only(left: 110),
                                                    height: 28,
                                                    width: 28,
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                        BorderRadius.circular(20),
                                                        boxShadow: [
                                                          BoxShadow(
                                                            color: Colors.grey,
                                                            blurRadius: 5.0,
                                                          ),
                                                        ]),
                                                    child: Image.asset(
                                                        "assets/likedButton_image.svg"),
                                                  ),
                                                ),
                                              ],
                                            );*/
                                          })),
                                    )),
                              ],
                            ),

                            // BEST PICKS OF THE FEATURE PRODUCTS

                            Column(
                              children: [
                                Container(
                                  // color: Colors.orange,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: Container(
                                            margin: EdgeInsets.only(top: 5, left: 15),
                                            padding: EdgeInsets.only(bottom: 5),
                                            child: Text(
                                              "Best Pick of the Featured Products",
                                              style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "Poppins",
                                                color: Color(0xff001833),
                                              ),
                                            )),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 5, left: 25, right: 15),
                                        padding: EdgeInsets.only(bottom: 5),
                                        child: Text(
                                          "See More",
                                          style: TextStyle(
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12,
                                            color: Color(0xff3742FE),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 230,
                                    child: Container(
                                      margin: EdgeInsets.only(left: 4,right: 4),
                                      child: GridView.count(
                                          physics: NeverScrollableScrollPhysics(),
                                          padding: EdgeInsets.zero,
                                          crossAxisCount: 2,
                                          childAspectRatio: 1/2,
                                          children: List.generate( 3 , (index) {
                                            return ProductWidget(response.best_picks_of_the_featured_products[index].toProduct(), response.country_data[0].currency);/*Stack(
                                              children: <Widget>[
                                                Container(
                                                  // color: Colors.blue,
                                                  height: double.infinity,
                                                  child: Card(
                                                    elevation: 1,
                                                    margin: EdgeInsets.only(left: 8, right: 8),
                                                    child: Column(
                                                      children: <Widget>[
                                                        Container(
                                                          alignment: Alignment.topLeft,
                                                          height: 14,
                                                          width: 30,
                                                          margin: EdgeInsets.only(right: 85, top: 5),
                                                          padding: EdgeInsets.only(left: 2, top: 2),
                                                          decoration: BoxDecoration(
                                                            borderRadius:
                                                            BorderRadius.circular(20),
                                                            color: Color(0x73C4C4C4),
                                                          ),
                                                          child: Text(
                                                            response.best_picks_of_the_featured_products[index].menu_varient_data.quantity +" "+"gm",
                                                            // item.menu_varient_data.unit_value,
                                                            // item.menu_varient_data.quantity,
                                                            style: TextStyle(
                                                                color: Colors.black,
                                                                fontSize: 8,
                                                                fontFamily: "Poppins",
                                                                fontWeight: FontWeight.w400),
                                                            textAlign: TextAlign.center,
                                                          ),
                                                        ),
                                                        Container(
                                                          margin: EdgeInsets.only(
                                                              top: 7, bottom: 3),
                                                          width: 118,
                                                          height: 98,
                                                          child: ImageView(
                                                            url: response.best_picks_of_the_featured_products[index].product_image,
                                                          ),
                                                        ),
                                                        Container(
                                                          margin: EdgeInsets.only(
                                                              right: 2, left: 2, bottom: 2),
                                                          padding: EdgeInsets.all(5),
                                                          child: Row(
                                                            children: <Widget>[
                                                              Container(
                                                                //color: Colors.red,
                                                                width: 85,
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: <Widget>[
                                                                    Text(
                                                                      response.country_data[0].currency+" "+response.best_picks_of_the_featured_products[index].menu_varient_data.price,
                                                                      style: TextStyle(
                                                                        color: Color(
                                                                            0xff8D8F92),
                                                                        fontWeight: FontWeight.w400,
                                                                        fontFamily: "Poppins",
                                                                        fontSize: 8,
                                                                      ),
                                                                    ),
                                                                    Container(

                                                                      //color: Colors.green,

                                                                      alignment: Alignment.centerLeft,
                                                                      height: 30,
                                                                      child: Text(
                                                                        response.best_picks_of_the_featured_products[index].menu_name,
                                                                        maxLines: 2,
                                                                        style: TextStyle(
                                                                          color: Colors.black,
                                                                          fontWeight: FontWeight.w400,
                                                                          fontFamily: "Poppins",
                                                                          fontSize: 10,
                                                                        ),
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                              Container(
                                                                //color: Colors.blue,
                                                                alignment: Alignment.center,
                                                                margin: EdgeInsets.only(
                                                                    left: 2, top: 20),
                                                                child: Image.asset(
                                                                  "assets/blue_cart.png",
                                                                  color: Color(0xff57606F),
                                                                ),
                                                                height: 15,
                                                                width: 20,)
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Align(
                                                  alignment: Alignment.topRight,
                                                  child: Container(
                                                    margin: EdgeInsets.only(left: 110),
                                                    height: 28,
                                                    width: 28,
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius:
                                                        BorderRadius.circular(20),
                                                        boxShadow: [
                                                          BoxShadow(
                                                            color: Colors.grey,
                                                            blurRadius: 5.0,
                                                          ),
                                                        ]),
                                                    child: Image.asset(
                                                        "assets/likedButton_image.svg"),
                                                  ),
                                                ),
                                              ],
                                            );*/
                                          })),
                                    )),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                )
              ],
            ))
          : SingleChildScrollView(
        child: Shimmer.fromColors(child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(width: double.infinity,height: 220,color: Colors.black26,),
            Container(width: 200,height: 40, margin: EdgeInsets.only(top: 20,left: 6),color: Colors.black26),
            Container(
              height: 140,
              margin: EdgeInsets.all(4),
              child: ListView(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                children: List.generate(6, (index) {
                  return ClipRRect(borderRadius:BorderRadius.all(Radius.circular(16)),child: Container(height: 140,width: 90,  margin: EdgeInsets.all(4),color: Colors.black26,));
                }),
              ),
            ),
            SizedBox(
                height: 230,
                child: GridView.count(
                    physics: NeverScrollableScrollPhysics(),
                    crossAxisCount: 2, childAspectRatio: 1/2,
                    children: List.generate(3,(childIndex) {
                      return Container(height: 230, margin: EdgeInsets.only(left: 6,right: 6),color: Colors.black26,);
                    }))
            ),
            Container(width: 200,height: 40, margin: EdgeInsets.only(top: 20,left: 6),color: Colors.black26),
            SizedBox(
                height: 230,
                child: GridView.count(
                    physics: NeverScrollableScrollPhysics(),
                    crossAxisCount: 2, childAspectRatio: 1/2,
                    children: List.generate(3,(childIndex) {
                      return Container(height: 230, margin: EdgeInsets.only(left: 6,right: 6),color: Colors.black26,);
                    }))
            )
          ],
        ), baseColor: Colors.black26, highlightColor: Colors.white),
      ),
    );
  }
}
