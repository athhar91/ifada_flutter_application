import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ifada/helpers/navigation_helper.dart';
import 'package:ifada/helpers/preference_helper.dart';

import '../helpers/dialog_helper.dart';
import '../helpers/preference_helper.dart';
import '../model/User.dart';
import 'state_screen.dart';

class MyAccount extends StateScreen {
  MyAccount(String name) : super(name);

  @override
  MyAccountState getState() => new MyAccountState();
}

class MyAccountState extends ScreenState {
  User _user = null;
  String first_name = "";
  String last_name = "";
  String email = "";
  String phone = "";

  @override
  void initState() {
    super.initState();
    getUser();
  }

  void getUser() async {
    var userData = await PreferenceHelper.getUserData();
    setState(() {
      _user = userData;
      first_name = _user.first_name;
      last_name = _user.last_name;
      email = _user.email;
      phone = _user.phone;
    });
  }

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Color(0xffE5E5E5),
        child: Container(
          child: SingleChildScrollView(
            child: Center(
              child: Stack(
                children: [
                  Column(
                    children: <Widget>[
                      // Container(
                      //   margin: EdgeInsets.only(top: 60),
                      //   child: Row(
                      //     children: <Widget>[
                      //       Container(
                      //         margin: EdgeInsets.only(
                      //           left: 10,
                      //           right: 10,
                      //         ),
                      //         width: 40,
                      //         height: 40,
                      //         child: Image.asset("assets/Group 1482.png"),
                      //       ),
                      //       Container(
                      //         margin: EdgeInsets.only(
                      //           right: 80,
                      //         ),
                      //         child: Text(
                      //           "My Account",
                      //           style: TextStyle(
                      //               fontWeight: FontWeight.w500,
                      //               fontFamily: 'Poppins',
                      //               color: Colors.black,
                      //               fontSize: 18),
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      Container(
                        margin: EdgeInsets.only(top: 125),
                        child: Center(
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(5),
                                    topRight: Radius.circular(5)),
                                color: Colors.white),
                            width: double.infinity,
                            margin: EdgeInsets.only(
                              left: 15,
                              right: 15,
                              bottom: 2,
                            ),
                            child: Container(
                              margin: EdgeInsets.only(top: 80),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    "Hi\ $first_name $last_name",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18,
                                        fontFamily: "Poppins"),
                                  ),
                                  Text(
                                    "\ $email",
                                    style: TextStyle(
                                        color: Color(0xff8D8F92),
                                        fontFamily: "Poppins",
                                        fontSize: 12),
                                  ),
                                  Text(
                                    "\ $phone",
                                    style: TextStyle(
                                        color: Color(0xff8D8F92),
                                        fontFamily: "Poppins",
                                        fontSize: 12),
                                  ),

                                  Container(
                                    margin: EdgeInsets.only(top: 30, bottom: 5),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            child: Row(
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets.all(8.0),
                                                  child: Container(
                                                    child:
                                                    Image.asset("assets/ic_wallet.png"),
                                                    padding: EdgeInsets.only(left: 10),
                                                    height: 27,
                                                    width: 27,
                                                  ),
                                                ),
                                                Container(
                                                  padding: EdgeInsets.only(left: 10),
                                                  child: Text(
                                                    "My Wallet",
                                                    style: TextStyle(
                                                        color: Color(0xff8D8F92),
                                                        fontWeight: FontWeight.w400,
                                                        fontSize: 15),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          //color: Colors.red,

                                          child: Image.asset(
                                              "assets/ic_right_arrow.png",
                                          ),
                                          padding: EdgeInsets.all(10),
                                          height: 34,
                                          width: 34,
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          widget.openScreen(NavigationHelper.myAddress);
                        },
                        child: Container(
                          color: Colors.white,
                          width: double.infinity,
                          margin: EdgeInsets.only(
                            left: 15,
                            right: 15,
                            bottom: 2,
                          ),
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(child:Container(
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        child: SvgPicture.asset("assets/address_image.svg"),
                                        padding: EdgeInsets.only(left: 10),
                                        height: 27,
                                        width: 27,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                        "My Address",
                                        style: TextStyle(
                                            color: Color(0xff8D8F92),
                                            fontWeight: FontWeight.w400,
                                            fontSize: 15),
                                      ),
                                    ),
                                  ],
                                ),),
                              ),
                              Container(
                                child: Image.asset("assets/ic_right_arrow.png"),
                                padding: EdgeInsets.all(10),
                                height: 34,
                                width: 34,
                              )
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: (){
                          widget.openScreen(NavigationHelper.myReward);
                        },
                        child: Container(
                          color: Colors.white,
                          width: double.infinity,
                          margin: EdgeInsets.only(
                            left: 15,
                            right: 15,
                            bottom: 2,
                          ),
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                          child: SvgPicture.asset("assets/reward_image.svg"),
                                          padding: EdgeInsets.only(left: 10),
                                          height: 27,
                                          width: 27,
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(left: 10),
                                        child: Text(
                                          "My Reward",
                                          style: TextStyle(
                                              color: Color(0xff8D8F92),
                                              fontWeight: FontWeight.w400,
                                              fontSize: 15),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                child: Image.asset("assets/ic_right_arrow.png"),
                                padding: EdgeInsets.all(10),
                                height: 34,
                                width: 34,
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        width: double.infinity,
                        margin: EdgeInsets.only(
                          left: 15,
                          right: 15,
                          bottom: 2,
                        ),
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                           Expanded(
                             child: Container(
                               child: Row(
                                 children: [
                                   Padding(
                                     padding: const EdgeInsets.all(8.0),
                                     child: Container(
                                       child: Image.asset("assets/creditLimit_image.png"),
                                       padding: EdgeInsets.only(left: 10),
                                       height: 27,
                                       width: 27,
                                     ),
                                   ),
                                   Container(
                                     padding: EdgeInsets.only(left: 10),
                                     child: Text(
                                       "My Credit Limit",
                                       style: TextStyle(
                                           color: Color(0xff8D8F92),
                                           fontWeight: FontWeight.w400,
                                           fontSize:15),
                                     ),
                                   ),
                                 ],
                               ),
                             ),
                           ),
                            Container(

                              child: Image.asset("assets/ic_right_arrow.png"),
                              padding: EdgeInsets.all(10),
                              height: 34,
                              width: 34,
                            )
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          //widget.openScreen(NavigationHelper.myOrdersroute);
                        },
                        child: Container(
                          color: Colors.white,
                          width: double.infinity,
                          margin: EdgeInsets.only(
                            left: 15,
                            right: 15,
                            bottom: 2,
                          ),
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                          child: SvgPicture.asset("assets/order_image.svg"),
                                          padding: EdgeInsets.only(left: 10),
                                          height: 27,
                                          width: 27,
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(left: 10),
                                        child: Text(
                                          "My Order",
                                          style: TextStyle(
                                              color: Color(0xff8D8F92),
                                              fontWeight: FontWeight.w400,
                                              fontSize: 15),
                                        ),
                                      ),
                                    ],
                                ),
                                ),
                              ),
                              Container(

                                child: Image.asset("assets/ic_right_arrow.png"),
                                padding: EdgeInsets.all(10),
                                height: 34,
                                width: 34,
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        width: double.infinity,
                        margin: EdgeInsets.only(
                          left: 15,
                          right: 15,
                          bottom: 2,
                        ),
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        child: SvgPicture.asset("assets/shoppingList_image.svg"),
                                        padding: EdgeInsets.only(left: 10),
                                        height: 27,
                                        width: 27,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                        "Shopping List",
                                        style: TextStyle(
                                            color: Color(0xff8D8F92),
                                            fontWeight: FontWeight.w400,
                                            fontSize: 15),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(

                              child: Image.asset("assets/ic_right_arrow.png"),
                              padding: EdgeInsets.all(10),
                              height: 34,
                              width: 34,
                            )
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        width: double.infinity,
                        margin: EdgeInsets.only(
                          left: 15,
                          right: 15,
                          bottom: 2,
                        ),
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  child: Row(
                                    children: [
                                      Container(
                                        child: SvgPicture.asset("assets/manageTickets_image.svg"),
                                        padding: EdgeInsets.only(left: 10),
                                        height: 27,
                                        width: 27,
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(left: 10),
                                        child: Text(
                                          "Manage Tickets",
                                          style: TextStyle(
                                              color: Color(0xff8D8F92),
                                              fontWeight: FontWeight.w400,
                                              fontSize: 15),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Container(

                              child: Image.asset("assets/ic_right_arrow.png"),
                              padding: EdgeInsets.all(10),
                              height: 34,
                              width: 34,
                            )
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        width: double.infinity,
                        margin: EdgeInsets.only(
                          left: 15,
                          right: 15,
                          bottom: 2,
                        ),
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        child: SvgPicture.asset("assets/coupon_image.svg"),
                                        padding: EdgeInsets.only(left: 10),
                                        height:27,
                                        width: 27,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                        "My Coupons",
                                        style: TextStyle(
                                            color: Color(0xff8D8F92),
                                            fontWeight: FontWeight.w400,
                                            fontSize: 15),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(

                              child: Image.asset("assets/ic_right_arrow.png"),
                              padding: EdgeInsets.all(10),
                              height: 34,
                              width: 34,
                            )
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        width: double.infinity,
                        margin: EdgeInsets.only(
                          left: 15,
                          right: 15,
                          bottom: 2,
                        ),
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                           Expanded(
                             child: Container(
                               child: Row(
                                 children: [
                                   Padding(
                                     padding: const EdgeInsets.all(8.0),
                                     child: Container(
                                       child: Image.asset("assets/deliveryMan_image.png"),
                                       padding: EdgeInsets.only(left: 10),
                                       height: 27,
                                       width: 27,
                                     ),
                                   ),
                                   Container(
                                     padding: EdgeInsets.only(left: 10),
                                     child: Text(
                                       "Manage Delivery Person",
                                       style: TextStyle(
                                           color: Color(0xff8D8F92),
                                           fontWeight: FontWeight.w400,
                                           fontSize:15),
                                     ),
                                   ),
                                 ],
                               ),
                             ),
                           ),
                            Container(

                              child: Image.asset("assets/ic_right_arrow.png"),
                              padding: EdgeInsets.all(10),
                              height: 34,
                              width: 34,
                            )
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        width: double.infinity,
                        margin: EdgeInsets.only(
                          left: 15,
                          right: 15,
                          bottom: 2,
                        ),
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                           Expanded(
                             child: Container(
                               child: Row(
                                 children: [
                                   Padding(
                                     padding: const EdgeInsets.all(8.0),
                                     child: Container(
                                       child: Image.asset("assets/assignOrder_image.png"),
                                       padding: EdgeInsets.only(left: 10),
                                       height: 27,
                                       width:27,
                                     ),
                                   ),
                                   Container(
                                     padding: EdgeInsets.only(left: 10),
                                     child: Text(
                                       "Manage Assign Order",
                                       style: TextStyle(
                                           color: Color(0xff8D8F92),
                                           fontWeight: FontWeight.w400,
                                           fontSize: 15),
                                     ),
                                   ),
                                 ],
                               ),
                             ),
                           ),
                            Container(

                              child: Image.asset("assets/ic_right_arrow.png"),
                              padding: EdgeInsets.all(10),
                              height: 34,
                              width: 34,
                            )
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        width: double.infinity,
                        margin: EdgeInsets.only(
                          left: 15,
                          right: 15,
                          bottom: 2,
                        ),
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        child: SvgPicture.asset("assets/location_image.svg"),
                                        padding: EdgeInsets.only(left: 10),
                                        height: 27,
                                        width: 27,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                        "Location",
                                        style: TextStyle(
                                            color: Color(0xff8D8F92),
                                            fontWeight: FontWeight.w400,
                                            fontSize:15),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(

                              child: Row(
                                children: [
                                  Container(
                                    child: Text(
                                      "Saudi Arabia",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 15,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Image.asset("assets/ic_right_arrow.png"),
                                    padding: EdgeInsets.all(10),
                                    height: 34,
                                    width: 34,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        width: double.infinity,
                        margin: EdgeInsets.only(
                          left: 15,
                          right: 15,
                          bottom: 2,
                        ),
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        child: Image.asset("assets/language_image.png"),
                                        padding: EdgeInsets.only(left: 10),
                                        height: 27,
                                        width: 27,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                        "Language",
                                        style: TextStyle(
                                            color: Color(0xff8D8F92),
                                            fontWeight: FontWeight.w400,
                                            fontSize: 15),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                    child: Text(
                                      "English",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 15,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Image.asset("assets/ic_right_arrow.png"),
                                    padding: EdgeInsets.all(10),
                                    height: 34,
                                    width: 34,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(5),
                                bottomRight: Radius.circular(5)),
                            color: Colors.white),
                        width: double.infinity,
                        margin: EdgeInsets.only(
                          left: 15,
                          right: 15,
                          bottom: 2,
                        ),
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        child: Image.asset("assets/legal_image.png"),
                                        padding: EdgeInsets.only(left: 10),
                                        height: 27,
                                        width: 27,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                        "Legal",
                                        style: TextStyle(
                                            color: Color(0xff8D8F92),
                                            fontWeight: FontWeight.w400,
                                            fontSize: 15),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              child: Image.asset("assets/ic_right_arrow.png"),
                              padding: EdgeInsets.all(10),
                              height: 34,
                              width: 34,
                            )
                          ],
                        ),
                      ),
                      InkWell(
                        child: Container(
                          margin: EdgeInsets.only(
                              top: 40, left: 16, right: 16, bottom: 40),
                          padding: EdgeInsets.only(top: 10),
                          height: 45,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(3.0),
                            ),
                            color: Color(0xff3742FA),
                          ),
                          child: Text(
                            "LOGOUT",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                              fontFamily: 'Poppins',
                            ),
                          ),
                        ),
                        onTap: () => {
                          DialogHelper.logoutAlertDialog(context),
                          // PreferenceHelper.clearUser(),
                          // NavigationHelper.launchNewScreen(context, NavigationHelper.loginRoute)
                        },
                      ),
                    ],
                  ),
                  Align(
                      alignment: Alignment.center,
                      child: Container(
                        margin: EdgeInsets.only(top: 70.0),
                        child: Card(
                          elevation: 8,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(75.0),
                          ),
                          child: Container(
                            width: 100,
                            height: 100,
                            // decoration: BoxDecoration(
                            //     shape: BoxShape.circle,
                            //     image: new DecorationImage(
                            //       image: new ExactAssetImage(
                            //           'assets/userProfile_image.png'),
                            //       fit: BoxFit.fill,
                            //     )),
                            child: SvgPicture.asset("assets/user_place_holder.svg"),
                          ),
                        ),
                      )
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
