

import 'package:flutter/material.dart';
import 'package:ifada/helpers/navigation_helper.dart';
import 'package:ifada/screens/stateless_screen.dart';

class OrderCancelScreen extends StateLessScreen{
  OrderCancelScreen(String name) : super(name);


  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        //color: Color(0xffE5E5E5),
        child: Center(
          child: Container(
            // color: Colors.deepOrange,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 220),
                  padding: EdgeInsets.all(27),
                  height: 160,
                  width: 160,
                  decoration: BoxDecoration(shape: BoxShape.circle, color: Color(0x1AF4323B)),
                  child: Image.asset("assets/ic_cancel.png"),
                ),
                Container(
                  margin: EdgeInsets.only(top: 30),
                  child: Text(
                      "Order Cancelled",
                    style: TextStyle(
                      fontSize: 26,
                      fontWeight: FontWeight.w500,
                      fontFamily: "Poppins",
                      color: Color(0xff001833)
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 25, left: 55, right: 55),
                  child: Text(
                    "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.",
                    style: TextStyle(
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w400,
                      fontSize: 16,
                      color: Color(0xff8D8F92),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                InkWell(
                  onTap: () {
                    openScreen(NavigationHelper.homeScreenroute);
                  },
                  child: Container(
                    height: 40,
                    width: double.infinity,
                    margin: EdgeInsets.only(left: 15, right: 15, top: 145),
                    padding: EdgeInsets.only(top: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      color: Color(0xff3742FE),
                    ),
                    child: Text(
                        "GO TO HOMEPAGE",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Poppins",
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    openScreen(NavigationHelper.myOrdersroute);
                  },
                  child: Container(
                    height: 40,
                    width: double.infinity,
                    margin: EdgeInsets.only(left: 15, right: 15, top: 10),
                    padding: EdgeInsets.only(top: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      color: Color(0xffEEEFFF),
                    ),
                    child: Text(
                      "MY ORDERS",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Poppins",
                        color: Color(0xff3742FE)
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}