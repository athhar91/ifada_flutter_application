import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ifada/helpers/api_helper.dart';
import 'package:ifada/helpers/app_data_helper.dart';
import 'package:ifada/helpers/navigation_helper.dart';
import 'package:ifada/model/response/BasicResponse.dart';
import 'package:ifada/model/response/my_wishlist.dart';
import 'package:ifada/screens/state_screen.dart';
import 'package:ifada/screens/stateless_screen.dart';
import 'package:ifada/widgets/product_widget.dart';

class ProductsScreen extends StateScreen {
  ProductsScreen(String name) : super(name);
  MyWishList myWishList = null;

  @override
  ScreenState<StateScreen> getState() {
    return FavProductsScreenState();
  }
}

class FavProductsScreenState extends ScreenState<ProductsScreen> {
  @override
  void initState() {
    ApiHelper<MyWishList>((res) {
      setState(() {
        widget.myWishList = res;
      });
    }, (error) {}, () => new MyWishList()).getWishList(AppDataHelper.user.id);
    super.initState();
  }

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
          width: double.infinity,
          height: double.infinity,
          color: Color(0xffE5E5E5),
          child: widget.myWishList != null
              ? GridView.count(
                  padding: EdgeInsets.all(0),
                  crossAxisCount: 2,
                  childAspectRatio:
                      MediaQuery.of(context).size.width / (2 * 240),
                  children: List.generate(
                      widget.myWishList.wishlist.length, (index) {
                    return ProductWidget(
                        widget.myWishList.wishlist[index].product, "");
                  }),
                )
              : Container()),
    );
  }
}
