import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ifada/helpers/app_color.dart';
import 'package:ifada/model/category.dart';
import 'package:ifada/screens/stateless_screen.dart';
import 'package:ifada/widgets/image_view.dart';

import '../helpers/navigation_helper.dart';

class SubCategoryScreen extends StateLessScreen {
  Category select = null;
  List<Category> categories = [];

  List colors = [
    Color(0x44C84E89),
    Color(0x44004E8F),
    Color(0x44A811DA),
    Color(0x446FB1FC),
    Color(0x44FFA751)
  ];
  int indexLocked;

  SubCategoryScreen(String name, Object args) : super(name) {
    if (args is Map) {
      if (args.containsKey("category")) select = args["category"];
      if (args.containsKey("categories")) categories = args["categories"];
      print(categories[0]);
    }
  }

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
        body: Container(
      width: double.infinity,
      height: double.infinity,
      color: AppColor.bg,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 250,
              color: Color(0xffF4F4F4),
              child: ImageView(
                height: double.infinity,
                width: double.infinity,
                url: select.cat_image,
                placeHolder: "assets/shopping-bag.png",
              ),
            ),
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30)),
                  color: Color(0xffF4F4F4)),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "Categories",
                        style: TextStyle(
                            color: Color(0xff001833),
                            fontWeight: FontWeight.w500,
                            fontFamily: "Poppins",
                            fontSize: 16),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 150,
              child: Container(
                padding: EdgeInsets.only(left: 4,right: 4),
                //color: Colors.green,
              child: GridView.count(
                  crossAxisCount: 1,
                  scrollDirection: Axis.horizontal,
                  children: List.generate(categories.length, (index) {
                    return Padding(
                      padding: EdgeInsets.all(6),
                      child: InkWell(
                        onTap: () {
                          openScreen(NavigationHelper.subCategoryDetailsroute, args: HashMap.from({"category": categories[index],"selectedSub":null}));
                        },
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: Container(
                            width: 50,
                            decoration: BoxDecoration(
                              //color: Colors.red,
                              color: colors[index % colors.length],
                            ),
                            child: Stack(
                              children: <Widget>[
                                Container(
                                    child: ImageView(
                                      height: double.infinity,
                                      width: double.infinity,
                                      url: categories[index].cat_image,
                                      placeHolder: "assets/shopping-bag.png",
                                    ),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: colors[index % colors.length],
                                  ),
                                  child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        gradient: LinearGradient(
                                            colors: [
                                              Color(0x01FFFFFF),
                                              Colors.black26,
                                              Colors.black
                                            ],
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter),
                                      ),
                                      alignment: Alignment.bottomCenter,
                                      child: Row(children: [
                                        Container(
                                          //color: Colors.blue,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(10)
                                            ),
                                            padding: EdgeInsets.all(4),
                                            child: Text(
                                              categories[index].name,
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.white,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.bold),
                                            ))
                                      ])),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  })),
            ),
            ),
            ListView(
                shrinkWrap: true,
                padding: EdgeInsets.only(top: 4),
                scrollDirection: Axis.vertical,
                physics: NeverScrollableScrollPhysics(),
                children: List.generate(select.sub.length, (index) {
                  return Container(
                    height: 160,
                    padding: EdgeInsets.all(10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Container(
                        color: Colors.white,
                        child: Stack(
                          children: <Widget>[
                            ImageView(
                              height: double.infinity,
                              width: double.infinity,
                              url: select.sub[index].cat_image,
                              placeHolder: "assets/shopping-bag.png",
                              fit: BoxFit.scaleDown,
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      colors: [
                                        Color(0x01FFFFFF),
                                        Colors.black26,
                                        Colors.black87
                                      ],
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomLeft),
                                ),
                                alignment: Alignment.bottomCenter,
                                child: Row(children: [
                                  Container(
                                      padding: EdgeInsets.all(15),
                                      child: Text(
                                        select.sub[index].name,
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.white,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.bold),
                                      ))
                                ])),
                          ],
                        ),
                      ),
                    ),
                  );
                })),
          ],
        ),
      ),
    ));
  }
}
