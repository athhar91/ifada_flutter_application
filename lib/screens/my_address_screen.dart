import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ifada/helpers/api_helper.dart';
import 'package:ifada/helpers/app_color.dart';
import 'package:ifada/helpers/app_data_helper.dart';
import 'package:ifada/model/response/BasicResponse.dart';
import 'package:ifada/model/response/my_address.dart';
import 'package:ifada/screens/state_screen.dart';
import 'package:ifada/widgets/top_bar.dart';

class MyAddressScreen extends StateScreen {

  MyAddresses myAddress = null;
  MyAddressScreen(String name) : super(name);

  @override
  ScreenState<StateScreen> getState() {
    return MyAddressState();
  }
}


List<String> name = [
  "Fahim",
  "Jassim",
  "Feroz",
];

List<String> address = [
  "Umm Al Hamam Al Gharbi, Riyadh 11564,Saudi Arabia",
  "Collector Road C, Lot 26ER, Al Safarat, Riyadh 11564, Saudi Arabia",
  "Umm Al Hamam Al Gharbi,Riyadh 12324, Saudi Arabia"
];

List<String> number = [
  "8745963210",
  "7458632109",
  "9685741230",
];

class MyAddressState extends ScreenState<MyAddressScreen> {

  @override
  void initState() {
    super.initState();
    // _getAddress();
  }

  _getAddress() async {
    ApiHelper<MyAddresses>((res) {
      setState(() {
        widget.myAddress = res;
      });
    }, (error) {}, () => new MyAddresses()).getMyAddress(AppDataHelper.user.id);
  }

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            TopBar(
              "My Address",
              onBack: () {
                widget.onBackPressed(forceBack: true);
              },
            ),
            Container(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    alignment: Alignment.center,
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(35),
                        color: AppColor.appBlue),
                    child: Text(
                      "+",
                      style: TextStyle(
                          fontSize: 22,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 5, right: 10),
                    child: Text(
                      "Add New",
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 8, left: 8, right: 8),
              child: ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: address.length,
                  itemBuilder: (_, index) {
                    return Card(
                      elevation: 3,
                      child: Container(
                        margin: EdgeInsets.only(top: 5, bottom: 5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 40,
                              width: 40,
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(left: 10),
                              decoration: BoxDecoration(
                                  color: Color(0xff3742FA),
                                  borderRadius: BorderRadius.circular(40)),
                              child: Text(
                                "S",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: [
                                  Container(
                                    alignment: Alignment.topLeft,
                                    margin: EdgeInsets.only(left: 13, top: 5),
                                    child: Text(
                                      name[index],
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                          fontFamily: "Poppins"),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                          height: 25,
                                          width: 25,
                                          margin: EdgeInsets.only(left: 10, top: 5),
                                          padding: EdgeInsets.all(6),
                                          child: SvgPicture.asset("assets/phone.svg")),
                                      Container(
                                        alignment: Alignment.topLeft,
                                        margin:
                                        EdgeInsets.only(top: 5),
                                        child: Text(
                                          number[index],
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w300,
                                              fontFamily: "Poppins"),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Container(
                                          height: 25,
                                          width: 25,
                                          margin: EdgeInsets.only(left: 10, top: 5),
                                          padding: EdgeInsets.all(4),
                                          child: SvgPicture.asset("assets/location_image.svg")),
                                      Expanded(
                                        child: Container(
                                          alignment: Alignment.topLeft,
                                          margin:
                                              EdgeInsets.only(top: 5),
                                          child: Text(
                                            address[index],
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w300,
                                                fontFamily: "Poppins", ),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Container(
                                height: 25,
                                width: 25,
                                margin: EdgeInsets.only(left: 10, top: 5),
                                padding: EdgeInsets.all(6),
                                decoration: BoxDecoration(
                                    color: Color(0xff000000),
                                    borderRadius: BorderRadius.circular(25)),
                                child: SvgPicture.asset(
                                    "assets/edit_address.svg")),
                            Container(
                                height: 25,
                                width: 25,
                                margin:
                                    EdgeInsets.only(left: 5, top: 5, right: 10),
                                padding: EdgeInsets.all(6),
                                decoration: BoxDecoration(
                                    color: Color(0xffFF4757),
                                    borderRadius: BorderRadius.circular(25)),
                                child: Image.asset("assets/ic_delete.png"))
                          ],
                        ),
                      ),
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
