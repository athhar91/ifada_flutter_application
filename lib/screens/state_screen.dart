import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:ifada/helpers/navigation_helper.dart';

abstract class StateScreen extends StatefulWidget{

  final String name;
  StateScreen(this.name);
  BuildContext _context;

  @override
  ScreenState<StateScreen> createState() {
    return getState();
  }

  ScreenState getState();

  void openScreen(String name,{HashMap args,bool forceNew = false}){
    NavigationHelper.openScreen(_context, name,args: args,forceNew: forceNew);
  }

  Future<bool> onBackPressed({forceBack = false}){
    NavigationHelper.screenWillPop(name);
    if(forceBack)
       Navigator.pop(_context);
    return  new Future(() => true);
  }
}

abstract class ScreenState<T extends StateScreen> extends State<T>{
  @override
  Widget build(BuildContext context) {
    widget._context = context;
    return SafeArea(child: WillPopScope(
        onWillPop: (){
          return widget.onBackPressed();
        },
        child:getWidget(context)));
  }

  Widget getWidget(BuildContext context);

  void update() {
    if (mounted) setState(() {});
  }
}