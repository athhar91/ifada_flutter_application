import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ifada/helpers/app_color.dart';
import 'package:ifada/screens/stateless_screen.dart';
import 'package:ifada/helpers/preference_helper.dart';
import 'package:ifada/helpers/navigation_helper.dart';

class SplashScreen extends StateLessScreen{
  SplashScreen(String name) : super(name);

  void checkUser(BuildContext context) async {
    var user = await PreferenceHelper.getUserData();
    if(user != null && user.id!=null&& user.id.isNotEmpty)
      NavigationHelper.launchNewScreen(context,NavigationHelper.homeScreen);
    else
      NavigationHelper.launchNewScreen(context,NavigationHelper.loginRoute);
  }

  @override
  Widget getWidget(BuildContext context) {
    checkUser(context);
    return Container(
      decoration: BoxDecoration(color: AppColor.appYellow),
      alignment: Alignment.center,
      child: Image.asset("assets/ic_ifada.png",width: 120,height: 120,),
    );
  }

}