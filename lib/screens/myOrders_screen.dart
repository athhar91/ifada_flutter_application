import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ifada/helpers/api_helper.dart';
import 'package:ifada/model/response/my_order_response.dart';
import 'package:ifada/screens/state_screen.dart';
import 'package:ifada/screens/stateless_screen.dart';

import '../helpers/navigation_helper.dart';
import '../helpers/navigation_helper.dart';
import '../widgets/image_view.dart';

class MyOrdersScreen extends StateScreen {
  MyOrdersScreen(String name) : super(name);

  @override
  MyOrdersScreenState getState() => new MyOrdersScreenState();
}

class MyOrdersScreenState extends ScreenState {
  MyOrdersScreen response = null;

  // List<String> getListElements() {
  //   var items = List<String>.generate(10, (counter) => "Item $counter ");
  //   return items;
  // }

  // ListView _buildListView() {
  //   return ListView.builder(
  //     itemCount: 10,
  //     itemBuilder: (_, index){
  //       return ListTile(
  //         title: Text("The List Item #$index"),
  //         leading: Icon(Icons.image),
  //       );
  //     },
  //   );
  // }

  @override
  void initState() {
    super.initState();
  }

  // getMyOrders() async {
  //   ApiHelper<MyOrderResponse>(
  //       (res) => {
  //         setState (() {
  //           response = res;
  //         })
  //       },
  //       (error) => {},
  //       () => new MyOrderResponse()).getHomePage();
  // }


  Widget getOrderWidget() {
    return Container(
      width: double.infinity,
      height: 90.0,
      margin: EdgeInsets.only(left: 12, right: 12),
      child: Card(
        child: Container(

          // padding: EdgeInsets.all(4.0),
          margin: EdgeInsets.only(top: 5.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InkWell(
                onTap: () {
                  widget.openScreen(NavigationHelper.rateProductroute);
                },
                child: Container(
                  margin: EdgeInsets.only(left: 25.0, top: 14),
                  child: ImageView(
                    placeHolder: "assets/google.png",
                    height: 35.0,
                    width: 35.0,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Center(
                  child: Container(

                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            // widget.openScreen(NavigationHelper.trackOrderroute);
                          },
                          child: Container(

                              child: Text(
                                "Order ID : 5678",
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16,
                                    color: Color(0xff3742FE)),
                              )),
                        ),
                        Container(
                        margin: EdgeInsets.only(top: 3),
                            child: Text(
                              "Dec 20 2020, 05:00 pm",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Poppins",
                                  color: Color(0xff8D8F92)),
                            )),
                        Container(

                            child: Text(
                              "Order Cancelled",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "Poppins",
                                  color: Color(0xffFF4757)),
                            ))
                      ],
                    ),
                  ),
                ),
              ),
              Container(
               margin:EdgeInsets.only(right: 10,bottom: 13) ,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(

                        child: Text(
                          "SAR 11.50",
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              fontFamily: "Poppins",
                              color: Color(0xff001833)),
                        )),
                    InkWell(
                      onTap: () {
                        widget.openScreen(NavigationHelper.orderDetailsroute);
                      },
                      child: Container(
                          height: 20,
                          width: 80,

                          padding: EdgeInsets.only(top: 3),
                          decoration: BoxDecoration(
                              color: Color(0xff3742FE),
                              borderRadius: BorderRadius.circular(3)),
                          child: Text(
                            "Pay",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                                fontFamily: "Poppins",
                                color: Colors.white),
                          )),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }


  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xffF4F4F4),
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 50,
              margin: EdgeInsets.only(top: 20),

              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.all(10),
                    child: Text(
                      "My Orders",
                      style: TextStyle(
                        fontFamily: "Poppins",
                        fontWeight: FontWeight.w500,
                        fontSize: 20,
                        color: Color(0xff001833),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      widget.openScreen(NavigationHelper.myCartroute);
                    },
                    child: Container(
                      height: 25,
                      width: 25,
                      margin: EdgeInsets.only(left: 0, bottom: 10, right: 10),
                      padding: EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        color: Color(0xff3742FA),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: SvgPicture.asset(
                          "assets/white_cart.svg",
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                width: double.infinity,
                child: ListView.builder(
                  itemCount: 10,
                  itemBuilder: (_, index) {
                    return getOrderWidget();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
