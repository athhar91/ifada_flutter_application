import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ifada/helpers/api_helper.dart';
import 'package:ifada/helpers/dialog_helper.dart';
import 'package:ifada/helpers/navigation_helper.dart';
import 'package:ifada/helpers/preference_helper.dart';
import 'package:ifada/model/User.dart';
import 'package:ifada/screens/stateless_screen.dart';
import 'package:ifada/widgets/cc_picker.dart';

import '../utils.dart';

class LoginScreen extends StateLessScreen {
  LoginScreen(String name) : super(name);
  String password = "";
  String num = "";
  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image:
                  new ExactAssetImage('assets/ic_login_background_image.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Container(
            color: Color(0xff80000000),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                      child: Image.asset(
                    "assets/ic_ifada.png",
                  )),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Text("LOGIN",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 35,
                          fontWeight: FontWeight.w500,
                          color: Colors.white,
                        )),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 25, top: 20),
                    child: TextField(
                      onChanged: (value) {
                        num = value;
                      },
                      maxLength: 10,
                      keyboardType: TextInputType.number,
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                        labelText: "Mobile Number",
                        labelStyle: TextStyle(color: Colors.white),
                        hintText: "",
                        // prefix: CCPicker(
                        //   color: Colors.white,
                        //   cc: "+971",
                        //   onChanged: (value) {},
                        // ),
                        floatingLabelBehavior: FloatingLabelBehavior.auto,
                        hintStyle: TextStyle(color: Colors.white),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: new BorderSide(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 25, top: 15),
                    child: TextField(
                      onChanged: (value) {
                        password = value;
                      },
                      obscureText: true,
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                          labelText: "Password",
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          labelStyle: TextStyle(color: Colors.white),
                          hintStyle: TextStyle(color: Colors.white),
                          hintText: "",
                          enabledBorder: UnderlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white),
                          )),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      openScreen(NavigationHelper.recoveryroute);
                    },
                    child: Container(
                        padding: EdgeInsets.all(7),
                        margin: EdgeInsets.only(left: 230, bottom: 15),
                        child: Text(
                          "Forgot Password?",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                          ),
                        )),
                  ),
                  Container(
                    height: 45,
                    width: 370,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(3.0)),
                    ),
                    child: RaisedButton(
                        color: Color(0xff3742FA),
                        child: Text(
                          "LOGIN",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 20,
                          ),
                        ),
                        onPressed: () {
                          if (isValidateAlldatas(context)) {
                            DialogHelper.showLoadingDialog(context, false);
                            //openScreen(NavigationHelper.categoryroute);
                            login(num, password,context);
                          }
                        }),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 60),
                    child: Text(
                      "Or Login With",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30),
                    child: Row(
                      children: <Widget>[
                        Container(
                          child: Image.asset("assets/facebook-app-symbol.png"),
                          height: 40,
                          width: 40,
                          margin: EdgeInsets.only(left: 120, right: 22),
                        ),
                        Container(
                          child: Image.asset("assets/google.png"),
                          height: 40,
                          width: 40,
                          margin: EdgeInsets.only(right: 22),
                        ),
                        Container(
                          child: Image.asset("assets/twitter.png"),
                          height: 40,
                          width: 40,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 100),
                          child: Text(
                            "Don't have an account?",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                        InkWell(
                          child: Container(
                            padding: EdgeInsets.only(top: 100),
                            child: Text(
                              " Sign Up",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color(0xffFFAF50),
                              ),
                            ),
                          ),
                          onTap: () {
                            openScreen(NavigationHelper.signupRote);
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  bool isValidateAlldatas(BuildContext context) {
    if (isNullOrEmpty(num) /*|| num.length < 10*/) {
      _showDialog("Alert", "Enter a valid phone number", context);
      return false;
    } else if (isNullOrWhiteSpace(password)) {
      _showDialog("Alert", "Enter a valid password", context);
      return false;
    }
    return true;
  }

  void login(String phone, String password, BuildContext context) async {
    ApiHelper<User>(
        (res) => {
          DialogHelper.closeLoadingDialog(),
          PreferenceHelper.saveUserData(res),
          NavigationHelper.launchNewScreen(context,NavigationHelper.homeScreen)
        },
        (error) => {
          DialogHelper.closeLoadingDialog(),
          _showDialog("Alert", error.isEmpty ? "Failed to Login" : error, context)
        },
            () => new User())
        .login(phone, password);
  }

  void _showDialog(String title, String content, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Text(content),
          actions: <Widget>[
            FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
