import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ifada/helpers/api_helper.dart';
import 'package:ifada/helpers/app_color.dart';
import 'package:ifada/helpers/app_data_helper.dart';
import 'package:ifada/helpers/navigation_helper.dart';
import 'package:ifada/model/response/cart.dart';
import 'package:ifada/widgets/top_bar.dart';

import '../widgets/image_view.dart';
import 'state_screen.dart';

class MyCart extends StateScreen {
  cart cartRes = null;

  MyCart(String name) : super(name);

  @override
  MyCartState getState() => new MyCartState();
}

class MyCartState extends ScreenState<MyCart> {
  var selectedIndex;

  // var check1 = false;
  // var check2 = false;
  // var check3 = false;
  var morning = false;
  var evening = false;
  var anytime = false;
  var none = false;
  var every2week = false;
  var every_month = false;
  var cod = false;
  var cad = false;
  var credit = false;
  var wallet = false;
  var counter = 0;
  var initialCounter = 0;
  var selectedItem;

  List<String> items = [
    "Mohammed Fahim ,Umm Al Hamam Al Gharbi, Riyadh 11564,Saudi Arabia, 8745963210.",
    "Mohammed Jassim, Collector Road C, Lot 26ER, Al Safarat, Riyadh 11564, Saudi Arabia, 7458632109",
    "Feroze, Umm Al Hamam Al Gharbi,Riyadh 12324, Saudi Arabia. 9685741230",
  ];

  List<String> items2 = [
    "1",
    "2",
    "3",
    "4",
  ];

  @override
  void initState() {
    super.initState();
    _getCart();
  }

  _getCart() async {
    ApiHelper<cart>((res) {
      setState(() {
        widget.cartRes = res;
      });
    }, (error) {}, () => new cart()).getCart(AppDataHelper.user.id);
  }

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
        body: Container(
      height: double.infinity,
      child: Column(
        children: [
          TopBar("My Cart", onBack: () {
            widget.onBackPressed(forceBack: true);
          }),
          Expanded(
            child: Container(
                color: Color(0xffF4F4F4),
                margin: EdgeInsets.only(top: 5),
                child: widget.cartRes == null
                    ? Align(
                        alignment: Alignment.center,
                        child: Center(
                            child: SpinKitCircle(
                          color: AppColor.appBlue,
                          size: 90,
                        )),
                      )
                    : SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: widget.cartRes.products.length,
                                itemBuilder: (_, index) {
                                  bool remove = false;
                                  final item = widget.cartRes.products[index];
                                  return Dismissible(
                                      background: InkWell(
                                        onTap: () {
                                          setState(() {
                                            remove = true;
                                          });
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              top: 5,
                                              bottom: 5,
                                              right: 15,
                                              left: 15),
                                          padding: EdgeInsets.only(right: 20),
                                          alignment: Alignment.centerRight,
                                          color: Color(0xffF18F94),
                                          child: Text(
                                            "Remove",
                                            style: TextStyle(
                                              fontFamily: "Poppins",
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                      key: Key(item.id),
                                      onDismissed: (direction) {
                                        if (remove == true)
                                          setState(() {
                                            items2.removeAt(index);
                                          });
                                      },
                                      child: Container(
                                          width: double.infinity,
                                          height: 110,
                                          margin: EdgeInsets.only(
                                              left: 15, right: 15, top: 3),
                                          child: Card(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8.0)),
                                              child: Container(
                                                  width: double.infinity,
                                                  height: double.infinity,
                                                  child: Row(children: <Widget>[
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 12, right: 7),
                                                      child: ImageView(
                                                        placeHolder:
                                                            "assets/creame_fruit.png",
                                                        url: widget.cartRes
                                                                .image_path +
                                                            item.image,
                                                        height: 70.0,
                                                        width: 60.0,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(8.0),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: <Widget>[
                                                            Container(
                                                              child: Text(
                                                                item.name,
                                                                style: TextStyle(
                                                                    color: Color(
                                                                        0xff3742FE),
                                                                    fontFamily:
                                                                        "Poppins",
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500,
                                                                    fontSize:
                                                                        16),
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              ),
                                                            ),
                                                            Container(
                                                              child: Text(
                                                                "1 Kg",
                                                                style: TextStyle(
                                                                    color: Color(
                                                                        0xff8D8F92),
                                                                    fontSize:
                                                                        12,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                    fontFamily:
                                                                        "Poppins"),
                                                              ),
                                                            ),
                                                            Container(
                                                              child: Text(
                                                                "SAR 90.00",
                                                                style: TextStyle(
                                                                    color: Color(
                                                                        0xff000000),
                                                                    fontSize:
                                                                        14,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500,
                                                                    fontFamily:
                                                                        "Poppins"),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          right: 15),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .end,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                        children: [
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            child: Row(
                                                              children: <
                                                                  Widget>[
                                                                InkWell(
                                                                  onTap: () {
                                                                    setState(
                                                                        () {
                                                                      selectedItem =
                                                                          index;
                                                                      if (counter !=
                                                                          0)
                                                                        counter =
                                                                            counter -
                                                                                1;
                                                                    });
                                                                  },
                                                                  child:
                                                                      Container(
                                                                    height: 20,
                                                                    width: 20,
                                                                    padding: EdgeInsets.only(
                                                                        top: 6,
                                                                        left: 4,
                                                                        right:
                                                                            4),
                                                                    decoration: BoxDecoration(
                                                                        shape: BoxShape
                                                                            .circle,
                                                                        color: Colors
                                                                            .white,
                                                                        border: Border.all(
                                                                            width:
                                                                                1,
                                                                            color:
                                                                                Color(0xff8D8F92))),
                                                                    child: Image
                                                                        .asset(
                                                                      "assets/ic_deleteItem.png",
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets
                                                                      .symmetric(
                                                                          horizontal:
                                                                              2),
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  width: 30,
                                                                  child: Text(
                                                                    selectedItem ==
                                                                            index
                                                                        ? "$counter"
                                                                        : "$initialCounter",
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          14,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500,
                                                                      fontFamily:
                                                                          "poppins",
                                                                      color: Color(
                                                                          0xff8D8F92),
                                                                    ),
                                                                  ),
                                                                ),
                                                                InkWell(
                                                                  onTap: () {
                                                                    setState(
                                                                        () {
                                                                      selectedItem =
                                                                          index;
                                                                      counter =
                                                                          counter +
                                                                              1;
                                                                    });
                                                                  },
                                                                  child:
                                                                      Container(
                                                                    height: 20,
                                                                    width: 20,
                                                                    padding:
                                                                        EdgeInsets
                                                                            .all(4),
                                                                    decoration: BoxDecoration(
                                                                        shape: BoxShape
                                                                            .circle,
                                                                        color: Colors
                                                                            .white,
                                                                        border: Border.all(
                                                                            width:
                                                                                1,
                                                                            color:
                                                                                Color(0xff8D8F92))),
                                                                    child: Image
                                                                        .asset(
                                                                            "assets/ic_addItem.png"),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    bottom: 10,
                                                                    top: 20),
                                                            height: 20,
                                                            width: 20,
                                                            padding:
                                                                EdgeInsets.all(
                                                                    4),
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            20),
                                                                color: Color(
                                                                    0xffFF4757)),
                                                            child: Image.asset(
                                                                "assets/ic_delete.png"),
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                  ])))));

                                  //   CartItemView(item, onRemove: (){
                                  //   items2.removeAt(index);
                                  // },);
                                },
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 15, top: 15),
                                child: Text(
                                  "Delivery details",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "Poppins"),
                                ),
                              ),
                              Card(
                                elevation: 3,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                ),
                                margin: EdgeInsets.all(15),
                                child: Container(
                                  width: double.infinity,
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.only(
                                            top: 10, left: 10, right: 10),
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: 20,
                                              child: SvgPicture.asset(
                                                  "assets/ic_deliveryAdd.svg"),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Text(
                                                "Delivery Address",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: "Poppins",
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 14),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      ListView(
                                          scrollDirection: Axis.vertical,
                                          shrinkWrap: true,
                                          physics:
                                              NeverScrollableScrollPhysics(),
                                          children: List.generate(items.length,
                                              (index) {
                                            return InkWell(
                                              onTap: () {
                                                setState(() {
                                                  selectedIndex = index;
                                                });
                                              },
                                              child: Container(
                                                margin: EdgeInsets.only(
                                                    left: 10, right: 10),
                                                decoration: BoxDecoration(
                                                    border: Border(
                                                        bottom: BorderSide(
                                                            width: 1,
                                                            color: Color(
                                                                0xffEEEEEE)))),
                                                child: Row(
                                                  children: <Widget>[
                                                    Container(
                                                        height: 10,
                                                        width: 10,
                                                        padding:
                                                            EdgeInsets.all(1),
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10),
                                                            border: Border.all(
                                                                width: 1,
                                                                color: Color(
                                                                    0xff8D8F92))),
                                                        child:
                                                            selectedIndex ==
                                                                    index
                                                                ? Container(
                                                                    height: 7,
                                                                    width: 7,
                                                                    decoration: BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                7),
                                                                        color: Color(
                                                                            0xff3742FE)),
                                                                  )
                                                                : null),
                                                    Expanded(
                                                      child: Container(
                                                        margin: EdgeInsets.only(
                                                            left: 5, right: 5),
                                                        padding:
                                                            EdgeInsets.all(6),
                                                        child: Text(
                                                          items[index],
                                                          textAlign:
                                                              TextAlign.start,
                                                          style: TextStyle(
                                                            color: Color(
                                                                0xff8D8F92),
                                                            fontFamily:
                                                                "Poppins",
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            fontSize: 12,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    selectedIndex == index
                                                        ? Container(
                                                            height: 25,
                                                            width: 25,
                                                            margin:
                                                                EdgeInsets.only(
                                                                    right: 10),
                                                            padding:
                                                                EdgeInsets.all(
                                                                    6),
                                                            decoration: BoxDecoration(
                                                                color: Color(
                                                                    0xff000000),
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            25)),
                                                            child: SvgPicture.asset(
                                                                "assets/edit_address.svg"))
                                                        : Container(
                                                            height: 25,
                                                            width: 25,
                                                          )
                                                  ],
                                                ),
                                              ),
                                            );
                                          })),
                                      InkWell(
                                        onTap: (){
                                          widget.openScreen(NavigationHelper.addAddress);
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                            top: 5,
                                            bottom: 10,
                                          ),
                                          padding: EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              color: Color(0xff3742FE)),
                                          child: Text(
                                            "Add New Address",
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: "Poppins",
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Card(
                                elevation: 2,
                                margin: EdgeInsets.only(
                                    top: 10, bottom: 10, left: 15, right: 15),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white),
                                  padding: EdgeInsets.all(10),
                                  alignment: Alignment.topLeft,
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            height: 15,
                                            margin: EdgeInsets.all(5),
                                            child: Image.asset(
                                                "assets/ic_watch.png"),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(
                                                left: 3, top: 5, bottom: 7),
                                            child: Text(
                                              "Timings",
                                              style: TextStyle(
                                                  fontFamily: "Poppins",
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 14,
                                                  color: Colors.black),
                                            ),
                                          )
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          InkWell(
                                            onTap: () {
                                              setState(() {
                                                morning = true;
                                                evening = false;
                                                anytime = false;
                                              });
                                            },
                                            child: Container(
                                              child: Row(
                                                children: <Widget>[
                                                  Container(
                                                      height: 10,
                                                      width: 10,
                                                      padding:
                                                          EdgeInsets.all(1),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          border: Border.all(
                                                              width: 1,
                                                              color: Color(
                                                                  0xff8D8F92))),
                                                      child: morning == true
                                                          ? Container(
                                                              height: 7,
                                                              width: 7,
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              7),
                                                                  color: Color(
                                                                      0xff3742FE)),
                                                            )
                                                          : null),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: Text(
                                                      "Morning",
                                                      style: TextStyle(
                                                          fontFamily: "Poppins",
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 14,
                                                          color: Color(
                                                              0xff8D8F92)),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          InkWell(
                                            onTap: () {
                                              setState(() {
                                                morning = false;
                                                evening = true;
                                                anytime = false;
                                              });
                                            },
                                            child: Container(
                                              child: Row(
                                                children: <Widget>[
                                                  Container(
                                                      height: 10,
                                                      width: 10,
                                                      padding:
                                                          EdgeInsets.all(1),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          border: Border.all(
                                                              width: 1,
                                                              color: Color(
                                                                  0xff8D8F92))),
                                                      child: evening == true
                                                          ? Container(
                                                              height: 7,
                                                              width: 7,
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              7),
                                                                  color: Color(
                                                                      0xff3742FE)),
                                                            )
                                                          : null),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: Text(
                                                      "Evening",
                                                      style: TextStyle(
                                                          fontFamily: "Poppins",
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 14,
                                                          color: Color(
                                                              0xff8D8F92)),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          InkWell(
                                            onTap: () {
                                              setState(() {
                                                morning = false;
                                                evening = false;
                                                anytime = true;
                                              });
                                            },
                                            child: Container(
                                              child: Row(
                                                children: <Widget>[
                                                  Container(
                                                      height: 10,
                                                      width: 10,
                                                      padding:
                                                          EdgeInsets.all(1),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          border: Border.all(
                                                              width: 1,
                                                              color: Color(
                                                                  0xff8D8F92))),
                                                      child: anytime == true
                                                          ? Container(
                                                              height: 7,
                                                              width: 7,
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              7),
                                                                  color: Color(
                                                                      0xff3742FE)),
                                                            )
                                                          : null),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: Text(
                                                      "Any Time",
                                                      style: TextStyle(
                                                          fontFamily: "Poppins",
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 14,
                                                          color: Color(
                                                              0xff8D8F92)),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Card(
                                elevation: 2,
                                margin: EdgeInsets.only(
                                    top: 10, bottom: 10, left: 15, right: 15),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white),
                                  padding: EdgeInsets.all(10),
                                  alignment: Alignment.topLeft,
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            height: 15,
                                            margin: EdgeInsets.all(5),
                                            child: Image.asset(
                                                "assets/ic_watch.png"),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(
                                                left: 3, top: 5, bottom: 7),
                                            child: Text(
                                              "Schedule Order",
                                              style: TextStyle(
                                                  fontFamily: "Poppins",
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 14,
                                                  color: Colors.black),
                                            ),
                                          )
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          InkWell(
                                            onTap: () {
                                              setState(() {
                                                none = true;
                                                every2week = false;
                                                every_month = false;
                                              });
                                            },
                                            child: Container(
                                              child: Row(
                                                children: <Widget>[
                                                  Container(
                                                      height: 10,
                                                      width: 10,
                                                      padding:
                                                          EdgeInsets.all(1),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          border: Border.all(
                                                              width: 1,
                                                              color: Color(
                                                                  0xff8D8F92))),
                                                      child: none == true
                                                          ? Container(
                                                              height: 7,
                                                              width: 7,
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              7),
                                                                  color: Color(
                                                                      0xff3742FE)),
                                                            )
                                                          : null),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: Text(
                                                      "None",
                                                      style: TextStyle(
                                                          fontFamily: "Poppins",
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 14,
                                                          color: Color(
                                                              0xff8D8F92)),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          InkWell(
                                            onTap: () {
                                              setState(() {
                                                none = false;
                                                every2week = true;
                                                every_month = false;
                                              });
                                            },
                                            child: Container(
                                              child: Row(
                                                children: <Widget>[
                                                  Container(
                                                      height: 10,
                                                      width: 10,
                                                      padding:
                                                          EdgeInsets.all(1),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          border: Border.all(
                                                              width: 1,
                                                              color: Color(
                                                                  0xff8D8F92))),
                                                      child: every2week == true
                                                          ? Container(
                                                              height: 7,
                                                              width: 7,
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              7),
                                                                  color: Color(
                                                                      0xff3742FE)),
                                                            )
                                                          : null),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: Text(
                                                      "Every two week",
                                                      style: TextStyle(
                                                          fontFamily: "Poppins",
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 14,
                                                          color: Color(
                                                              0xff8D8F92)),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          InkWell(
                                            onTap: () {
                                              setState(() {
                                                none = false;
                                                every2week = false;
                                                every_month = true;
                                              });
                                            },
                                            child: Container(
                                              child: Row(
                                                children: <Widget>[
                                                  Container(
                                                      height: 10,
                                                      width: 10,
                                                      padding:
                                                          EdgeInsets.all(1),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          border: Border.all(
                                                              width: 1,
                                                              color: Color(
                                                                  0xff8D8F92))),
                                                      child: every_month == true
                                                          ? Container(
                                                              height: 7,
                                                              width: 7,
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              7),
                                                                  color: Color(
                                                                      0xff3742FE)),
                                                            )
                                                          : null),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: Text(
                                                      "Every Month",
                                                      style: TextStyle(
                                                          fontFamily: "Poppins",
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 14,
                                                          color: Color(
                                                              0xff8D8F92)),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 15, top: 5),
                                child: Text(
                                  "Coupons",
                                  style: TextStyle(
                                      color: Color(0xff000000),
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "Poppins"),
                                ),
                              ),
                              Card(
                                elevation: 2,
                                margin: EdgeInsets.only(
                                    left: 15, right: 15, top: 5),
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 5, right: 5, top: 8, bottom: 8),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        margin: EdgeInsets.all(5),
                                        child: SvgPicture.asset(
                                          "assets/manageTickets_image.svg",
                                          height: 30,
                                          width: 20,
                                        ),
                                      ),
                                      Container(
                                          width: 200,
                                          height: 30,
                                          margin: EdgeInsets.only(
                                              left: 5, right: 5),
                                          child: TextField(
                                            decoration:
                                                InputDecoration.collapsed(
                                              hintText: "Enter coupon",
                                              floatingLabelBehavior:
                                                  FloatingLabelBehavior.never,
                                              border: UnderlineInputBorder(
                                                borderSide: new BorderSide(
                                                    color: Color(0xffD6D6D6),
                                                    width: 1),
                                              ),
                                            ),
                                          )),
                                      Container(
                                        margin: EdgeInsets.only(right: 5),
                                        child: Text(
                                          "Apply Coupon",
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "poppins",
                                              color: Color(0xff3742FE)),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 15, top: 10),
                                child: Text(
                                  "Payment",
                                  style: TextStyle(
                                      color: Color(0xff000000),
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "Poppins"),
                                ),
                              ),
                              Card(
                                /*shadowColor: AppColor.shadowColor,*/
                                elevation: 2,
                                margin: EdgeInsets.only(
                                    left: 15, right: 15, top: 5),
                                child: Container(
                                  width: double.infinity,
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white),
                                  child: Column(
                                    children: <Widget>[
                                      InkWell(
                                        onTap: () {
                                          setState(() {
                                            cod = true;
                                            cad = false;
                                            credit = false;
                                            wallet = false;
                                          });
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 10, right: 10),
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  bottom: BorderSide(
                                                      width: 1,
                                                      color:
                                                          Color(0xffEEEEEE)))),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                  height: 13,
                                                  width: 13,
                                                  padding: EdgeInsets.all(2),
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      border: Border.all(
                                                          width: 1,
                                                          color: Color(
                                                              0xff8D8F92))),
                                                  child: cod == true
                                                      ? Container(
                                                          height: 7,
                                                          width: 7,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          7),
                                                              color: Color(
                                                                  0xff3742FE)),
                                                        )
                                                      : null),
                                              Container(
                                                margin: EdgeInsets.only(
                                                    left: 5, right: 5, top: 5),
                                                padding: EdgeInsets.only(
                                                    top: 10,
                                                    bottom: 15,
                                                    left: 15),
                                                child: Text(
                                                  "Cash on Delivery",
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                    color: Color(0xff8D8F92),
                                                    fontFamily: "Poppins",
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 16,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          setState(() {
                                            cod = false;
                                            cad = true;
                                            credit = false;
                                            wallet = false;
                                          });
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 10, right: 10),
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  bottom: BorderSide(
                                                      width: 1,
                                                      color:
                                                          Color(0xffEEEEEE)))),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                  height: 13,
                                                  width: 13,
                                                  padding: EdgeInsets.all(2),
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      border: Border.all(
                                                          width: 1,
                                                          color: Color(
                                                              0xff8D8F92))),
                                                  child: cad == true
                                                      ? Container(
                                                          height: 7,
                                                          width: 7,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          7),
                                                              color: Color(
                                                                  0xff3742FE)),
                                                        )
                                                      : null),
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      left: 5,
                                                      right: 5,
                                                      top: 5),
                                                  padding: EdgeInsets.only(
                                                      top: 10,
                                                      bottom: 15,
                                                      left: 15),
                                                  child: Text(
                                                    "Credit / ATM / Debit Card",
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                      color: Color(0xff8D8F92),
                                                      fontFamily: "Poppins",
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.all(5),
                                                child: SvgPicture.asset(
                                                  "assets/ic_credit_card.svg",
                                                  height: 30,
                                                  width: 50,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          setState(() {
                                            cod = false;
                                            cad = false;
                                            credit = true;
                                            wallet = false;
                                          });
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 10, right: 10),
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  bottom: BorderSide(
                                                      width: 1,
                                                      color:
                                                          Color(0xffEEEEEE)))),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                  height: 13,
                                                  width: 13,
                                                  padding: EdgeInsets.all(2),
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      border: Border.all(
                                                          width: 1,
                                                          color: Color(
                                                              0xff8D8F92))),
                                                  child: credit == true
                                                      ? Container(
                                                          height: 7,
                                                          width: 7,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          7),
                                                              color: Color(
                                                                  0xff3742FE)),
                                                        )
                                                      : null),
                                              Container(
                                                margin: EdgeInsets.only(
                                                    left: 5, right: 5, top: 5),
                                                padding: EdgeInsets.only(
                                                    top: 10,
                                                    bottom: 15,
                                                    left: 15),
                                                child: Text(
                                                  "Credit",
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                    color: Color(0xff8D8F92),
                                                    fontFamily: "Poppins",
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 16,
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(top: 5),
                                                child: Text(
                                                  "(Amount - 3,471.25)",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontFamily: "Poppins",
                                                      fontSize: 12,
                                                      color: Color(0xff000000)),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          setState(() {
                                            cod = false;
                                            cad = false;
                                            credit = false;
                                            wallet = true;
                                          });
                                        },
                                        child: Container(
                                          margin: EdgeInsets.only(
                                              left: 10, right: 10),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                  height: 13,
                                                  width: 13,
                                                  padding: EdgeInsets.all(2),
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      border: Border.all(
                                                          width: 1,
                                                          color: Color(
                                                              0xff8D8F92))),
                                                  child: wallet == true
                                                      ? Container(
                                                          height: 7,
                                                          width: 7,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          7),
                                                              color: Color(
                                                                  0xff3742FE)),
                                                        )
                                                      : null),
                                              Container(
                                                margin: EdgeInsets.only(
                                                    left: 5, right: 5, top: 5),
                                                padding: EdgeInsets.only(
                                                    top: 10,
                                                    bottom: 15,
                                                    left: 15),
                                                child: Text(
                                                  "Wallet",
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                    color: Color(0xff8D8F92),
                                                    fontFamily: "Poppins",
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 16,
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(top: 5),
                                                child: Text(
                                                  "(Amount - 748.60)",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontFamily: "Poppins",
                                                      fontSize: 12,
                                                      color: Color(0xff000000)),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 15, top: 10),
                                child: Text(
                                  "Payment Details",
                                  style: TextStyle(
                                      color: Color(0xff000000),
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: "Poppins"),
                                ),
                              ),
                              Card(
                                elevation: 2,
                                margin: EdgeInsets.only(
                                    left: 15, right: 15, top: 5),
                                child: Container(
                                  margin: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                child: Text(
                                                  "SUB TOTAL",
                                                  style: TextStyle(
                                                      fontFamily: "Poppins",
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 12,
                                                      color: Color(0xff8D8F92)),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              child: Text(
                                                "SAR 250.000",
                                                style: TextStyle(
                                                    fontFamily: "Poppins",
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 14,
                                                    color: Color(0xff001833)),
                                              ),
                                            )
                                          ],
                                        ),
                                        margin: EdgeInsets.only(
                                            left: 15, right: 15, top: 15),
                                        padding: EdgeInsets.only(bottom: 5),
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    width: 2,
                                                    color: Color(0xffEEEEEE)))),
                                      ),
                                      Container(
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                child: Text(
                                                  "VAT",
                                                  style: TextStyle(
                                                      fontFamily: "Poppins",
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 12,
                                                      color: Color(0xff8D8F92)),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              child: Text(
                                                "SAR 10.00",
                                                style: TextStyle(
                                                    fontFamily: "Poppins",
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 14,
                                                    color: Color(0xff001833)),
                                              ),
                                            )
                                          ],
                                        ),
                                        margin: EdgeInsets.only(
                                            left: 15, right: 15, top: 15),
                                        padding: EdgeInsets.only(bottom: 5),
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    width: 2,
                                                    color: Color(0xffEEEEEE)))),
                                      ),
                                      Container(
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                child: Text(
                                                  "Total",
                                                  style: TextStyle(
                                                      fontFamily: "Poppins",
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 12,
                                                      color: Color(0xff001833)),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              child: Text(
                                                "SAR 260",
                                                style: TextStyle(
                                                    fontFamily: "Poppins",
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 14,
                                                    color: Color(0xff3742FE)),
                                              ),
                                            )
                                          ],
                                        ),
                                        margin: EdgeInsets.only(
                                            left: 15, right: 15, top: 15),
                                        padding: EdgeInsets.only(bottom: 12),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                    top: 40, left: 15, right: 15, bottom: 40),
                                padding: EdgeInsets.only(top: 10),
                                height: 45,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(3.0),
                                  ),
                                  color: Color(0xff3742FA),
                                ),
                                child: Text(
                                  "CHECKOUT",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 20,
                                    fontFamily: 'Poppins',
                                  ),
                                ),
                              ),
                            ]))),
          ),
        ],
      ),
    ));
  }
// _removeItem () => {}
}
