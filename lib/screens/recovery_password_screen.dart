import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:ifada/helpers/api_helper.dart';
import 'package:ifada/helpers/dialog_helper.dart';
import 'package:ifada/helpers/navigation_helper.dart';
import 'package:ifada/helpers/preference_helper.dart';
import 'package:ifada/model/User.dart';
import 'package:ifada/model/response/BasicResponse.dart';
import 'package:ifada/screens/state_screen.dart';
import 'package:ifada/widgets/cc_picker.dart';

class RecoveryPassword extends StateScreen {
  RecoveryPassword(String name) : super(name);

  @override
  RecoveryPasswordState getState() => new RecoveryPasswordState();
}

class RecoveryPasswordState extends ScreenState {

  String num;

  @override
  Widget getWidget(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Color(0xffE5E5E5),
        child: Container(
          child: SingleChildScrollView(
            child: Center(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            widget.openScreen(NavigationHelper.loginRoute);
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                                top: 60, left: 10, right: 10, bottom: 40),
                            width: 40,
                            height: 40,
                            child: Image.asset("assets/Group 1482.png"),
                          ),
                        ),
                        Container(
                          margin:
                              EdgeInsets.only(right: 80, top: 60, bottom: 40),
                          child: Text(
                            "Password Recovery",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontFamily: 'Poppins',
                                color: Colors.black,
                                fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 100, bottom: 20),
                    child: Text(
                      "Forgot Password",
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Poppins',
                          fontSize: 21,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 40),
                    child: Text(
                      "Please enter your registered mobile number. You will receive an OTP to Reset Your Password.",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff8D8F92),
                          fontFamily: 'Poppins',
                          fontSize: 16),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3)),
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.all(10),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(
                            left: 15,
                            right: 15,
                            top: 15,
                          ),
                          child: TextField(
                            onChanged: (String value) {
                              num = value;
                              return num;
                            },
                            maxLines: 1,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                labelText: "Mobile Number",
                                contentPadding: EdgeInsets.only(top: 12),
                                labelStyle: TextStyle(
                                  color: Color(0xff8D8F92),
                                ),
                                hintText: "",
                                floatingLabelBehavior:
                                    FloatingLabelBehavior.auto,
                                hintStyle: TextStyle(
                                  color: Color(0xff8D8F92),
                                  fontSize: 14,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w500,
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                    color: Color(0xffC4C4C4),
                                  ),
                                )),
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            //DialogHelper.showLoadingDialog(context, false);
                            //forgetPasswoed();
                            widget.openScreen(NavigationHelper.otpRoute, args: HashMap.from({'phone': num}));
                          },
                          child: Container(
                              padding: EdgeInsets.all(10),
                              margin: EdgeInsets.only(left: 16, right: 14, top: 35, bottom: 20),
                              height: 45,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3.0)),
                                color: Color(0xff3742FA),
                              ),
                              child: Text(
                                "CONTINUE",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 20,
                                  fontFamily: 'Poppins',
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void forgetPasswoed(){
    ApiHelper<BasicResponse>(
            (res) => {
          DialogHelper.closeLoadingDialog(),
            widget.openScreen(NavigationHelper.otpRoute, args: HashMap.from({'phone': num}))
        },
            (error) => {
          DialogHelper.closeLoadingDialog(),
          DialogHelper.showAlertDialog("Alert", error.isEmpty ? "Failed to Login" : error, context)
        },() => new BasicResponse())
        .forgetPassword(num);
  }
}
