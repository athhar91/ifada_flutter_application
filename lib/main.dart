import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ifada/helpers/navigation_helper.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'IFADA',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'poppins'
      ),
      initialRoute: NavigationHelper.initialRoute,
      onGenerateRoute: NavigationHelper.generateRoute,
    );
  }
}