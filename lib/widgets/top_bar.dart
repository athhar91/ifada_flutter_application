import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ifada/helpers/navigation_helper.dart';

class TopBar extends StatelessWidget {
  String title;

  Function onBack;

  TopBar(this.title, {this.onBack});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: Row(
        children: [
          onBack != null ?
          InkWell(
            onTap: (){
             onBack();
            },
            child: Container(
                height: 35,
                width: 35,
                margin: EdgeInsets.only(left: 16),
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Color(0xff3742FE)),
                child: SvgPicture.asset(
                  "assets/back_arrow.svg",
                )),
          ): Container(

          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 16),
              child: Text(
                title,
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    fontFamily: "poppins_regular",
                color: Colors.black),
              ),
            ),
          )
        ],
      ),
    );
  }
}
