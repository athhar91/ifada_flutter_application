import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ifada/model/category.dart';

class CategoryItemWidget extends StatelessWidget {
  const CategoryItemWidget({Key key, this.categories}) : super(key: key);
  final Category categories;

  @override
  Widget build(BuildContext context) {
    return Card(
        color: Colors.deepPurple,
        child: Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    child: Image.asset(
                      "assets/twitter.png",
                  color: Colors.black,
                  height: 50,
                  width: 50,
                )),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                    child: Text(
                  "",
                  style: TextStyle(color: Colors.white),
                ))
              ]),
        ));
    //  Expanded(child: Icon(categories.categoryImage, size:50.0, color: textStyle.color)),
    //
  }
}
