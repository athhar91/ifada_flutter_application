import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ifada/model/cart_item.dart';

import 'image_view.dart';

class CartItemView extends StatefulWidget {
  CartItem item;

  Function onRemove;
  CartItemView(this.item, {this.onRemove});

  @override
  State<StatefulWidget> createState() {
    return CartItemState();
  }
}

class CartItemState extends State<CartItemView> {
  var remove = true;
  var counter = 0;

  @override
  Widget build(BuildContext context) {
    var item = widget.item;
    return Dismissible(
      background: InkWell(
        onTap: () {
          setState(() {
            remove = true;
          });
        },
        child: Container(
          margin: EdgeInsets.only(top: 5, bottom: 5, right: 15, left: 15),
          padding: EdgeInsets.only(right: 20),
          alignment: Alignment.centerRight,
          color: Color(0xffF18F94),
          child: Text(
            "Remove",
            style: TextStyle(
              fontFamily: "Poppins",
              fontWeight: FontWeight.w500,
              fontSize: 14,
              color: Colors.white,
            ),
          ),
        ),
      ),
      key: Key(widget.item.id),
      onDismissed: (direction) {
        // if (remove == true)
          setState(() {
            widget.onRemove;
          });
      },
      child: Container(
        width: double.infinity,
        height: 110,
        margin: EdgeInsets.only(left: 15, right: 15, top: 3),
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          child: Container(
              width: double.infinity,
              height: double.infinity,
              child: Row(children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 12, right: 7),
                  child: ImageView(
                    placeHolder: "assets/creame_fruit.png",
                    url: item.image,
                    height: 70.0,
                    width: 60.0,
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Text(
                            item.name,
                            style: TextStyle(
                                color: Color(0xff3742FE),
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w500,
                                fontSize: 16),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Container(
                          child: Text(
                            "1 Kg",
                            style: TextStyle(
                                color: Color(0xff8D8F92),
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                fontFamily: "Poppins"),
                          ),
                        ),
                        Container(
                          child: Text(
                            "SAR 90.00",
                            style: TextStyle(
                                color: Color(0xff000000),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                fontFamily: "Poppins"),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Row(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                setState(() {
                                  if (counter != 0) counter = counter - 1;
                                });
                              },
                              child: Container(
                                height: 20,
                                width: 20,
                                padding:
                                    EdgeInsets.only(top: 6, left: 4, right: 4),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                    border: Border.all(
                                        width: 1, color: Color(0xff8D8F92))),
                                child: Image.asset(
                                  "assets/ic_deleteItem.png",
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 2),
                              alignment: Alignment.center,
                              width: 30,
                              child: Text(
                                counter.toString(),
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: "poppins",
                                  color: Color(0xff8D8F92),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  counter = counter + 1;
                                });
                              },
                              child: Container(
                                height: 20,
                                width: 20,
                                padding: EdgeInsets.all(4),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                    border: Border.all(
                                        width: 1, color: Color(0xff8D8F92))),
                                child: Image.asset("assets/ic_addItem.png"),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 10, top: 20),
                        height: 20,
                        width: 20,
                        padding: EdgeInsets.all(4),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Color(0xffFF4757)),
                        child: Image.asset("assets/ic_delete.png"),
                      )
                    ],
                  ),
                )
              ])),
        ),
      ),
    );
  }
}
