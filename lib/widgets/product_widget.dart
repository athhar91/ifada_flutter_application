import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart';
import 'package:ifada/helpers/api_helper.dart';
import 'package:ifada/helpers/app_color.dart';
import 'package:ifada/helpers/app_data_helper.dart';
import 'package:ifada/helpers/navigation_helper.dart';
import 'package:ifada/model/menu_item.dart';
import 'package:ifada/model/product.dart';
import 'package:ifada/model/response/BasicResponse.dart';
import 'package:ifada/widgets/image_view.dart';

class ProductWidget extends StatefulWidget{

  Product menuItem;
  String currency;
  int cartCount = 0;

  ProductWidget(this.menuItem,this.currency);

  @override
  State<StatefulWidget> createState() {
    return ProductWidgetState();
  }

}

class ProductWidgetState extends State<ProductWidget>{
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        InkWell(
          onTap: (){
            NavigationHelper.openScreen(context, NavigationHelper.productdetailsroute, args: HashMap.from({"product":widget.menuItem}));
          },
          child: Container(
            // color: Colors.blue,
            margin: EdgeInsets.all(4),
            child: Card(
              elevation: 2,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 5, left: 5),
                    padding: EdgeInsets.only(left: 6, top: 2, bottom: 2, right: 6),
                    decoration: BoxDecoration(
                      borderRadius:
                      BorderRadius.circular(20),
                      color: Color(0x73C4C4C4),
                    ),
                    child: Wrap(
                      children : [Text(
                        widget.menuItem.product_varient[0].unit_id + " ${widget.menuItem.product_varient[0].unit_value}",
                        // item.menu_varient_data.unit_value,
                        // item.menu_varient_data.quantity,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 10,
                            fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      )],
                    ),
                  ),
                  // SizedBox(
                  //   height: 98,
                  //   width: double.infinity,
                  //   child: ImageView(
                  //     url: widget.menuItem.product_image,
                  //   ),
                  // ),

                  Container(
                    margin: EdgeInsets.only(
                        top: 7, bottom: 3,left: 20,right: 20),
                    width: double.infinity,
                    height: 120,
                    child: ImageView(
                      url: widget.menuItem.image,
                      fit: BoxFit.scaleDown,
                    ),
                  ),
                  Container(
                    //color: Colors.green,
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(left: 6,right: 6,top: 6),
                    child: Text(
                      widget.menuItem.menu_name,
                      textAlign: TextAlign.center,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: AppColor.darkText,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Poppins",
                        fontSize: 12,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 8,right: 8,top: 4,bottom: 4),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            widget.currency+" "+widget.menuItem.product_varient[0].price,
                            style: TextStyle(
                              color: AppColor.darkText,
                              fontWeight: FontWeight.w600,
                              fontFamily: "Poppins",
                              fontSize: 14,
                            ),
                          ),
                        ),
                        widget.menuItem.product_varient[0].is_in_cart?
                        Row(
                          children: [
                            InkWell(
                              onTap: () {
                                setState(() {
                                  widget.cartCount = widget.cartCount-1;
                                  if(widget.cartCount == 0)
                                    widget.menuItem.product_varient[0].is_in_cart = false;
                                });
                              },
                              child: Container(
                                height: 18,
                                width: 18,
                                padding: EdgeInsets.only(top: 5, left: 4, right: 4),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                    border: Border.all(
                                        width: 1,
                                        color: Color(0xff8D8F92)
                                    )
                                ),
                                child: Image.asset(
                                  "assets/ic_deleteItem.png",
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 8, right: 8),
                              alignment: Alignment.center,
                              child: Wrap(
                                children: [Text(
                                  "${widget.cartCount}",
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: "poppins",
                                    color: Color(0xff8D8F92),
                                  ),
                                )],
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  widget.cartCount = widget.cartCount+1;
                                });
                              },
                              child: Container(
                                height: 18,
                                width: 18,
                                padding: EdgeInsets.all(3),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                    border: Border.all(
                                        width: 1,
                                        color: Color(0xff8D8F92)
                                    )
                                ),
                                child: Image.asset(
                                    "assets/ic_addItem.png"),
                              ),
                            ),
                          ],
                        ):
                        InkWell(
                          onTap: ((){
                            // ApiHelper<BasicResponse>((res){
                            //   setState(() {
                            //     if(res.responseCode == "200"){
                            //       setState(() {
                            //         widget.menuItem.product_varient[0].is_in_cart = true;
                            //         widget.cartCount = 1;
                            //       });
                            //     }
                            //   });
                            // },(error){},() => new BasicResponse()).addToCart(widget.menuItem, AppDataHelper.user.id);
                          }),
                          child: Container(
                            child: SvgPicture.asset(
                              "assets/blue_cart.svg",
                              color: AppColor.appBlue,
                            ),
                            height: 15,
                            width: 20,),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: InkWell(
            onTap: ((){
              if(widget.menuItem.product_varient[0].is_in_wishlist){
                ApiHelper<BasicResponse>((res){
                  if(res.responseCode == "200"){
                    setState(() {
                      widget.menuItem.product_varient[0].is_in_wishlist = true;
                    });
                  }
                },(error){},() => new BasicResponse()).addToWishList(widget.menuItem.product_varient[0], AppDataHelper.user.id);
              } else {
                ApiHelper<BasicResponse>((res){
                  if(res.responseCode == "200"){
                    setState(() {
                      widget.menuItem.product_varient[0].is_in_wishlist = true;
                    });
                  }
                },(error){

                },() => new BasicResponse()).addToWishList(widget.menuItem.product_varient[0], AppDataHelper.user.id);
              }
            }),
            child: Container(
              margin: EdgeInsets.only(top: 3,right: 3),
              padding: EdgeInsets.all(6),
              height: 28,
              width: 28,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                  BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 1.0,
                    ),
                  ]),
              child: SvgPicture.asset(
                  widget.menuItem.product_varient[0].is_in_wishlist?"assets/likedButton_image.svg":"assets/shoppingList_image.svg"),
            ),
          )
        ),
      ],
    );
  }

}