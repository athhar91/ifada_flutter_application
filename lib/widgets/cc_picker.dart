import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ifada/helpers/dialog_helper.dart';

class CCPicker extends StatefulWidget{
  String cc = "";
  Color color = Colors.black;
  ValueChanged<String> onChanged;

  CCPicker({this.color, this.cc,this.onChanged}){}

  @override
  State<StatefulWidget> createState() {
    return CCPickerState();
  }
}

class CCPickerState extends State<CCPicker> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        direction: Axis.horizontal,
        children: [
          Text(widget.cc, style: TextStyle(color: widget.color),),
          Container(
            padding: EdgeInsets.only(left: 10,right: 20),
              child : Image.asset("assets/expand_up_icon.png",height: 10,width: 10,color: widget.color)
          )
        ],
      ),
      onTap: (){
        DialogHelper.showCountryCodePicker(context, widget.cc, (value) {
          setState(() {
            widget.cc = value;
            widget.onChanged(value);
          });
        });
      },
    );
  }
}