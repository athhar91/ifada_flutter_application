import 'package:flutter/material.dart';
import 'package:ifada/helpers/app_color.dart';

class BannerPaginateWidget extends StatefulWidget {
  int pageValue = 0;
  List<Widget> children;
  PageController controller = null;
  Duration duration;

  BannerPaginateWidget(
      {this.children, this.duration = const Duration(seconds: 2)}) {
    controller = new PageController(initialPage: pageValue, keepPage: true);
  }

  @override
  State<StatefulWidget> createState() {
    return BannerPaginateState();
  }
}

class BannerPaginateState extends State<BannerPaginateWidget> {
  @override
  void initState() {
    //run();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    run();
    return Stack(
      children: [
        PageView(
          controller: widget.controller,
          children: List.generate(
              widget.children.length, (index) => widget.children[index]),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            margin: EdgeInsets.only(bottom: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(widget.children.length, (index) {
                return Container(
                  margin: EdgeInsets.all(4),
                  width: 12,
                  height: 12,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: index == widget.pageValue
                        ? AppColor.appYellow
                        : Colors.white,
                  ),
                );
              }),
            ),
          ),
        )
      ],
    );
  }

  void run() {
    Future.delayed(widget.duration, () {
      if (mounted) {
        widget.pageValue = widget.pageValue + 1;
        if (widget.pageValue == widget.children.length) widget.pageValue = 0;
        if (widget.pageValue > 0)
          widget.controller.animateToPage(widget.pageValue,
              duration: Duration(milliseconds: 400), curve: Curves.easeIn);
        else
          widget.controller.animateToPage(widget.pageValue,
              duration: Duration(milliseconds: 400), curve: Curves.easeIn);
        setState(() {

        });
      }
    });
  }
}
