import 'package:flutter/widgets.dart';
import 'package:ifada/model/banner_model.dart';
import 'package:ifada/widgets/image_view.dart';

class BannerWidget extends StatelessWidget {
  BannerModel banner;

  BannerWidget(this.banner);

  @override
  Widget build(BuildContext context) {
    return ImageView(
      // "twitter.png",
      url: banner.product_image,
      height: double.infinity,
      width: double.infinity,
    );
  }
}